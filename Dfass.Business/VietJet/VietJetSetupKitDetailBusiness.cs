﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.VietJet.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class VietJetSetupKitDetailBusiness : Business<Tab_SetupKitDetail>
    {

        public VietJetSetupKitDetailBusiness(DbContext context)
            : base(new Repository<Tab_SetupKitDetail>(context))
        {
        }

    }
}
