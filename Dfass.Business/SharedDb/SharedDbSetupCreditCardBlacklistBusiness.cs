﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.SharedDb.Repository;
using Dfass.SharedDb.DataAccess;
using Dfass.Requests;
using Dfass.Results;
using System.IO;
using System.Data.SQLite;

namespace Dfass.Business
{
    public class SharedDbSetupCreditCardBlacklistBusiness : Business<Tab_SetupCreditCardBlackList>
    {
        public AddCreditCardBlacklistResult AddCreditCardBlacklist(string wcfPath, AddCreditCardBlacklistRequest request)
        {
            AddCreditCardBlacklistResult result = new AddCreditCardBlacklistResult();
            try
            {
                result.Status = "OK";
                //Get CreditCard Blacklist
                IRepository<Tab_SetupCreditCardBlackList> repositorySetupCreditCardBlacklist = new Repository<Tab_SetupCreditCardBlackList>();
                IList<Tab_SetupCreditCardBlackList> ccBlacklist = repositorySetupCreditCardBlacklist.GetAll();


                string destination = System.IO.Path.Combine(wcfPath, "db.sqlite");

                string cmdInsert = null;

                string encryptedCcNo = null;
                using (var conn = new SQLiteConnection("Data Source=" + destination))
                {
                    // Be sure you already created the Person Table!
                    conn.Open();

                    List<string> existedCcNos = new List<string>();
                    using (SQLiteCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = @"Select CcNo From sys_blacklist";
                        SQLiteDataReader r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            existedCcNos.Add((string)r["CcNo"]);
                        }
                    }

                    using (var cmd = new SQLiteCommand(conn))
                    {
                        using (var transaction = conn.BeginTransaction())
                        {
                            //Insert SetupCCType into sys_cctype
                            cmdInsert = "INSERT INTO sys_blacklist VALUES(?);";
                            cmd.CommandText = cmdInsert;
                            int i = 0;
                            foreach (var cb in ccBlacklist)
                            {
                                encryptedCcNo = Dfass.Utils.Util.EncryptNumber(cb.BL_CCNo);
                                if (!existedCcNos.Contains(encryptedCcNo))
                                {
                                    i++;
                                    cmd.Parameters.Add(new SQLiteParameter("CcNo", encryptedCcNo));
                                    cmd.ExecuteNonQuery();
                                }
                            }

                            transaction.Commit();
                            result.AddedBlacklistCount = i;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                result.Status = "ERROR";
                result.Description = ex.Message;
            }
            return result;
        }
    }
}
