﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.SharedDb.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class SharedDbSetupPassportBlacklistBusiness : Business<Tab_SetupPassportBlackList>
    {

        public SharedDbSetupPassportBlacklistBusiness()
            : base(new Repository<Tab_SetupPassportBlackList>(new DFASS_RAS_SharedDBEntities()))
        {
        }

    }
}
