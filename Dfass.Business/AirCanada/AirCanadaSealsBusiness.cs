﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.AirCanada.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class AirCanadaSealsBusiness : Business<Tab_Seals>
    {
        public AirCanadaSealsBusiness(DbContext context)
            : base(new Repository<Tab_Seals>(context))
        {
            
        }

        public override IList<Tab_Seals> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_Seals> ses = new List<Tab_Seals>();
            if (string.IsNullOrEmpty(xmlDocument))
                return ses;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);
            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_Seals");

            var nodes = document.SelectNodes("//ns:Tab_Seals", manager);
            //var nodes = document.SelectNodes("//Tab_FlightRecordeds/Tab_FlightRecorded");

            foreach (XmlNode n in nodes)
            {
                Tab_Seals se = new Tab_Seals();
                se.SE_SifNo = n.SelectSingleNode("ns:SE_SifNo", manager).InnerText;
                se.SE_CartNo = n.SelectSingleNode("ns:SE_CartNo", manager).InnerText;
                se.SE_Sector = byte.Parse(n.SelectSingleNode("ns:SE_Sector", manager).InnerText);
                se.SE_DisplaySeq = byte.Parse(n.SelectSingleNode("ns:SE_DisplaySeq", manager).InnerText);
                se.SE_SealNo = n.SelectSingleNode("ns:SE_SealNo", manager).InnerText;
                se.SE_Status = n.SelectSingleNode("ns:SE_Status", manager).InnerText;
                se.SE_RecordedDate = DateTime.Parse(n.SelectSingleNode("ns:SE_RecordedDate", manager).InnerText);
                se.SE_DeviceId = short.Parse(n.SelectSingleNode("ns:SE_DeviceId", manager).InnerText);
                se.SE_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:SE_DeviceSyncKey", manager).InnerText);
                ses.Add(se);
            }
            //Remove duplicated seals
            ses = ses.GroupBy(g => new { g.SE_SifNo, g.SE_Sector, g.SE_SealNo, g.SE_RecordedDate }).Select(s => s.First()).ToList();

            return ses;
        }
        public override bool Add(IList<Tab_Seals> listSeals)
        {
            bool result = true;
            repository.Add(listSeals.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_Seals> listSeals)
        {
            return repository.PreAdd(listSeals.ToArray<Tab_Seals>());
        }
    }
}
