﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.AirCanada.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class AirCanadaPreorderPosCaptureBusiness : Business<Tab_PreorderPosCapture>
    {

        public AirCanadaPreorderPosCaptureBusiness(DbContext context)
            : base(new Repository<Tab_PreorderPosCapture>(context))
        {
            
        }

        public override IList<Tab_PreorderPosCapture> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_PreorderPosCapture> pcs = new List<Tab_PreorderPosCapture>();
            if (string.IsNullOrEmpty(xmlDocument))
                return pcs;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_PreorderPosCapture");

            var nodes = document.SelectNodes("//ns:Tab_PreorderPosCapture", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_PreorderPosCapture pc = new Tab_PreorderPosCapture();
                pc.PC_SifNo = n.SelectSingleNode("ns:PC_SifNo", manager).InnerText;
                pc.PC_Sector = byte.Parse(n.SelectSingleNode("ns:PC_Sector", manager).InnerText);
                pc.PC_OrderNo = short.Parse(n.SelectSingleNode("ns:PC_OrderNo", manager).InnerText);
                pc.PC_FlightCodeCap = n.SelectSingleNode("ns:PC_FlightCodeCap", manager).InnerText;
                pc.PC_FlightNoCap = short.Parse(n.SelectSingleNode("ns:PC_FlightNoCap", manager).InnerText);
                pc.PC_FromCap = n.SelectSingleNode("ns:PC_FromCap", manager).InnerText;
                pc.PC_ToCap = n.SelectSingleNode("ns:PC_ToCap", manager).InnerText;
                pc.PC_DepartureDateCap = DateTime.Parse(n.SelectSingleNode("ns:PC_DepartureDateCap", manager).InnerText);
                pc.PC_OriginalSector = byte.Parse(n.SelectSingleNode("ns:PC_OriginalSector", manager).InnerText);
                pc.PC_DeviceId = short.Parse(n.SelectSingleNode("ns:PC_DeviceId", manager).InnerText);
                pc.PC_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:PC_DeviceSyncKey", manager).InnerText);
                pc.PC_PaxName = n.SelectSingleNode("ns:PC_PaxName", manager).InnerText;
                pc.PC_ProcessStatus = n.SelectSingleNode("ns:PC_ProcessStatus", manager).InnerText;//"A"->"P"->"D"
                //pc.PC_DownloadDate = null;
                pcs.Add(pc);
            }

            return pcs;
        }
        public override bool Add(IList<Tab_PreorderPosCapture> listOrderLine)
        {
            bool result = true;
            repository.Add(listOrderLine.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_PreorderPosCapture> listOrderLine)
        {
            return repository.PreAdd(listOrderLine.ToArray<Tab_PreorderPosCapture>());
        }

        public override bool PreUpdate(IList<Tab_PreorderPosCapture> list)
        {
            return repository.PreUpdate(list.ToArray<Tab_PreorderPosCapture>());
        }
    }
}
