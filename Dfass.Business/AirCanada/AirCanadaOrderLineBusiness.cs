﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.AirCanada.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class AirCanadaOrderLineBusiness : Business<Tab_OrderLine>
    {

        public AirCanadaOrderLineBusiness(DbContext context)
            : base(new Repository<Tab_OrderLine>(context))
        {
            
        }

        public override IList<Tab_OrderLine> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_OrderLine> ols = new List<Tab_OrderLine>();
            if (string.IsNullOrEmpty(xmlDocument))
                return ols;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_OrderLine");

            var nodes = document.SelectNodes("//ns:Tab_OrderLine", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_OrderLine ol = new Tab_OrderLine();
                ol.OL_SifNo = n.SelectSingleNode("ns:OL_SifNo", manager).InnerText;
                ol.OL_Sector = byte.Parse(n.SelectSingleNode("ns:OL_Sector", manager).InnerText);
                ol.OL_OrderNo = short.Parse(n.SelectSingleNode("ns:OL_OrderNo", manager).InnerText);
                ol.OL_LineNo = byte.Parse(n.SelectSingleNode("ns:OL_LineNo", manager).InnerText);
                ol.OL_Sku = n.SelectSingleNode("ns:OL_Sku", manager).InnerText;
                ol.OL_Quantity = short.Parse(n.SelectSingleNode("ns:OL_Quantity", manager).InnerText);
                ol.OL_PriceEach = Decimal.Parse(n.SelectSingleNode("ns:OL_PriceEach", manager).InnerText);
                ol.OL_Type = n.SelectSingleNode("ns:OL_Type", manager).InnerText;
                ol.OL_KitCode = n.SelectSingleNode("ns:OL_KitCode", manager).InnerText;
                ol.OL_OriginalSector = ol.OL_Sector;
                ol.OL_DeviceId = short.Parse(n.SelectSingleNode("ns:OL_DeviceId", manager).InnerText);
                ol.OL_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:OL_DeviceSyncKey", manager).InnerText);
                //Check to save Steb Bag serial no.
                if (ol.OL_Sku.Equals("991002"))
                {
                    ol.OL_SerialNo = n.SelectSingleNode("ns:OL_ItemName", manager).InnerText;
                }
                    
                ols.Add(ol);
            }

            return ols;
        }
        public override bool Add(IList<Tab_OrderLine> listOrderLine)
        {
            bool result = true;
            repository.Add(listOrderLine.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_OrderLine> listOrderLine)
        {
            return repository.PreAdd(listOrderLine.ToArray<Tab_OrderLine>());
        }
    }
}
