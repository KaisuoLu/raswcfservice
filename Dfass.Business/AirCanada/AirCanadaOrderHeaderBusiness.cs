﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.AirCanada.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class AirCanadaOrderHeaderBusiness : Business<Tab_OrderHeader>
    {
        public AirCanadaOrderHeaderBusiness(DbContext context)
            : base(new Repository<Tab_OrderHeader>(context))
        {
        }
        //public static long? ParseNullableLong(this string value)
        //{
        //    long longValue;
        //    if (long.TryParse(value, out longValue))
        //        return longValue;
        //    return null;
        //}
        public override IList<Tab_OrderHeader> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_OrderHeader> ohs = new List<Tab_OrderHeader>();
            if (string.IsNullOrEmpty(xmlDocument))
                return ohs;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_OrderHeader");

            var nodes = document.SelectNodes("//ns:Tab_OrderHeader", manager);
            XmlNode xmlnode = null;

            foreach (XmlNode n in nodes)
            {
                Tab_OrderHeader oh = new Tab_OrderHeader();
                oh.OH_SifNo = n.SelectSingleNode("ns:OH_SifNo", manager).InnerText;
                oh.OH_Sector = byte.Parse(n.SelectSingleNode("ns:OH_Sector", manager).InnerText);
                oh.OH_OrderNo = short.Parse(n.SelectSingleNode("ns:OH_OrderNo", manager).InnerText);
                oh.OH_Total = decimal.Parse(n.SelectSingleNode("ns:OH_Total", manager).InnerText);
                oh.OH_Voided = n.SelectSingleNode("ns:OH_Voided", manager).InnerText == "1" ? true : false;
                oh.OH_OrderTime = DateTime.Parse(n.SelectSingleNode("ns:OH_OrderTime", manager).InnerText);
                oh.OH_Credit = false;
                oh.OH_OriginalSector = byte.Parse(n.SelectSingleNode("ns:OH_OriginalSector", manager).InnerText);
                oh.OH_Seat = n.SelectSingleNode("ns:OH_Seat", manager).InnerText;
                oh.OH_Type = n.SelectSingleNode("ns:OH_Type", manager).InnerText;
                oh.OH_DeviceId = short.Parse(n.SelectSingleNode("ns:OH_DeviceId", manager).InnerText);
                oh.OH_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:OH_DeviceSyncKey", manager).InnerText);
                oh.OH_Passport = n.SelectSingleNode("ns:OH_Passport", manager).InnerText;
                oh.OH_Service = "MA";
                oh.OH_PaxName = n.SelectSingleNode("ns:OH_PaxName", manager).InnerText;
                oh.OH_PaxClass = n.SelectSingleNode("ns:OH_PaxClass", manager).InnerText;
                oh.OH_CrewSale_CrewId = n.SelectSingleNode("ns:OH_CrewSale_CrewId", manager).InnerText;

                //When we have time, we will check xml text from device.
                xmlnode = n.SelectSingleNode("ns:OH_PointCardNo", manager);
                if (xmlnode != null)
                    oh.OH_PointCardNo = xmlnode.InnerText;
                else
                    oh.OH_PointCardNo = "";

                xmlnode = n.SelectSingleNode("ns:OH_PointCardType", manager);
                if (xmlnode != null)
                    oh.OH_PointCardType = xmlnode.InnerText;
                else
                    oh.OH_PointCardType = "";

                //PointCardPoints
                try
                {

                    xmlnode = n.SelectSingleNode("ns:OH_PointCardPoints", manager);
                    if (xmlnode != null)
                        oh.OH_PointCardPoints = Utils.Util.ParseNullableLong(xmlnode.InnerText);
                    else
                        oh.OH_PointCardPoints = 0;
                    
                }
                catch(Exception ex)
                {
                    oh.OH_PointCardPoints = 0;
                }

                //PointCardVoucherPoints
                try
                {

                    xmlnode = n.SelectSingleNode("ns:OH_PointCardVoucherPoints", manager);
                    if (xmlnode != null)
                        oh.OH_PointCardVoucherPoints = Utils.Util.ParseNullableLong(xmlnode.InnerText);
                    else
                        oh.OH_PointCardVoucherPoints = 0;

                }
                catch (Exception ex)
                {
                    oh.OH_PointCardVoucherPoints = 0;
                }

                xmlnode = n.SelectSingleNode("ns:OH_Status", manager);
                if (xmlnode != null)
                    oh.OH_Status = xmlnode.InnerText;
                else
                    oh.OH_Status = "";

                ohs.Add(oh);
            }

            return ohs;
        }
        public override bool Add(IList<Tab_OrderHeader> listOrderHeader)
        {
            bool result = true;
            repository.Add(listOrderHeader.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_OrderHeader> listOrderHeader)
        {
            return repository.PreAdd(listOrderHeader.ToArray<Tab_OrderHeader>());
        }
    }
}
