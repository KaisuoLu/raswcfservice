﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Singapore.DataAccess;

namespace Dfass.Business
{
    public interface ICartAssignmentBusiness
    {
        Dfass.Models.CartAssignment GetCartAssignment(string cartno);

        void UpdateCartAssignmentStatus(long recordno, string status);

        void UpdateCartAssignmentStatus(string cartno, string status);
    }
}
