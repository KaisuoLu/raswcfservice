﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Demo.DataAccess;
using Dfass.Repository;
using Dfass.Utils;

namespace Dfass.Business
{
    public class DemoSifStatusBusiness : Business<Tab_SifStatus>,ISifStatusBusiness<Tab_SifStatus>
    {

        public DemoSifStatusBusiness(DbContext context)
            : base(new Repository<Tab_SifStatus>(context))
        {
            
        }

        public override IList<Tab_SifStatus> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_SifStatus> sss = new List<Tab_SifStatus>();
            if (string.IsNullOrEmpty(xmlDocument))
                return sss;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);
            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_SifStatus");

            var nodes = document.SelectNodes("//ns:Tab_SifStatus", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_SifStatus ss = new Tab_SifStatus();
                ss.SS_AirlineCode = short.Parse(n.SelectSingleNode("ns:SS_AirlineCode", manager).InnerText);
                ss.SS_SifNo = n.SelectSingleNode("ns:SS_SifNo", manager).InnerText;
                ss.SS_SifPackedPrinted = Util.ParseNullableDateTime(n.SelectSingleNode("ns:SS_SifPackedPrinted", manager).InnerText);
                ss.SS_DeviceIdHHC1 = Util.ParseNullableShort(n.SelectSingleNode("ns:SS_DeviceIdHHC1", manager).InnerText);
                ss.SS_DownloadStationHHC1 = n.SelectSingleNode("ns:SS_DownloadStationHHC1", manager).InnerText;
                ss.SS_DownloadSuccessHHC1 = Util.ParseNullableDateTime(n.SelectSingleNode("ns:SS_DownloadSuccessHHC1", manager).InnerText);
                ss.SS_FirstInboundPI = DateTime.Parse(n.SelectSingleNode("ns:SS_FirstInboundPI", manager).InnerText);
                ss.SS_LastUpdated = DateTime.Parse(n.SelectSingleNode("ns:SS_LastUpdated", manager).InnerText);

                sss.Add(ss);
            }

            return sss;
        }
        public override bool Add(IList<Tab_SifStatus> listSifStatus)
        {
            bool result = true;
            repository.Add(listSifStatus.ToArray());
            return result;
        }

        public bool Existed(string sifno)
        {
            return repository.Exist(s => s.SS_SifNo == sifno);
        }

        public override bool PreAdd(IList<Tab_SifStatus> listSifStatus)
        {
            return repository.PreAdd(listSifStatus.ToArray<Tab_SifStatus>());
        }
    }
}
