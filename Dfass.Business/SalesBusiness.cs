﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
using Dfass.Repository;
using Dfass.Requests;
using Dfass.Results;

namespace Dfass.Business
{
    public class SalesBusiness : ISalesBusiness
    {
        //protected DbContext context = null;
        //protected IFlightRecordedBusiness businessFlightRecorded = null;
        //protected IOrderHeaderBusiness businessOrderHeader = null;
        //protected IOrderLineBusiness businessOrderLine = null;
        //protected IPaymentLineBusiness businessPaymentLine = null;
        //protected IItemCountBusiness businessItemCount = null;
        //protected ICrewRecordedBusiness businessCrewRecorded = null;
        //protected ISealsBusiness businessSeals = null;

        public SalesBusiness()
        {
        }

        public virtual bool CreateDatabase(string wcfPath, int airlinecode)
        {
            throw new Exception("You can not directly invoke base method!");
        }

        public virtual DownloadDatabaseResult DownloadDatabase(string pathWcf, DownloadDatabaseRequest request)
        {
            throw new Exception("You can not directly invoke base method!");
        }

        public virtual bool SaveSales(UploadSalesRequest request)
        {
            throw new Exception("You can not directly invoke base method!");
        }

        public virtual PICountResult PICount(PICountRequest request)
        {
            throw new Exception("You can not directly invoke base method!");
        }

        public virtual PIUpdateCountResult PIUpdateCount(PIUpdateCountRequest request)
        {
            throw new Exception("You can not directly invoke base method!");
        }


        public enum DeviceRequest
        {
            //Warning: The order is crtically important to keep binary compatibility with the web service
            setAirlineCodes, downloadRowsFromBackoffice_ifUpdated, downloadRowsFromBackoffice, sendRowsToBackoffice, setCartNumber,
            setMode, logText, getNewSifNo, setDeviceId, getDeviceId, downloadWelcomeBitmap_ifUpdated, noLongerUsed_downloadWelcomeBitmap, end,
            setBondroomLoginId, bondroomPrintSif, bondroomGetLaserPrinterStatus, bondroomPrintPI, bondroomRecallPI_usingSifDigits,
            bondroomRecallPI_usingFullSifNumber, getPackingCounts, sendItemCountsToBackoffice_outbound, pairNow, laserPiSheet_checkUpload,
            sendPreOrderMasterToBackoffice, sendItemCountsToBackoffice_piDataEntry, checkBadPairingSif, getCartNumbersForCartSet,
            sendItemCountsToBackoffice_inbound, sendSifRecordedToBackoffice, downloadChineseFont, downloadCustomBitmap_ifUpdated,

            downloadBlackListFile_request,
            downloadCustomDatbase_request,
            DownloadPrintAgentLibrary
        }

        void sendAirlineCodes(BinaryWriter bwRequest,int airline)
        {
            bwRequest.Write((byte)DeviceRequest.setAirlineCodes);
            bwRequest.Write((Int16)airline);
            int newAirlineCode = -1;
            bwRequest.Write((Int16)newAirlineCode);
        }
        public static void sendBondroomLoginId(BinaryWriter bwRequest,int bondroomLoginId)
        {
            if (bondroomLoginId == null) return;
            bwRequest.Write((byte)bondroomLoginId);
            bwRequest.Write(bondroomLoginId);
        }

        public virtual PrintOnLaserResult PrintOnLaser(string url, PrintOnLaserRequest request)
        {
            PrintOnLaserResult result = new PrintOnLaserResult();
            result.ResultState = "ERROR";
            try
            {
                int airlineCode = request.AirlineCode;
                string sifPrefix = request.SifPrefix;
                string cartNo = request.CartNo;
                string sifNo = request.SifNo;
                string kitCode = request.KitCode;
                string completedBy = request.CompleteBy;
                string hhcVersion = request.HhcVersion;
                int hhcProtocol = request.HhcProtocal;
                int copies = request.Copies;
                string serialNo = request.SerialNo;

                string print_url = String.Format(url +"?airlineCode={0}&sifPrefix={1}&cartNo={2}&sifNo={3}&kitCode={4}&completedBy={5}&hhcVersion={6}&hhcProtocol={7}&copies={8}&serialNo={9}", airlineCode, sifPrefix, cartNo, sifNo, kitCode, completedBy, hhcVersion, hhcProtocol, copies, serialNo);
                var webrequest = (HttpWebRequest)WebRequest.Create(print_url);
                var response = webrequest.GetResponse();
                var receiveStream = response.GetResponseStream();

                //StreamReader sr = new StreamReader(receiveStream, new UTF8Encoding());
                //string all=sr.ReadToEnd();
                result.SifNo = request.SifNo;
                result.KitCode = request.KitCode;
                if (receiveStream != null && receiveStream.CanRead)
                {
                    BinaryReader br = new BinaryReader(receiveStream);
                    result.ResultState = br.ReadBoolean() ? "OK" : "ERROR";
                    result.ResultDiscription = br.ReadString();
                    result.PrintInMinutes = br.ReadInt32();
                }
                else
                {
                    result.ResultDiscription = "Please check the Print Agent!";
                }

            }
            catch (Exception ex)
            {
                result.ResultState = "ERROR";
                result.ResultDiscription = ex.Message;
            }
            return result;
        }

        public virtual void UpdateItemCount(int deviceid, int deviceSyncKey,  string sifNo, string cartNo, Dfass.Models.PICountInfo[] piCountInfos,Dfass.Models.SealInfo[] seals)
        {
            throw new Exception("You can not directly invoke base method!");
        }

        public virtual PrintSifOnLaserResult PrintSifOnLaser(string url, PrintSifOnLaserRequest request)
        {
            
            PrintSifOnLaserResult result = new PrintSifOnLaserResult();
            result.ResultState = "ERROR";

            try
            {
                UpdateItemCount(int.Parse(request.DeviceId), int.Parse(request.DeviceSyncKey), request.SifNo, request.CartNo, request.PICountInfos,request.SealInfos);

                int airlineCode = request.AirlineCode;
                string bondCode = request.BondCode;
                string cartNo = request.CartNo;
                string sifNo = request.SifNo;
                string flightPrefix = request.FlightPrefix;
                int flightNumberInt = request.FlightNumberInt;
                string flightFrom = request.FlightFrom;
                string flightTo = request.FlightTo;
                string departureDate = request.DepartureDate;
                string packedBy = request.PackedBy;
                string kitCode = request.KitCode;
                string hhcVersion = request.HhcVersion;
                int hhcProtocol = request.HhcProtocal;
                int copies = request.Copies;
                string serialNo = request.SerialNo;

                string print_url = String.Format(url + "?airlineCode={0}&bondCode={1}&cartNo={2}&sifNo={3}&flightPrefix={4}&flightNumberInt={5}&flightFrom={6}&flightTo={7}&departureDate={8}&packedBy={9}&kitCode={10}&hhcVersion={11}&hhcProtocol={12}&copies={13}&serialNo={14}", airlineCode, bondCode, cartNo, sifNo, flightPrefix, flightNumberInt, flightFrom, flightTo, departureDate, packedBy, kitCode, hhcVersion, hhcProtocol, copies, serialNo);
                var webrequest = (HttpWebRequest)WebRequest.Create(print_url);
                var response = webrequest.GetResponse();
                var receiveStream = response.GetResponseStream();

                //StreamReader sr = new StreamReader(receiveStream, new UTF8Encoding());
                //string all=sr.ReadToEnd();
                result.SifNo = request.SifNo;
                result.KitCode = request.KitCode;
                if (receiveStream != null && receiveStream.CanRead)
                {
                    BinaryReader br = new BinaryReader(receiveStream);
                    result.ResultState = br.ReadBoolean() ? "OK" : "ERROR";
                    result.ResultDiscription = br.ReadString();
                    result.PrintInMinutes = br.ReadInt32();
                }
                else
                {
                    result.ResultDiscription = "Please check the Print Agent!";
                }

            }
            catch (Exception ex)
            {
                result.ResultState = "ERROR";
                result.ResultDiscription = ex.Message;
            }
            return result;

        }

        protected decimal GetSaleTotal(IList<Models.OrderLine> list)
        {
            decimal? total = 0;
            foreach (var o in list)
            {
                total += (o.OL_Sku.StartsWith("@") ? -1 : 1) * o.OL_PriceEach * o.OL_Quantity;
            }
            return total ?? 0;
        }

        public virtual GetPreordersResult GetPreorders(GetPreordersRequest request)
        {
            throw new Exception("You can not directly invoke base method!");
        }
    }
}
