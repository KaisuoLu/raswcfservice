﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.HKAirlines.DataAccess;
using Dfass.Repository;
using Dfass.Requests;
using Dfass.Results;
using System.Net;

namespace Dfass.Business
{
    public class HKAirlinesSalesBusiness:SalesBusiness
    {
        DbContext context = null;
        IBusiness<Tab_FlightRecorded> businessFlightRecorded = null;
        IBusiness<Tab_OrderHeader> businessOrderHeader = null;
        IBusiness<Tab_OrderLine> businessOrderLine = null;
        IBusiness<Tab_PreorderPosCapture> businessPreorderPosCapture = null;
        IBusiness<Tab_PaymentLine> businessPaymentLine = null;
        IBusiness<Tab_ItemCount> businessItemCount = null;
        IBusiness<Tab_CrewRecorded> businessCrewRecorded = null;
        IBusiness<Tab_Seals> businessSeals = null;
        HKAirlinesSifStatusBusiness businessSifStatus = null;

        public HKAirlinesSalesBusiness()
        {
            context = new IRS_BO_HKAirlinesEntities();
            businessFlightRecorded = new HKAirlinesFlightRecordedBusiness(context);
            businessOrderHeader = new HKAirlinesOrderHeaderBusiness(context);
            businessOrderLine = new HKAirlinesOrderLineBusiness(context);
            businessPreorderPosCapture = new HKAirlinesPreorderPosCaptureBusiness(context);
            businessPaymentLine = new HKAirlinesPaymentLineBusiness(context);
            businessItemCount = new HKAirlinesItemCountBusiness(context);
            businessCrewRecorded = new HKAirlinesCrewRecordedBusiness(context);
            businessSeals = new HKAirlinesSealsBusiness(context);
            businessSifStatus = new HKAirlinesSifStatusBusiness(context);
        }

        public HKAirlinesSalesBusiness(DbContext context)
        {
            businessFlightRecorded = new HKAirlinesFlightRecordedBusiness(context);
            businessOrderHeader = new HKAirlinesOrderHeaderBusiness(context);
            businessOrderLine = new HKAirlinesOrderLineBusiness(context);
            businessPaymentLine = new HKAirlinesPaymentLineBusiness(context);
            businessItemCount = new HKAirlinesItemCountBusiness(context);
            businessCrewRecorded = new HKAirlinesCrewRecordedBusiness(context);
            businessSeals = new HKAirlinesSealsBusiness(context);
            businessSifStatus = new HKAirlinesSifStatusBusiness(context);
        }

        public override bool CreateDatabase(string wcfPath, int airlinecode)
        {
            bool result = true;

            DbContext context = new IRS_BO_HKAirlinesEntities();
            //ISetupCCBlackListRepository repositorySetupCCBlackList = new SetupCCBlackListRepository(context);
            //IList<Tab_SetupCCBlackList> ccBlackList = repositorySetupCCBlackList.GetAll();

            IRepository<Tab_SetupCCType> repositorySetupCCType = new Repository<Tab_SetupCCType>(context);
            IList<Tab_SetupCCType> ccTypes = repositorySetupCCType.GetAll();

            IRepository<Tab_SetupCrewMaster> repositorySetupCrewMaster = new Repository<Tab_SetupCrewMaster>(context);
            IList<Tab_SetupCrewMaster> crews = repositorySetupCrewMaster.GetAll();

            //ISetupCrewPositionRepository repositorySetupCrewPosition = new SetupCrewPositionRepository(context);
            //IList<Tab_SetupCrewPosition> positions = repositorySetupCrewPosition.GetAll();

            IRepository<Tab_SetupCurrency> repositorySetupCurrency = new Repository<Tab_SetupCurrency>(context);
            IList<Tab_SetupCurrency> currencies = repositorySetupCurrency.GetAll();

            IRepository<Tab_SetupFlightMaster> repositorySetupFlightMaster = new Repository<Tab_SetupFlightMaster>(context);
            IList<Tab_SetupFlightMaster> flights = repositorySetupFlightMaster.GetAll();

            IRepository<Tab_SetupHHCMessage> repositorySetupHHCMessage = new Repository<Tab_SetupHHCMessage>(context);
            IList<Tab_SetupHHCMessage> messages = repositorySetupHHCMessage.GetAll();

            IRepository<Tab_SetupItemCategory> repositorySetupItemCategory = new Repository<Tab_SetupItemCategory>(context);
            IList<Tab_SetupItemCategory> categories = repositorySetupItemCategory.GetAll();

            IRepository<Tab_SetupKitMaster> repositorySetupKitMaster = new Repository<Tab_SetupKitMaster>(context);
            IList<Tab_SetupKitMaster> kits = repositorySetupKitMaster.GetAll();

            IRepository<Tab_SetupKitDetail> repositorySetupKitDetail = new Repository<Tab_SetupKitDetail>(context);
            IList<Tab_SetupKitDetail> kds = repositorySetupKitDetail.GetAll();

            //ISetupRegionRepository repositorySetupRegion = new SetupRegionRepository();
            //IList<Tab_SetupRegion> regions = repositorySetupRegion.GetAll();

            IRepository<Tab_SetupSector> repositorySetupSector = new Repository<Tab_SetupSector>(context);
            IList<Tab_SetupSector> sectors = repositorySetupSector.GetAll();

            IRepository<Tab_SetupStation> repositorySetupStation = new Repository<Tab_SetupStation>(context);
            IList<Tab_SetupStation> stations = repositorySetupStation.GetAll();

            IRepository<Tab_SetupProductDescription> repositorySetupProductDescription = new Repository<Tab_SetupProductDescription>(context);
            IList<Tab_SetupProductDescription> productDescriptions = repositorySetupProductDescription.GetAll();

            Dfass.SharedDb.Repository.ISetupPassportBlacklistRepository repositorySetupPassportBlacklist = new Dfass.SharedDb.Repository.SetupPassportBlacklistRepository();
            IList<Dfass.SharedDb.DataAccess.Tab_SetupPassportBlackList> passportBlacklists = repositorySetupPassportBlacklist.GetList(b => b.PB_AirlineCode == 1053);

            string source=Path.Combine(wcfPath,"db.sqlite");

            var cnn1 = new SQLiteConnection("Data Source="+source);


            string destination=System.IO.Path.Combine(wcfPath+"database" ,airlinecode+".sqlite");
            FileInfo fl = new FileInfo(destination);
            if (fl.Exists)
                fl.Delete();
            File.Copy(source, destination);


            string cmdInsert=null;
            using (var conn = new SQLiteConnection("Data Source=" + destination))
            {
                // Be sure you already created the Person Table!
                conn.Open();

                using (var cmd = new SQLiteCommand(conn))
                {
                    using (var transaction = conn.BeginTransaction())
                    {
                        //Insert SetupCCType into sys_cctype
                        cmdInsert = "INSERT INTO sys_cctype VALUES(?,?,?,?,?,?,?,?,?,?,?);";
                        cmd.CommandText = cmdInsert;
                        int i = 0;
                        foreach (var ct in ccTypes)
                        {
                            i++;
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", i));
                            cmd.Parameters.Add(new SQLiteParameter("CCCode", ct.CT_CCCode));
                            cmd.Parameters.Add(new SQLiteParameter("CCName", ct.CT_CCName));
                            cmd.Parameters.Add(new SQLiteParameter("MaxLimit", ct.CT_MaxLimit));
                            cmd.Parameters.Add(new SQLiteParameter("MinLimit", ct.CT_MinLimit));
                            cmd.Parameters.Add(new SQLiteParameter("Range", ct.CT_Range));
                            cmd.Parameters.Add(new SQLiteParameter("PaymentMode", ct.CT_PaymentMode));
                            cmd.Parameters.Add(new SQLiteParameter("MerchantAC", ct.CT_MerchantAC));
                            cmd.Parameters.Add(new SQLiteParameter("MerchantAddress", ct.CT_MerchantAddress));
                            cmd.Parameters.Add(new SQLiteParameter("MerchantPhone", ct.CT_MerchantPhone));
                            cmd.Parameters.Add(new SQLiteParameter("BaseCurrency", ct.CT_BaseCurrency));
                            cmd.ExecuteNonQuery();
                        }


                        //Insert Crew from CrewMaster
                        cmdInsert = "INSERT INTO sys_crew VALUES(?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;

                        foreach (var crew in crews)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("CrewID", crew.CM_CrewId));
                            cmd.Parameters.Add(new SQLiteParameter("Name", crew.CM_FirstName));
                            cmd.Parameters.Add(new SQLiteParameter("PositionCode", crew.CM_PositionCode));
                            cmd.Parameters.Add(new SQLiteParameter("Base", crew.CM_Base));
                            cmd.Parameters.Add(new SQLiteParameter("Surname", crew.CM_Surname));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert default Training1
                        cmd.Parameters.Add(new SQLiteParameter("CrewID", "88888"));
                        cmd.Parameters.Add(new SQLiteParameter("Name", "TRAINING1"));
                        cmd.Parameters.Add(new SQLiteParameter("PositionCode", "LFA"));
                        cmd.Parameters.Add(new SQLiteParameter("Base", "AMU"));
                        cmd.Parameters.Add(new SQLiteParameter("Surname", ""));
                        cmd.ExecuteNonQuery();

                        //Insert default Training1
                        cmd.Parameters.Add(new SQLiteParameter("CrewID", "99999"));
                        cmd.Parameters.Add(new SQLiteParameter("Name", "TRAINING2"));
                        cmd.Parameters.Add(new SQLiteParameter("PositionCode", "LFA"));
                        cmd.Parameters.Add(new SQLiteParameter("Base", "AMU"));
                        cmd.Parameters.Add(new SQLiteParameter("Surname", ""));
                        cmd.ExecuteNonQuery();

                        //Insert SetupCurrency into sys_currency
                        cmdInsert = "INSERT INTO sys_currency VALUES(?,?,?,?,?,?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var currency in currencies)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("Currency", currency.CU_Code));
                            cmd.Parameters.Add(new SQLiteParameter("Rate", currency.CU_ExRate));
                            cmd.Parameters.Add(new SQLiteParameter("Name", currency.CU_CurrencyName));
                            cmd.Parameters.Add(new SQLiteParameter("Symbol", currency.CU_Symbol));
                            cmd.Parameters.Add(new SQLiteParameter("MaxLimit", currency.CU_MaxLimit));
                            cmd.Parameters.Add(new SQLiteParameter("MinLimit", currency.CU_MinLimit));
                            cmd.Parameters.Add(new SQLiteParameter("temp", DBNull.Value));
                            cmd.Parameters.Add(new SQLiteParameter("temp2", DBNull.Value));
                            cmd.Parameters.Add(new SQLiteParameter("StartDate", currency.CU_StartDate));
                            cmd.Parameters.Add(new SQLiteParameter("EndDate", currency.CU_EndDate));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert FlightMaster into sys_flight
                        cmdInsert = "INSERT INTO sys_flight VALUES(?,?,?,?,?,?,?,?);";//(FlightCode,FlightNo,FlightFrom,FlightTo,Status,Region)
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var f in flights)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", f.FM_RecordNo));
                            cmd.Parameters.Add(new SQLiteParameter("FlightCode", f.FM_FlightCode));
                            cmd.Parameters.Add(new SQLiteParameter("FlightNo", f.FM_FlightNo));
                            cmd.Parameters.Add(new SQLiteParameter("FlightFrom", f.FM_From));
                            cmd.Parameters.Add(new SQLiteParameter("FlightTo", f.FM_To));
                            cmd.Parameters.Add(new SQLiteParameter("Status", f.FM_Status));
                            cmd.Parameters.Add(new SQLiteParameter("Region", f.FM_Region));
                            cmd.Parameters.Add(new SQLiteParameter("SalesTarget", f.FM_SalesTarget));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupHHCMessage into 
                        cmdInsert = "INSERT INTO sys_msg VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var m in messages)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("MessageType", m.HM_MessageType));
                            cmd.Parameters.Add(new SQLiteParameter("Message", m.HM_MessageContent));
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", m.HM_RecordNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupItemCategory into sys_category
                        cmdInsert = "INSERT or IGNORE INTO sys_category VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var c in categories)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("CategoryID", c.CA_CategoryId));
                            cmd.Parameters.Add(new SQLiteParameter("Name", c.CA_ButtonText));
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", c.CA_RecordNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupKitMaster into sys_kit
                        cmdInsert = "INSERT INTO sys_kit VALUES(?,?,?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var k in kits)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", k.SK_RecordNo));
                            cmd.Parameters.Add(new SQLiteParameter("KitCode", k.SK_KitCode));
                            cmd.Parameters.Add(new SQLiteParameter("Description", k.SK_Description));
                            cmd.Parameters.Add(new SQLiteParameter("POSDescription", k.SK_POSDescription));
                            cmd.Parameters.Add(new SQLiteParameter("Status", k.SK_Status));
                            cmd.Parameters.Add(new SQLiteParameter("StartDate", k.SK_StartDate));
                            cmd.Parameters.Add(new SQLiteParameter("EndDate", k.SK_EndDate));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupKitDetail into sys_kit_details
                        cmdInsert = "INSERT INTO sys_kit_details VALUES(?,?,?,?,?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var kd in kds)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", kd.KD_RecordNo));//
                            cmd.Parameters.Add(new SQLiteParameter("ItemCode", kd.KD_ItemCode));
                            cmd.Parameters.Add(new SQLiteParameter("ItemName", kd.KD_ButtonText));
                            cmd.Parameters.Add(new SQLiteParameter("CategoryID", kd.KD_CategoryId));
                            cmd.Parameters.Add(new SQLiteParameter("UploadQty", kd.KD_UploadQty));
                            cmd.Parameters.Add(new SQLiteParameter("ItemLocation", kd.KD_ItemLocation));
                            cmd.Parameters.Add(new SQLiteParameter("Barcode", kd.KD_BarCode));
                            cmd.Parameters.Add(new SQLiteParameter("RetailPrice", kd.KD_RetailPrice));
                            cmd.Parameters.Add(new SQLiteParameter("CatalogNo", kd.KD_CatalogNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert into g_products
                        if (kits.Count > 0)
                        {
                            IEnumerable<Tab_SetupKitDetail> prs=from d in kds where d.KD_RecordNo==kits[0].SK_RecordNo select d;
                            cmdInsert = "INSERT INTO g_products VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                            cmd.Parameters.Clear();
                            cmd.CommandText = cmdInsert;

                            foreach (var pr in prs)
                            {
                                //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                                cmd.Parameters.Add(new SQLiteParameter("ProductCode", pr.KD_ItemCode));//
                                cmd.Parameters.Add(new SQLiteParameter("Name", pr.KD_ButtonText));
                                cmd.Parameters.Add(new SQLiteParameter("Price", pr.KD_RetailPrice));
                                cmd.Parameters.Add(new SQLiteParameter("CategoryID", pr.KD_CategoryId));
                                cmd.Parameters.Add(new SQLiteParameter("Qty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("Brand", ""));
                                cmd.Parameters.Add(new SQLiteParameter("Bin", pr.KD_ItemLocation));
                                cmd.Parameters.Add(new SQLiteParameter("CatalogNo", pr.KD_CatalogNo));
                                cmd.Parameters.Add(new SQLiteParameter("Description", pr.KD_ItemName));
                                cmd.Parameters.Add(new SQLiteParameter("Description2", ""));
                                cmd.Parameters.Add(new SQLiteParameter("DmgQty", 0));
                                cmd.Parameters.Add(new SQLiteParameter("PhyQty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("CartQty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("Barcode", pr.KD_BarCode));
                                cmd.Parameters.Add(new SQLiteParameter("CrewDiscount", 0));
                                cmd.Parameters.Add(new SQLiteParameter("OpenQty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("LAG", 0));
                                cmd.ExecuteNonQuery();
                            }
                        }

                        //Insert SetupRegion into sys_region

                        //Insert SetupSector into sys_sector
                        cmdInsert = "INSERT INTO sys_sector VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var se in sectors)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("SectorCode", se.SS_SectorCode));
                            cmd.Parameters.Add(new SQLiteParameter("SectorName", se.SS_SectorName));
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", se.SS_RecordNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupProductDescription
                        cmdInsert = "INSERT INTO sys_products(ProductCode, Description,Description2) VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var pd in productDescriptions)
                        {
                            cmd.Parameters.Add(new SQLiteParameter("ProductCode", pd.PD_ProductCode));
                            cmd.Parameters.Add(new SQLiteParameter("Description", pd.PD_ProductDescription));
                            cmd.Parameters.Add(new SQLiteParameter("Description2", ""));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupPassportBlacklist
                        cmdInsert = "INSERT INTO sys_ppblacklist VALUES(?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var pb in passportBlacklists)
                        {
                            cmd.Parameters.Add(new SQLiteParameter("PpNo", pb.PB_PPNoBX));
                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                }
            }

            
            return result;
        }

        public override DownloadDatabaseResult DownloadDatabase(string pathWcf, DownloadDatabaseRequest request)
        {
            DownloadDatabaseResult rst = new DownloadDatabaseResult();
            string filename = Path.Combine(pathWcf + "/database", request.AirlineCode + ".sqlite");


            FileInfo fileinfo = new FileInfo(filename);
            if (!fileinfo.Exists)
            {
                if (!CreateDatabase(pathWcf, request.AirlineCode))
                {
                    rst.Status = "ERROR";
                    rst.Description = "Create Kit Database error!";
                    return rst;
                }
            }
            byte[] bytes = System.IO.File.ReadAllBytes(filename);

            //Get Sif No.
            //rst.SifNo = 100088;
            MalaysiaSifAllocationBusiness businessSifAllocation = new HKAirlinesSifAllocationBusiness(context);
            rst.SifNo = businessSifAllocation.GetSifNo(request.SifPrefix);

            rst.Base64Content = System.Convert.ToBase64String(bytes);
            rst.FileName = request.AirlineCode + ".sqlite";
            //rst.FileLength = fileinfo.Length;
            rst.Status = "OK";
            return rst;

        }

        public override bool SaveSales(UploadSalesRequest request)
        {
            bool result=true;
            //
            IList<Tab_SifStatus> sss = businessSifStatus.ParseXmlDocument(request.XmlSifStatus);
            if (sss.Count == 0)
                return false;
            else
            {
                //If already uploaded, just return true.
                if (businessSifStatus.Existed(sss[0].SS_SifNo))
                    return true;
            }
            //Save Flight Recorded
            IList<Tab_FlightRecorded> frs = businessFlightRecorded.ParseXmlDocument(request.XmlFlightRecorded);
            result = result && businessFlightRecorded.PreAdd(frs);

            //Save OrderHeader
            IList<Tab_OrderHeader> ohs = businessOrderHeader.ParseXmlDocument(request.XmlOrderHeader);
            result = result && businessOrderHeader.PreAdd(ohs);

            //Save OrderLine
            IList<Tab_OrderLine> ols = businessOrderLine.ParseXmlDocument(request.XmlOrderLine);
            result = result && businessOrderLine.PreAdd(ols);

            //Save Preorders
            IList<Tab_PreorderPosCapture> pcs = businessPreorderPosCapture.ParseXmlDocument(request.XmlPreorderPosCapture);
            result = result && businessPreorderPosCapture.PreAdd(pcs);

            ////Save PaymentLine
            IList<Tab_PaymentLine> pls = businessPaymentLine.ParseXmlDocument(request.XmlPaymentLine);
            result = result && businessPaymentLine.PreAdd(pls);

            //Update Deliveried preorders
            var pus = ohs.Where(oh=>oh.OH_Status.ToLower().Equals("d")).Join(pls, 
                h => new {SifNo= h.OH_SifNo, Sector= h.OH_Sector, OrderNo= h.OH_OrderNo }, 
                p => new {SifNo= p.PA_SifNo, Sector= p.PA_Sector, OrderNo= p.PA_OrderNo }, 
                (h, p) => new {RefNo=p.PA_RefNo });
            if (pus.Count() > 0)
            {
                IList<Tab_PreorderPosCapture> pds = new List<Tab_PreorderPosCapture>();
                foreach (var p1 in pus)
                {
                    if (!String.IsNullOrEmpty(p1.RefNo))
                    {
                        string[] ssos = p1.RefNo.Split(new char[] { ',' });
                        Tab_PreorderPosCapture pd = businessPreorderPosCapture.GetSingle(p => p.PC_SifNo.Equals(ssos[0]) && p.PC_Sector == Convert.ToByte(ssos[1]) && p.PC_OrderNo ==short.Parse(ssos[2]));
                        pd.PC_ProcessStatus = "d";
                        pds.Add(pd);
                    }
                    
                }
                businessPreorderPosCapture.PreUpdate(pds);
            }

            //Save ItemCount
            IList<Tab_ItemCount> ics = businessItemCount.ParseXmlDocument(request.XmlItemCount);
            if (businessItemCount.Existed(ic=>ic.IC_SifNo==sss[0].SS_SifNo))
                result = result && businessItemCount.PreUpdate(ics);
            else
                result = result && businessItemCount.PreAdd(ics);

            

            //Save CrewRecorded
            IList<Tab_CrewRecorded> crs = businessCrewRecorded.ParseXmlDocument(request.XmlCrewRecorded);
            result = result && businessCrewRecorded.PreAdd(crs);

            //Save Seals
            IList<Tab_Seals> ses = businessSeals.ParseXmlDocument(request.XmlSeals);
            result = result && businessSeals.PreAdd(ses);

            //Save SifStatus
                
            result = result && businessSifStatus.PreAdd(sss);


            context.SaveChanges();
 
            return result;
        }

        public override PICountResult PICount(PICountRequest request)
        {
            PICountResult result = new PICountResult();
            using (var context = new IRS_BO_HKAirlinesEntities())
            {
                IRepository<Tab_ItemCount> repositoryItemCount = new Repository<Tab_ItemCount>(context);
                IRepository<Tab_SetupKitDetail> repositorySetupKitDetail = new Repository<Tab_SetupKitDetail>(context);
                IRepository<Tab_SetupItemCategory> repositorySetupItemCategory = new Repository<Tab_SetupItemCategory>(context);
                IRepository<Tab_FlightRecorded> repositoryFlightRecorded = new Repository<Tab_FlightRecorded>(context);
                IRepository<Tab_SetupKitMaster> repositorySetupKitMaster = new Repository<Tab_SetupKitMaster>(context);

                Tab_FlightRecorded fr = repositoryFlightRecorded.GetSingle(f => f.FR_SifNo.Equals(request.SifNo) && f.FR_Hide==false);
                if (fr == null)
                {
                    result.ResultState = "ERROR";
                    return result;
                }
                result.FlightNo = fr.FR_FlightNo;
                result.FlightFrom = fr.FR_From;
                result.FlightTo = fr.FR_To;
                result.FlightDate = fr.FR_DepartureDate;
                result.CartNo = fr.FR_CartNo;
                result.KitCode = fr.FR_KitCode;

                int kitRecordNo = repositorySetupKitMaster.GetSingle(k => k.SK_KitCode == fr.FR_KitCode).SK_RecordNo;

                var itemcounts = from ic in repositoryItemCount.GetList(i => i.IC_SifNo == request.SifNo)
                                 join skd in repositorySetupKitDetail.GetList(k => k.KD_RecordNo == kitRecordNo)
                                   on ic.IC_Sku equals skd.KD_ItemCode into temp1
                                 from t in temp1
                                 join sic in repositorySetupItemCategory.GetAll()
                                   on t.KD_CategoryId equals sic.CA_CategoryId
                                 select new Dfass.Models.ItemCount
                                 {
                                     SifNo = request.SifNo,
                                     CartNo = result.CartNo,
                                     CategoryId = t.KD_CategoryId,
                                     CategoryName = sic.CA_CategoryName,
                                     Sku = t.KD_ItemCode,
                                     Name = t.KD_ButtonText,
                                     PackerOut = ic.IC_PackerOut,
                                     CrewBegin = ic.IC_CrewBegin,
                                     CrewBeginRouteAdjust = ic.IC_CrewBeginRouteAdjust,
                                     CrewSold = ic.IC_CrewSold,
                                     CrewSoldRoute = ic.IC_CrewSoldRoute,
                                     CrewReturnRouteAdjust = ic.IC_CrewReturnRouteAdjust,
                                     CrewReturn = ic.IC_CrewReturn,
                                     ReturnTotal = ic.IC_ReturnTotal,
                                     ReturnDamaged = ic.IC_ReturnDamaged,
                                     Replenish = ic.IC_Replenish,
                                     PI_Mode = ic.IC_PI_Mode,
                                     CrewSoldUnit1 = ic.IC_CrewSoldUnit1,
                                     CrewSoldUnit2 = ic.IC_CrewSoldUnit2,
                                     RecordedDate = ic.IC_RecordedDate,
                                     Latest = ic.IC_Latest ?? 0,
                                     LatestUplift = ic.IC_LatestUplift ?? 0
                                 };
                result.ItemCounts = itemcounts.ToList<Dfass.Models.ItemCount>();
                //IList<Tab_ItemCount> ics = businessItemCount.GetList(i => i.IC_SifNo == request.SifNo);
                //result.ItemCounts = new List<Models.ItemCount>();
                //foreach (Tab_ItemCount ic in ics)
                //{
                //    Dfass.Models.ItemCount itemcount = Dfass.Malaysia.Translators.ItemCountTranslator.TranslateToModel(ic);
                //    result.ItemCounts.Add(itemcount);
                //}
            }
            return result;
        }

        public override PIUpdateCountResult PIUpdateCount(PIUpdateCountRequest request)
        {
            PIUpdateCountResult result = new PIUpdateCountResult();
            result.ResultState = "OK";
            using (var context = new IRS_BO_HKAirlinesEntities())
            {
                IRepository<Tab_ItemCount> repositoryItemCount = new Repository<Tab_ItemCount>(context);
                IList<Tab_ItemCount> itemCounts = repositoryItemCount.GetList(ic => ic.IC_SifNo == request.SifNo);
                int index = -1;
                foreach (var item in request.PICountInfos)
                {
                    index = itemCounts.IndexOf(itemCounts.FirstOrDefault(ic => ic.IC_SifNo == request.SifNo && ic.IC_Sku == item.Sku));
                    if (index < 0)
                    {
                        result.ResultState = "ERROR";
                        result.ErrorDiscription = String.Format("Product of {0} does not existed!", item.Sku);
                        return result;
                    }
                    itemCounts[index].IC_PackerOut = (short)item.PackerOut;
                    itemCounts[index].IC_ReturnTotal = (short)item.ReturnTotal;
                    itemCounts[index].IC_ReturnDamaged = (short)item.ReturnDamaged;
                    itemCounts[index].IC_Replenish = (short)item.Replenish;
                    itemCounts[index].IC_Latest = (short)item.Latest;
                    itemCounts[index].IC_LatestUplift = (short)item.Replenish;
                }
                repositoryItemCount.PreUpdate(itemCounts.ToArray());
                context.SaveChanges();
            }
            return result;
        }

        public override void UpdateItemCount(int deviceid, int deviceSyncKey, string sifNo, string cartNo, Dfass.Models.PICountInfo[] piCountInfos, Dfass.Models.SealInfo[] seals)
        {
            //Write item count
            IRepository<Tab_ItemCount> repositoryItemCount = new Repository<Tab_ItemCount>(context);
            IList<Tab_ItemCount> itemCounts = repositoryItemCount.GetList(ic => ic.IC_SifNo == sifNo);
            bool itemexisted = false;
            if (itemCounts != null && itemCounts.Count() > 0)
                itemexisted = true;
            else
                itemCounts = new List<Tab_ItemCount>();

            int index = -1;
            DateTime now = DateTime.Now;
            Tab_ItemCount ic1 = null;
            foreach (var item in piCountInfos)
            {
                if (itemexisted)
                {
                    index = itemCounts.IndexOf(itemCounts.FirstOrDefault(ic => ic.IC_SifNo == sifNo && ic.IC_Sku == item.Sku));
                    if (index < 0)
                    {
                        throw new Exception(String.Format("Product of {0} does not existed!", item.Sku));
                    }
                    ic1 = itemCounts[index];
                }
                else
                {
                    ic1 = new Tab_ItemCount();
                    ic1.IC_SifNo = sifNo;
                    ic1.IC_CartNo = cartNo;
                    ic1.IC_Sku = item.Sku;
                    ic1.IC_CrewBegin = 0;
                    ic1.IC_CrewBeginRouteAdjust = 0;
                    ic1.IC_CrewSold = 0;
                    ic1.IC_CrewReturn = 0;
                    ic1.IC_PI_Mode = false;
                    ic1.IC_ReturnTotal = 0;

                }


                //Update values
                ic1.IC_PackerOut = (short)item.PackerOut;
                ic1.IC_ReturnTotal = (short)item.ReturnTotal;
                ic1.IC_ReturnDamaged = (short)item.ReturnDamaged;
                ic1.IC_Replenish = (short)item.Replenish;
                ic1.IC_Latest = (short)item.Latest;
                ic1.IC_LatestUplift = (short)item.Replenish;
                ic1.IC_RecordedDate = now;
                if (!itemexisted)
                    itemCounts.Add(ic1);
            }
            if (itemexisted)
                repositoryItemCount.PreUpdate(itemCounts.ToArray());
            else
                repositoryItemCount.PreAdd(itemCounts.ToArray());

            //Write Seals
            IRepository<Tab_Seals> repositorySeals = new Repository<Tab_Seals>(context);
            Tab_Seals seal = null;
            DateTime now_date = new DateTime(now.Year, now.Month, now.Day);
            for (int i = 0; i < seals.Count(); i++)
            {
                seal = repositorySeals.GetSingle(s => s.SE_CartNo == cartNo && s.SE_SifNo == sifNo && s.SE_Sector == 0 && s.SE_SealNo.Equals(seals[i].SealNo) && s.SE_RecordedDate.Equals(now_date));
                if (seal == null)
                {
                    seal = new Tab_Seals();
                    seal.SE_SifNo = sifNo;
                    seal.SE_CartNo = cartNo;
                    seal.SE_Sector = 0;
                    seal.SE_DisplaySeq = (byte)i;
                    seal.SE_SealNo = seals[i].SealNo;
                    seal.SE_Status = "BO";
                    seal.SE_RecordedDate = now;
                    seal.SE_DeviceId = (short)deviceid;
                    seal.SE_DeviceSyncKey = deviceSyncKey;
                    repositorySeals.PreAdd(seal);
                }
            }

            context.SaveChanges();

        }

        public override GetPreordersResult GetPreorders(GetPreordersRequest request)
        {
            GetPreordersResult result = new GetPreordersResult();
            result.ResultState = "OK";
            DateTime departureDate = DateTime.Parse(request.DepartureDate);
            DateTime startDateTime = new DateTime(departureDate.Year, departureDate.Month, departureDate.Day);
            DateTime nextdayDateTime=startDateTime.AddDays(1);

            IRepository<Tab_PreorderPosCapture> repositoryPreorderCapture = new Repository<Tab_PreorderPosCapture>(context);
            IRepository<Tab_OrderHeader> repositoryOrderHeader = new Repository<Tab_OrderHeader>(context);
            IRepository<Tab_OrderLine> repositoryOrderLine = new Repository<Tab_OrderLine>(context);
            IRepository<Tab_PaymentLine> repositoryPaymentLine = new Repository<Tab_PaymentLine>(context);

            //var pcs = repositoryPreorderCapture.GetList(p => p.PC_FlightNoCap == request.FlightNo && p.PC_FromCap.Equals(request.From) && p.PC_ToCap.Equals(request.To) && (p.PC_DepartureDateCap >= startDateTime && p.PC_DepartureDateCap < nextdayDateTime));
            //Include return flights preorders
            var pcs = repositoryPreorderCapture.GetList(p => p.PC_ProcessStatus!="d" && ((p.PC_FromCap.Equals(request.From) && p.PC_ToCap.Equals(request.To)) || (p.PC_FromCap.Equals(request.To) && p.PC_ToCap.Equals(request.From))) && (p.PC_DepartureDateCap >= startDateTime && p.PC_DepartureDateCap < nextdayDateTime));
            if(pcs!=null)
            {
                result.Preorders = new List<Models.Preorder>();
                var phs = from pc in pcs
                          join oh in repositoryOrderHeader.GetAll()
                          on new {SN= pc.PC_SifNo, Sector= pc.PC_Sector, OrderNo= pc.PC_OrderNo } equals new {SN= oh.OH_SifNo, Sector= oh.OH_Sector, OrderNo= oh.OH_OrderNo }
                          select new{pc,oh};

                foreach(var ph in phs)
                {
                    //Get Preorder Header
                    Models.Preorder pr1 = new Models.Preorder();
                    pr1.SifNo = ph.pc.PC_SifNo;
                    pr1.FlightNo =ph.pc.PC_FlightNoCap;
                    pr1.From = ph.pc.PC_FromCap;
                    pr1.To = ph.pc.PC_ToCap;
                    pr1.DepartureDate = ph.pc.PC_DepartureDateCap;
                    pr1.SaleTotal = ph.oh.OH_Total;
                    pr1.PessengerName = ph.pc.PC_PaxName;
                    pr1.SeatNo = ph.oh.OH_Seat;
                    pr1.DeliverInfo = "";
                    pr1.Reference = ph.pc.PC_SifNo+"," +ph.pc.PC_Sector + "," +ph.pc.PC_OrderNo;

                    //Get OrderLines
                    pr1.OrderLines = (from ol in repositoryOrderLine.GetList(o => o.OL_SifNo.Equals(ph.oh.OH_SifNo) && o.OL_Sector == ph.oh.OH_Sector && o.OL_OrderNo == ph.oh.OH_OrderNo && o.OL_PriceEach>0)
                                     select new Models.OrderLine
                                     {
                                         OL_LineNo = ol.OL_LineNo,
                                         OL_Sku = ol.OL_Sku,
                                         OL_Quantity = ol.OL_Quantity,
                                         OL_PriceEach = ol.OL_PriceEach
                                     }).ToList();

                    //Get PaymentLines
                    pr1.PaymentLines = (from pl in repositoryPaymentLine.GetList(p => p.PA_SifNo.Equals(ph.oh.OH_SifNo) && p.PA_Sector == ph.oh.OH_Sector && p.PA_OrderNo == ph.oh.OH_OrderNo)
                                      select new Models.PaymentLine
                                      {
                                          PA_LineNo = pl.PA_LineNo,
                                          PA_TenderType = pl.PA_TenderType,
                                          PA_TenderSubType = pl.PA_TenderSubType,
                                          PA_Currency = pl.PA_Currency,
                                          PA_Amount = pl.PA_Amount,
                                          PA_AmountBase = pl.PA_AmountBase,
                                          PA_ExchangeRate = pl.PA_ExchangeRate
                                      }).ToList();

                    //Add into PreorderHeaders
                    result.Preorders.Add(pr1);
                }

            }

            return result;
        }
    }
}
