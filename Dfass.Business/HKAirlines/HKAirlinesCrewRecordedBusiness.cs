﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.HKAirlines.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class HKAirlinesCrewRecordedBusiness : Business<Tab_CrewRecorded>
    {
        public HKAirlinesCrewRecordedBusiness(DbContext context)
            : base(new Repository<Tab_CrewRecorded>(context))
        {
        }

        public override IList<Tab_CrewRecorded> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_CrewRecorded> crs = new List<Tab_CrewRecorded>();
            if (string.IsNullOrEmpty(xmlDocument))
                return crs;


            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);
            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_CrewRecorded");

            var nodes = document.SelectNodes("//ns:Tab_CrewRecorded", manager);
            //var nodes = document.SelectNodes("//Tab_FlightRecordeds/Tab_FlightRecorded");
            XmlNode xmlnode = null;
            foreach (XmlNode n in nodes)
            {
                Tab_CrewRecorded cr = new Tab_CrewRecorded();
                cr.CR_SifNo = n.SelectSingleNode("ns:CR_SifNo", manager).InnerText;
                cr.CR_Sector = byte.Parse(n.SelectSingleNode("ns:CR_Sector", manager).InnerText);
                cr.CR_CrewId = n.SelectSingleNode("ns:CR_CrewId", manager).InnerText;
                cr.CR_Position = n.SelectSingleNode("ns:CR_Position", manager).InnerText;

                xmlnode = n.SelectSingleNode("ns:CR_OriginalSector", manager);
                if (xmlnode != null)
                    cr.CR_OriginalSector = byte.Parse(xmlnode.InnerText);
                else
                    cr.CR_OriginalSector = cr.CR_Sector;

                cr.CR_RecordedDate = DateTime.Parse(n.SelectSingleNode("ns:CR_RecordedDate", manager).InnerText);
                cr.CR_DeviceId = short.Parse(n.SelectSingleNode("ns:CR_DeviceId", manager).InnerText);
                cr.CR_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:CR_DeviceSyncKey", manager).InnerText);

                crs.Add(cr);
            }

            return crs;
        }
        public override bool Add(IList<Tab_CrewRecorded> listCrewRecorded)
        {
            bool result = true;
            repository.Add(listCrewRecorded.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_CrewRecorded> listCrewRecorded)
        {
            return repository.PreAdd(listCrewRecorded.ToArray<Tab_CrewRecorded>());
        }
    }
}
