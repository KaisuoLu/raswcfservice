﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Requests;
using Dfass.Results;

namespace Dfass.Business
{
    public interface ISalesBusiness
    {
        bool CreateDatabase(string wcfPath, int airlinecode);

        bool SaveSales(UploadSalesRequest request);

        PICountResult PICount(PICountRequest request);

        PIUpdateCountResult PIUpdateCount(PIUpdateCountRequest request);

        PrintOnLaserResult PrintOnLaser(string url, PrintOnLaserRequest request);

        PrintSifOnLaserResult PrintSifOnLaser(string url, PrintSifOnLaserRequest request);

        DownloadDatabaseResult DownloadDatabase(string wcfPath, DownloadDatabaseRequest request);

        void UpdateItemCount(int deviceid, int deviceSyncKey, string sifNo, string cartNo, Dfass.Models.PICountInfo[] piCountInfos, Dfass.Models.SealInfo[] seals);

        GetPreordersResult GetPreorders(GetPreordersRequest request);
    }
}
