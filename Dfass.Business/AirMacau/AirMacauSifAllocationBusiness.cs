﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.AirMacau.DataAccess;
using Dfass.Repository;
using Dfass.Utils;

namespace Dfass.Business
{
    public class AirMacauSifAllocationBusiness : MalaysiaSifAllocationBusiness
    {
        object lockObject = new object();
        IRepository<Tab_SifAllocation> repository;

        public AirMacauSifAllocationBusiness()
        {
            repository = new Repository<Tab_SifAllocation>(new IRS_BO_AirMacauEntities());
        }

        public AirMacauSifAllocationBusiness(DbContext context)
        {
            repository = new Repository<Tab_SifAllocation>(context);
        }

        public override int GetSifNo(string sifprefix)
        {
            int sifno = -1;
            lock (lockObject)
            {
                Tab_SifAllocation sa = repository.GetSingle(a => a.SA_SifPrefix == sifprefix);
                if(sa!=null)
                    sa.SA_SifNo_int++;
                repository.Update(sa);

                sifno = sa.SA_SifNo_int;
            }
            return sifno;
        }
    }
}
