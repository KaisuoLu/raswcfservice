﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Business
{
    public interface ISifAllocationBusiness
     {
        int GetSifNo(string sifprefix);
    }
}
