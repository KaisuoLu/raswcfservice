﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Hawaiian.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class HawaiianFeedbacksBusiness : Business<Tab_FeedbackMessage>
    {
        public HawaiianFeedbacksBusiness(DbContext context)
            : base(new Repository<Tab_FeedbackMessage>(context))
        {
            
        }

        public override bool Add(IList<Tab_FeedbackMessage> listFeedbacks)
        {
            bool result = true;
            repository.Add(listFeedbacks.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_FeedbackMessage> listFeedbacks)
        {
            return repository.PreAdd(listFeedbacks.ToArray<Tab_FeedbackMessage>());
        }
    }
}
