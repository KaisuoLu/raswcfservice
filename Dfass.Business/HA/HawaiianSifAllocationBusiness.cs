﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Hawaiian.DataAccess;
using Dfass.Repository;
using Dfass.Utils;

namespace Dfass.Business
{
    public class HawaiianSifAllocationBusiness : MalaysiaSifAllocationBusiness
    {
        object lockObject = new object();
        IRepository<Tab_SifAllocation> repository;

        public HawaiianSifAllocationBusiness()
        {
            repository = new Repository<Tab_SifAllocation>(new IRS_BO_HAEntities());
        }

        public HawaiianSifAllocationBusiness(DbContext context)
        {
            repository = new Repository<Tab_SifAllocation>(context);
        }

        public override int GetSifNo(string sifprefix)
        {
            int sifno = -1;
            lock (lockObject)
            {
                Tab_SifAllocation sa = repository.GetSingle(a => a.SA_SifPrefix == sifprefix);
                if(sa!=null)
                    sa.SA_SifNo_int++;
                repository.Update(sa);

                sifno = sa.SA_SifNo_int;
            }
            return sifno;
        }
    }
}
