﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Hawaiian.DataAccess;
using Dfass.Repository;
using Dfass.Utils;

namespace Dfass.Business
{
    public class HawaiianItemCountBusiness : Business<Tab_ItemCount>
    {
        public HawaiianItemCountBusiness(DbContext context)
            : base(new Repository<Tab_ItemCount>(context))
        {
        }

        public override IList<Tab_ItemCount> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_ItemCount> ohs = new List<Tab_ItemCount>();
            if (string.IsNullOrEmpty(xmlDocument))
                return ohs;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_ItemCount");

            var nodes = document.SelectNodes("//ns:Tab_ItemCount", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_ItemCount ic = new Tab_ItemCount();
                ic.IC_SifNo = n.SelectSingleNode("ns:IC_SifNo", manager).InnerText;
                ic.IC_CartNo = n.SelectSingleNode("ns:IC_CartNo", manager).InnerText;
                ic.IC_Sku = n.SelectSingleNode("ns:IC_Sku", manager).InnerText;
                ic.IC_PackerOut = byte.Parse(n.SelectSingleNode("ns:IC_PackerOut", manager).InnerText);
                ic.IC_CrewBegin = ic.IC_PackerOut;
                //ic.IC_CrewBegin = short.Parse(n.SelectSingleNode("ns:IC_CrewBegin", manager).InnerText);
                ic.IC_CrewBeginRouteAdjust = short.Parse(n.SelectSingleNode("ns:IC_CrewBeginRouteAdjust", manager).InnerText);
                ic.IC_CrewSold = short.Parse(n.SelectSingleNode("ns:IC_CrewSold", manager).InnerText);
                ic.IC_CrewReturn = short.Parse(n.SelectSingleNode("ns:IC_CrewReturn", manager).InnerText);
                ic.IC_ReturnTotal = short.Parse(n.SelectSingleNode("ns:IC_ReturnTotal", manager).InnerText);
                ic.IC_ReturnDamaged = short.Parse(n.SelectSingleNode("ns:IC_ReturnDamaged", manager).InnerText);
                ic.IC_Replenish = short.Parse(n.SelectSingleNode("ns:IC_Replenish", manager).InnerText);
                ic.IC_PI_Mode = n.SelectSingleNode("ns:IC_PI_Mode", manager).InnerText == "1";
                ic.IC_RecordedDate = DateTime.Parse(n.SelectSingleNode("ns:IC_RecordedDate", manager).InnerText);
                ic.IC_Latest = Util.ParseNullableShort(n.SelectSingleNode("ns:IC_Latest", manager).InnerText);
                //ic.IC_DrawerNo = n.SelectSingleNode("ns:IC_DrawerNo", manager).InnerText;
                ic.IC_ReturnTotal = short.Parse(n.SelectSingleNode("ns:IC_ReturnTotal", manager).InnerText);
                ic.IC_LatestUplift = ic.IC_Replenish;
                ohs.Add(ic);
            }

            return ohs;
        }
        public override bool Add(IList<Tab_ItemCount> listItemCount)
        {
            bool result = true;
            repository.Add(listItemCount.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_ItemCount> listItemCount)
        {
            return repository.PreAdd(listItemCount.ToArray<Tab_ItemCount>());
        }

        public override bool PreUpdate(IList<Tab_ItemCount> listItemCount)
        {
            return repository.PreUpdate(listItemCount.ToArray<Tab_ItemCount>());
        }
    }
}
