﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Hawaiian.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class HawaiianPaymentLineBusiness : Business<Tab_PaymentLine>
    {
        public HawaiianPaymentLineBusiness(DbContext context)
            : base(new Repository<Tab_PaymentLine>(context))
        {
        }

        public override IList<Tab_PaymentLine> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_PaymentLine> pls = new List<Tab_PaymentLine>();
            if (string.IsNullOrEmpty(xmlDocument))
                return pls;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_PaymentLine");

            var nodes = document.SelectNodes("//ns:Tab_PaymentLine", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_PaymentLine pl = new Tab_PaymentLine();
                pl.PA_SifNo = n.SelectSingleNode("ns:PA_SifNo", manager).InnerText;
                pl.PA_Sector = byte.Parse(n.SelectSingleNode("ns:PA_Sector", manager).InnerText);
                pl.PA_OrderNo = short.Parse(n.SelectSingleNode("ns:PA_OrderNo", manager).InnerText);
                pl.PA_LineNo = byte.Parse(n.SelectSingleNode("ns:PA_LineNo", manager).InnerText);

                pl.PA_TenderType = n.SelectSingleNode("ns:PA_TenderType", manager).InnerText;
                XmlNode nodeTenderSubType = n.SelectSingleNode("ns:PA_TenderSubType", manager);
                pl.PA_TenderSubType = nodeTenderSubType==null? null: nodeTenderSubType.InnerText;
                pl.PA_Currency = n.SelectSingleNode("ns:PA_Currency", manager).InnerText;
                pl.PA_Amount = decimal.Parse(n.SelectSingleNode("ns:PA_Amount", manager).InnerText);
                pl.PA_AmountBase = decimal.Parse(n.SelectSingleNode("ns:PA_AmountBase", manager).InnerText);
                pl.PA_ExchangeRate = decimal.Parse(n.SelectSingleNode("ns:PA_ExchangeRate", manager).InnerText);
                pl.PA_CardHolderName = n.SelectSingleNode("ns:PA_CardHolderName", manager).InnerText;
                pl.PA_ProcessStatus = n.SelectSingleNode("ns:PA_ProcessStatus", manager).InnerText;
                pl.PA_HandheldMergeRowId = long.Parse(n.SelectSingleNode("ns:PA_HandheldMergeRowId", manager).InnerText);
                pl.PA_OriginalSector = pl.PA_Sector;
                pl.PA_DeviceId = short.Parse(n.SelectSingleNode("ns:PA_DeviceId", manager).InnerText);
                pl.PA_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:PA_DeviceSyncKey", manager).InnerText);

                pl.PA_CardNoBX = Encoding.ASCII.GetBytes(n.SelectSingleNode("ns:PA_CardNoBX", manager).InnerText);
                string edb = n.SelectSingleNode("ns:PA_ExpiryDateBX", manager).InnerText;
                pl.PA_ExpiryDateBX = string.IsNullOrEmpty(edb) ? (int?)null : int.Parse(Dfass.Utils.Util.DecryptNumber(edb));

                XmlNode nodeRefNo =n.SelectSingleNode("ns:PA_RefNo", manager);
                pl.PA_RefNo = nodeRefNo==null?null:nodeRefNo.InnerText;
                
                pls.Add(pl);
            }

            return pls;
        }
        public override bool Add(IList<Tab_PaymentLine> listPaymentLine)
        {
            bool result = true;
            repository.Add(listPaymentLine.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_PaymentLine> listPaymentLine)
        {
            return repository.PreAdd(listPaymentLine.ToArray<Tab_PaymentLine>());
        }
    }
}
