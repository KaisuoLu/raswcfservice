﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Singapore.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class SingaporeCartAssignmentBusiness :ICartAssignmentBusiness
    {
        IRepository<Tab_SetupKitMaster> repositoryKitMaster = null;
        IRepository<Tab_TransCartAssignment> repositoryCartAssignment = null;
        IRepository<Tab_TransCartAssignDetails> repositoryCartAssignmentDetails = null;

        object lockObject = new object();

        public SingaporeCartAssignmentBusiness()
        {
            DbContext context = new IRS_BO_SingaporeEntities();
            repositoryKitMaster = new Repository<Tab_SetupKitMaster>(context);
            repositoryCartAssignment = new Repository<Tab_TransCartAssignment>(context);
            repositoryCartAssignmentDetails = new Repository<Tab_TransCartAssignDetails>(context);
        }

        public SingaporeCartAssignmentBusiness(DbContext context)
        {
            repositoryKitMaster = new Repository<Tab_SetupKitMaster>(context);
            repositoryCartAssignment = new Repository<Tab_TransCartAssignment>(context);
            repositoryCartAssignmentDetails = new Repository<Tab_TransCartAssignDetails>(context);
        }

        public Dfass.Models.CartAssignment GetCartAssignment(string cartno)
        {
            //Dfass.Models.CartAssignment ca = null;
            lock (lockObject)
            {
                var _ca = (from cd in repositoryCartAssignmentDetails.GetList(cd1 => cd1.CD_CartNo.Equals(cartno))
                          join ca in repositoryCartAssignment.GetList(ca1=>(ca1.CA_Status.Equals("R") || ca1.CA_Status.Equals("P"))) on cd.CD_RecordNo equals ca.CA_RecordNo
                          select new Dfass.Models.CartAssignment
                          {
                              RecordNo = ca.CA_RecordNo,
                              FlightNo = ca.CA_DeptFltNo,
                              FlightFrom = ca.CA_FLTFrom,
                              FlightTo = ca.CA_FLTTo,
                              DepartureDateTime = ca.CA_DeptDate,
                              AircraftType = ca.CA_AircraftType,
                              KitCode = cd.CD_KITCode,
                              CartAssignmentStatus = ca.CA_Status
                          }).FirstOrDefault();

                return _ca;

             }
             
        }

        public void UpdateCartAssignmentStatus(long recordno, string status)
        {
            var ca1=repositoryCartAssignment.GetSingle(ca=>ca.CA_RecordNo.Equals(recordno));
            if(ca1!=null)
            {
                ca1.CA_Status=status;
                repositoryCartAssignment.Update(ca1);
            }
            
        }

        public void UpdateCartAssignmentStatus(string cartno, string status)
        {
            //Get CartAssignment
            var _ca = (from cd in repositoryCartAssignmentDetails.GetList(cd1 => cd1.CD_CartNo.Equals(cartno))
                       join ca in repositoryCartAssignment.GetList(ca1 => ca1.CA_Status.Equals("P")) on cd.CD_RecordNo equals ca.CA_RecordNo
                       select ca)
                       .FirstOrDefault();

            if (_ca != null)
            {
                _ca.CA_Status = status;
                repositoryCartAssignment.Update(_ca);
            }

        }
    }
}
