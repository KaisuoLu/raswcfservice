﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Business
{
    public interface ISifStatusBusiness<T> where T : class
    {
        //IList<Tab_SifStatus> ParseXmlDocument(string xmlDocument);

        bool Existed(string sifno);
        //bool Add(IList<Tab_SifStatus> listSifStatus);
        //bool PreAdd(DbContext context, IList<Tab_SifStatus> listSifStatus);
    }
}
