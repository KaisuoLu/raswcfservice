﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.NokScoot.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class NokScootSetupKitDetailBusiness : Business<Tab_SetupKitDetail>
    {

        public NokScootSetupKitDetailBusiness(DbContext context)
            : base(new Repository<Tab_SetupKitDetail>(context))
        {
        }

    }
}
