﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class MalaysiaFlightRecordedBusiness : Business<Tab_FlightRecorded>, IFlightRecordedBusiness
    {
        IFlightRecordedRepository repositoryFlightRecorded = null;

        public MalaysiaFlightRecordedBusiness(DbContext context)
        {
            repositoryFlightRecorded = new FlightRecordedRepository(context);
            base.repository = repositoryFlightRecorded;
        }

        public override IList<Tab_FlightRecorded> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_FlightRecorded> frs = new List<Tab_FlightRecorded>();
            if (string.IsNullOrEmpty(xmlDocument))
                return frs;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);
            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_FlightRecorded");

            var nodes = document.SelectNodes("//ns:Tab_FlightRecorded", manager);
            //var nodes = document.SelectNodes("//Tab_FlightRecordeds/Tab_FlightRecorded");

            foreach (XmlNode n in nodes)
            {
                Tab_FlightRecorded fr = new Tab_FlightRecorded();
                fr.FR_AirlineCode = short.Parse(n.SelectSingleNode("ns:FR_AirlineCode", manager).InnerText);
                fr.FR_SifNo = n.SelectSingleNode("ns:FR_SifNo", manager).InnerText;
                fr.FR_FlightCode = n.SelectSingleNode("ns:FR_FlightCode", manager).InnerText;
                fr.FR_FlightNo = short.Parse(n.SelectSingleNode("ns:FR_FlightNo", manager).InnerText);
                fr.FR_From = n.SelectSingleNode("ns:FR_From", manager).InnerText;
                fr.FR_To = n.SelectSingleNode("ns:FR_To", manager).InnerText;
                fr.FR_DepartureDate = DateTime.Parse(n.SelectSingleNode("ns:FR_DepartureDate", manager).InnerText);
                fr.FR_Sector = short.Parse(n.SelectSingleNode("ns:FR_Sector", manager).InnerText);
                fr.FR_PaxCount = short.Parse(n.SelectSingleNode("ns:FR_PaxCount", manager).InnerText);
                fr.FR_KitCode = n.SelectSingleNode("ns:FR_KitCode", manager).InnerText;
                fr.FR_Region = n.SelectSingleNode("ns:FR_Region", manager).InnerText;
                fr.FR_CartNo = n.SelectSingleNode("ns:FR_CartNo", manager).InnerText;
                fr.FR_OriginalSector = (byte)fr.FR_Sector;
                fr.FR_DeviceId = short.Parse(n.SelectSingleNode("ns:FR_DeviceId", manager).InnerText);
                fr.FR_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:FR_DeviceSyncKey", manager).InnerText);
                fr.FR_Hide = n.SelectSingleNode("ns:FR_Hide", manager).InnerText == "0" ? true : false;

                frs.Add(fr);
            }

            return frs;
        }
        public override bool Add(IList<Tab_FlightRecorded> listFlightRecorded)
        {
            bool result = true;
            repositoryFlightRecorded.Add(listFlightRecorded.ToArray());
            return result;
        }

        public override bool PreAdd(IList<Tab_FlightRecorded> listFlightRecorded)
        {
            return repositoryFlightRecorded.PreAdd(listFlightRecorded.ToArray<Tab_FlightRecorded>());
        }
    }
}
