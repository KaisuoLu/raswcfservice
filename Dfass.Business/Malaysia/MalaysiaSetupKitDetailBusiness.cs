﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Repository;

namespace Dfass.Business
{
    public class MalaysiaSetupKitDetailBusiness : Business<Tab_SetupKitDetail>, ISetupKitDetailBusiness
    {
        ISetupKitDetailRepository repositorySetupKitDetail = null;

        public MalaysiaSetupKitDetailBusiness(DbContext context)
        {
            repositorySetupKitDetail = new SetupKitDetailRepository(context);
        }

    }
}
