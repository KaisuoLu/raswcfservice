﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Repository;
using Dfass.Utils;

namespace Dfass.Business
{
    public class MalaysiaSifAllocationBusiness : ISifAllocationBusiness
    {
        ISifAllocationRepository repositorySifAllocation = null;
        object lockObject = new object();

        public MalaysiaSifAllocationBusiness()
        {
            repositorySifAllocation = new SifAllocationRepository(new IRS_BO_MalaysiaEntities());
        }

        public MalaysiaSifAllocationBusiness(DbContext context)
        {
            repositorySifAllocation = new SifAllocationRepository(context);
        }
        public virtual int GetSifNo(string sifprefix)
        {
            int sifno = -1;
            lock (lockObject)
            {
                Tab_SifAllocation sa = repositorySifAllocation.GetSingle(a => a.SA_SifPrefix == sifprefix);
                if (sa != null)
                    sa.SA_SifNo_int++;
                repositorySifAllocation.Update(sa);

                sifno = sa.SA_SifNo_int;
            }
            return sifno;
        }
    }
}
