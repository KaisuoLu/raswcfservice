﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dfass.SqliteDatabaseCreator
{
    public partial class FormCreator : Form
    {
        public FormCreator()
        {
            InitializeComponent();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            SalesServiceReference.ISalesService service = new SalesServiceReference.SalesServiceClient();
            int airlinecode = 1049;
            int.TryParse(this.comboBoxAirline.Text, out airlinecode);
            bool result=service.UpdateDatabase(airlinecode);
            string m="Failed to create!";
            if (result)
                m = "Successfully Created!";
            MessageBox.Show(m, "Create Database", MessageBoxButtons.OK);
        }

        private void FormCreator_Load(object sender, EventArgs e)
        {
            comboBoxAirline.SelectedIndex = 0;
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            //Dfass.Business.SalesBusiness2 sb = new Dfass.Malaysia.Business.SalesBusiness2();
            //sb.OpenDatabase();

        }

        private void buttonAddBlacklistFromSharedDb_Click(object sender, EventArgs e)
        {
            SalesServiceReference.ISalesService service = new SalesServiceReference.SalesServiceClient();
            var result = service.AddCreditCardBlacklist(new SalesServiceReference.AddCreditCardBlacklistRequest());

            string m = "Failed to add!";
            if (result.Status.Equals("OK"))
                m = "Successfully Executed! " + result.AddedBlacklistCount + " Credit Cards were added!";
            else
                m = result.Status + ":" + result.Description;
            MessageBox.Show(m, "Add Credit Card Blacklist", MessageBoxButtons.OK);
        }
    }
}
