﻿namespace Dfass.SqliteDatabaseCreator
{
    partial class FormCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCreate = new System.Windows.Forms.Button();
            this.comboBoxAirline = new System.Windows.Forms.ComboBox();
            this.buttonAddBlacklistFromSharedDb = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(353, 31);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(96, 23);
            this.buttonCreate.TabIndex = 0;
            this.buttonCreate.Text = "Create Database";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // comboBoxAirline
            // 
            this.comboBoxAirline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAirline.FormattingEnabled = true;
            this.comboBoxAirline.Items.AddRange(new object[] {
            "0025",
            "0026",
            "1026",
            "1031",
            "1049",
            "1053",
            "1055",
            "1056",
            "1057",
            "1058",
            "1060",
            "1062",
            "1063",
            "9008"});
            this.comboBoxAirline.Location = new System.Drawing.Point(31, 31);
            this.comboBoxAirline.Name = "comboBoxAirline";
            this.comboBoxAirline.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAirline.TabIndex = 1;
            // 
            // buttonAddBlacklistFromSharedDb
            // 
            this.buttonAddBlacklistFromSharedDb.Location = new System.Drawing.Point(288, 75);
            this.buttonAddBlacklistFromSharedDb.Name = "buttonAddBlacklistFromSharedDb";
            this.buttonAddBlacklistFromSharedDb.Size = new System.Drawing.Size(161, 23);
            this.buttonAddBlacklistFromSharedDb.TabIndex = 2;
            this.buttonAddBlacklistFromSharedDb.Text = "Add Blacklists from SharedDb";
            this.buttonAddBlacklistFromSharedDb.UseVisualStyleBackColor = true;
            this.buttonAddBlacklistFromSharedDb.Click += new System.EventHandler(this.buttonAddBlacklistFromSharedDb_Click);
            // 
            // FormCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 360);
            this.Controls.Add(this.buttonAddBlacklistFromSharedDb);
            this.Controls.Add(this.comboBoxAirline);
            this.Controls.Add(this.buttonCreate);
            this.Name = "FormCreator";
            this.Text = "SqliteDatabaseCreator";
            this.Load += new System.EventHandler(this.FormCreator_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.ComboBox comboBoxAirline;
        private System.Windows.Forms.Button buttonAddBlacklistFromSharedDb;
    }
}

