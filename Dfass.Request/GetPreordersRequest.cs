﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Requests
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public class GetPreordersRequest
    {
        [DataMember(Order = 1)]
        public int AirlineCode;

        [DataMember(Order = 2)]
        public int FlightNo;

        [DataMember(Order = 3)]
        public string From;

        [DataMember(Order = 4)]
        public string To;

        [DataMember(Order = 5)]
        public string DepartureDate;

    }
}