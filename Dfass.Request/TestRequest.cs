﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Requests
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public class TestRequest
    {
        [DataMember(Order = 1)]
        public string UserName;

        [DataMember(Order = 2)]
        public string UserPassword;

        [DataMember(Order = 3)]
        public int AirlineCode;

        [DataMember(Order = 4)]
        public string SifPrefix;

        [DataMember(Order = 5)]
        public SubTestRequest SubRequest;

    }
}
