﻿using Dfass.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Requests
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    [KnownType(typeof(PICountInfo[]))]
    [KnownType(typeof(PICountInfo))]
    [KnownType(typeof(SealInfo[]))]
    [KnownType(typeof(SealInfo))]
    public class PrintSifOnLaserRequest
    {
        [DataMember(Order = 1)]
        public int AirlineCode;

        [DataMember(Order = 2)]
        public string BondCode;
        
        [DataMember(Order = 3)]
        public string CartNo;

        [DataMember(Order = 4)]
        public string SifNo;
        
        [DataMember(Order = 5)]
        public string FlightPrefix;

        [DataMember(Order = 6)]
        public int FlightNumberInt;

        [DataMember(Order = 7)]
        public string FlightFrom;

        [DataMember(Order = 8)]
        public string FlightTo;

        [DataMember(Order = 9)]
        public string DepartureDate;

        [DataMember(Order = 10)]
        public string PackedBy;

        [DataMember(Order = 11)]
        public string KitCode;

        [DataMember(Order = 12)]
        public string HhcVersion;

        [DataMember(Order = 13)]
        public int HhcProtocal;

        [DataMember(Order = 14)]
        public int Copies;

        [DataMember(Order = 15)]
        public string SerialNo;

        [DataMember(Order = 16)]
        public PICountInfo[] PICountInfos;

        [DataMember(Order = 17)]
        public string DeviceId;

        [DataMember(Order = 18)]
        public string DeviceSyncKey;

        [DataMember(Order = 19)]
        public SealInfo[] SealInfos;

    }
}
