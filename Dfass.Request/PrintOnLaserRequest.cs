﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Requests
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public class PrintOnLaserRequest
    {
        [DataMember(Order = 1)]
        public string UserName;

        [DataMember(Order = 2)]
        public string UserPassword;

        [DataMember(Order = 3)]
        public int AirlineCode;

        [DataMember(Order = 4)]
        public string SifPrefix;

        [DataMember(Order = 5)]
        public string CartNo;
        
        [DataMember(Order = 6)]
        public string SifNo;
        
        [DataMember(Order = 7)]
        public string KitCode;

        [DataMember(Order = 8)]
        public string CompleteBy;

        [DataMember(Order = 9)]
        public int Copies;

        [DataMember(Order = 10)]
        public string SerialNo;

        [DataMember(Order = 11)]
        public string HhcVersion;

        [DataMember(Order = 12)]
        public int HhcProtocal;

    }
}
