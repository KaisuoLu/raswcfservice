﻿using Dfass.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Requests
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    [KnownType(typeof(Feedback[]))]
    [KnownType(typeof(Feedback))]
    public class UploadSalesRequest
    {
        [DataMember(Order = 1)]
        public string UserName;

        [DataMember(Order = 2)]
        public string UserPassword;

        [DataMember(Order = 3)]
        public int AirlineCode;

        [DataMember(Order = 4)]
        public string SifPrefix;

        [DataMember(Order = 5)]
        public string XmlFlightRecorded;

        [DataMember(Order = 6)]
        public string XmlOrderHeader;

        [DataMember(Order = 7)]
        public string XmlOrderLine;

        [DataMember(Order = 8)]
        public string XmlPaymentLine;

        [DataMember(Order = 9)]
        public string XmlItemCount;

        [DataMember(Order = 10)]
        public string XmlCrewRecorded;

        [DataMember(Order = 11)]
        public string XmlSeals;

        [DataMember(Order = 12)]
        public string XmlSifStatus;

        [DataMember(Order = 13)]
        public string XmlBillRecorded;

        [DataMember(Order = 14)]
        public string XmlCurrencyCountdown;

        [DataMember(Order = 15)]
        public string XmlPreorderPosCapture;

        [DataMember(Order = 16)]
        public Feedback[] Feedbacks;

    }
}
