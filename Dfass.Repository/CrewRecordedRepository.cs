﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class CrewRecordedRepository : Repository<Tab_CrewRecorded>, ICrewRecordedRepository
    {
        public CrewRecordedRepository(System.Data.Entity.DbContext context):base(context)
        {
        }
    }
}
