﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SealsRepository : Repository<Tab_Seals>, ISealsRepository
    {
        public SealsRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
