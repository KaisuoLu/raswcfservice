﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupRegionRepository : Repository<Tab_SetupRegion>, ISetupRegionRepository
    {
        public SetupRegionRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
