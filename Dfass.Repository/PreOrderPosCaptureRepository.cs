﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class PreOrderPosCaptureRepository : Repository<Tab_PreOrderPosCapture>, IPreOrderPosCaptureRepository
    {
        public PreOrderPosCaptureRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
