﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupKitMasterRepository : Repository<Tab_SetupKitMaster>, ISetupKitMasterRepository
    {
        public SetupKitMasterRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
