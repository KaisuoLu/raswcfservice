﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupStationRepository : Repository<Tab_SetupStation>, ISetupStationRepository
    {
        public SetupStationRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
