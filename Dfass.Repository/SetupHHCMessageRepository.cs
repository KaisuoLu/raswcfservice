﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupHHCMessageRepository : Repository<Tab_SetupHHCMessage>, ISetupHHCMessageRepository
    {
        public SetupHHCMessageRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
