﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SifStatusRepository : Repository<Tab_SifStatus>, ISifStatusRepository
    {
        public SifStatusRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
