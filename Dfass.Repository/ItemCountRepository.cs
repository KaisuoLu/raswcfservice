﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class ItemCountRepository : Repository<Tab_ItemCount>, IItemCountRepository
    {
        public ItemCountRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
