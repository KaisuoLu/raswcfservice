﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupKitDetailRepository : Repository<Tab_SetupKitDetail>, ISetupKitDetailRepository
    {
        public SetupKitDetailRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
