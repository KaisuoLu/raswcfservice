﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupCCTypeRepository : Repository<Tab_SetupCCType>, ISetupCCTypeRepository
    {
        public SetupCCTypeRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
