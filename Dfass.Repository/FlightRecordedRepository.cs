﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class FlightRecordedRepository : Repository<Tab_FlightRecorded>, IFlightRecordedRepository
    {
        public FlightRecordedRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
