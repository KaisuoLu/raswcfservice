﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupItemCategoryRepository : Repository<Tab_SetupItemCategory>, ISetupItemCategoryRepository
    {
        public SetupItemCategoryRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
