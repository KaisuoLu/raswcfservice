﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Dfass.Malaysia.DataAccess;

namespace Dfass.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        DbContext context = null;
        
        public Repository()
        {
            context = new IRS_BO_MalaysiaEntities();
        }
        

        public Repository(DbContext dbcontext)
        {
            context = dbcontext;
        }


        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list = null;
            if(context!=null)
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList<T>();
            }
            return list;
        }

        public virtual IList<T> GetList(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list = null;
            if (context != null)
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList<T>();
            }
            return list;
        }

        public virtual T GetSingle(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;
            if (context != null)
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                item = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .FirstOrDefault(where); //Apply where clause
            }
            return item;
        }

        public virtual bool Exist(Func<T, bool> where,
            params Expression<Func<T, object>>[] navigationProperties)
        {
            bool result = false;
            if (context != null)
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                result = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .Any(where);
            }
            return result;
        }

        public virtual void Add(params T[] items)
        {
            if (context != null)
            {
                
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.EntityState.Added;
                }
                context.SaveChanges();
            }
        }

        public virtual void Update(params T[] items)
        {
            if (context != null)
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public virtual void Delete(params T[] items)
        {
            if (context != null)
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }

        public virtual IList<T> Delete(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list = null;
            if (context != null)
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList<T>();

                foreach (T item in list)
                {
                    context.Entry(item).State = System.Data.EntityState.Deleted;
                }
                context.SaveChanges();
            }
            return list;
        }

        public DbContext OpenContext()
        {
            return new IRS_BO_MalaysiaEntities();
        }

        bool SaveChanges(DbContext context)
        {
            return context.SaveChanges() > 0;
        }        
        
        public bool PreAdd(params T[] items)
        {
            bool result = false;
            foreach (T item in items)
            {
                context.Entry(item).State = System.Data.EntityState.Added;
            }
            result = true;
            return result;
        }

        public bool PreUpdate(params T[] items)
        {
            bool result = false;
            foreach (T item in items)
            {
                context.Entry(item).State = System.Data.EntityState.Modified;
            }
            result = true;
            return result;
        }


    }
}