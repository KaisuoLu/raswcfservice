﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupSectorRepository : Repository<Tab_SetupSector>, ISetupSectorRepository
    {
        public SetupSectorRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
