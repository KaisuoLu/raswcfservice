﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupFlightMasterRepository : Repository<Tab_SetupFlightMaster>, ISetupFlightMasterRepository
    {
        public SetupFlightMasterRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
