﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SifAllocationRepository : Repository<Tab_SifAllocation>, ISifAllocationRepository
    {
        public SifAllocationRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }

    }
}
