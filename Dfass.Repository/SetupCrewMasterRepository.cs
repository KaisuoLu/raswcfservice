﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class SetupCrewMasterRepository : Repository<Tab_SetupCrewMaster>, ISetupCrewMasterRepository
    {
        public SetupCrewMasterRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
