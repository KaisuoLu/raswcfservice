﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public class OrderHeaderRepository : Repository<Tab_OrderHeader>, IOrderHeaderRepository
    {
        public OrderHeaderRepository(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
