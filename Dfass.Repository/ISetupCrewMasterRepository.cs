﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
namespace Dfass.Repository
{
    public interface ISetupCrewMasterRepository : IRepository<Tab_SetupCrewMaster>
    {
    }
}