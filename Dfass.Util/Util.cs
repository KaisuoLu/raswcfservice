﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Utils
{
    public static class Util
    {
        public static short? ParseNullableShort(this string value)
        {
            short shortValue;
            if (short.TryParse(value, out shortValue))
                return shortValue;
            return null;
        }

        public static int? ParseNullableInt(this string value)
        {
            double intValue;
            if (double.TryParse(value, out intValue))
                return (int)intValue;
            return null;
        }

        public static long? ParseNullableLong(this string value)
        {
            double longValue;
            if (double.TryParse(value, out longValue))
                return (long)longValue;
            return null;
        }

        public static DateTime? ParseNullableDateTime(this string value)
        {
            DateTime dtValue;
            if (DateTime.TryParse(value, out dtValue))
                return dtValue;
            return null;
        }
        public static string DBO_KEY = "initium";
        public static String DecryptNumber(string e_cipher) {
			if (e_cipher == null) 
                return null;
            StringBuilder sb = new StringBuilder(e_cipher.Length);
            for (int i = 0; i < e_cipher.Length; i++)
            {
                int intXOrValue1 = (int)e_cipher[i];
                int intXOrValue2 = (int)DBO_KEY[(i + 1) % DBO_KEY.Length];
                char c = (char)(intXOrValue1 ^ intXOrValue2);
                sb.Append(c);
            }
            
            return sb.ToString();

	    }


        public static String EncryptNumber(string e_cipher) {

            if (e_cipher == null) 
                return null;
            StringBuilder sb = new StringBuilder(e_cipher.Length);
            for (int i = 0; i < e_cipher.Length; i++)
            {
                int intXOrValue1 = (int)e_cipher[i];
                int intXOrValue2 = (int)DBO_KEY[(i + 1) % DBO_KEY.Length];
                char c = (char)(intXOrValue1 ^ intXOrValue2);
                sb.Append(c);
            }
            return sb.ToString();

	    }    
    }
}
