//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dfass.AviancaBogota.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_TransPreOrderMaster
    {
        public int PM_RecordNo { get; set; }
        public short PM_AirlineCode { get; set; }
        public string PM_Station { get; set; }
        public string PM_PreOrderNo { get; set; }
        public short PM_FlightNo { get; set; }
        public System.DateTime PM_DeptDate { get; set; }
        public string PM_FlightFrom { get; set; }
        public string PM_FlightTo { get; set; }
        public string PM_PaxFirstName { get; set; }
        public string PM_PaxLastName { get; set; }
        public string PM_SIFNo { get; set; }
        public Nullable<byte> PM_Sector { get; set; }
        public bool PM_Delivered { get; set; }
        public string PM_Remarks { get; set; }
        public System.DateTime PM_TransDate { get; set; }
        public string PM_TransID { get; set; }
        public string PM_Status { get; set; }
        public string PM_Source { get; set; }
        public Nullable<short> PM_OBFlightNo { get; set; }
        public string PM_OBStation { get; set; }
        public Nullable<System.DateTime> PM_OBDeptDate { get; set; }
        public string PM_OBFlightFrom { get; set; }
        public string PM_OBFlightTo { get; set; }
        public string PM_OrderSIFNo { get; set; }
        public Nullable<byte> PM_OrderSector { get; set; }
        public Nullable<short> PM_OrderNo { get; set; }
        public Nullable<System.DateTime> PM_OrderDeptDate { get; set; }
        public Nullable<short> PM_OrderFlightNo { get; set; }
        public string PM_OrderFrom { get; set; }
        public string PM_OrderTo { get; set; }
        public string PM_POSIF { get; set; }
        public Nullable<bool> PM_DVerified { get; set; }
        public Nullable<bool> PM_RVerified { get; set; }
        public string PM_RefNo { get; set; }
        public string PM_Email { get; set; }
        public string PM_Phone { get; set; }
        public string PM_Address { get; set; }
        public string PM_FFlyerNo { get; set; }
        public string PM_AeroplanNumber { get; set; }
        public Nullable<long> PM_AeroplanPoints { get; set; }
        public Nullable<long> PM_BonusAeroplanMiles { get; set; }
        public string PM_EmployeeID { get; set; }
    }
}
