//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dfass.AviancaBogota.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_SetupHHCMessage
    {
        public int HM_RecordNo { get; set; }
        public short HM_AirlineCode { get; set; }
        public string HM_MessageType { get; set; }
        public short HM_LangID { get; set; }
        public string HM_MessageContent { get; set; }
        public System.DateTime HM_TransDate { get; set; }
        public string HM_TransID { get; set; }
        public string HM_Status { get; set; }
    }
}
