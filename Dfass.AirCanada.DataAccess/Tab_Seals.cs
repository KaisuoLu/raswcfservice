//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dfass.AirCanada.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_Seals
    {
        public string SE_CartNo { get; set; }
        public string SE_SifNo { get; set; }
        public byte SE_Sector { get; set; }
        public byte SE_DisplaySeq { get; set; }
        public string SE_SealNo { get; set; }
        public string SE_SealColor { get; set; }
        public string SE_Status { get; set; }
        public byte SE_OriginalSector { get; set; }
        public System.DateTime SE_RecordedDate { get; set; }
        public short SE_DeviceId { get; set; }
        public int SE_DeviceSyncKey { get; set; }
    }
}
