﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Results
{
    [DataContract]
    public class PIUpdateCountResult
    {
        [DataMember]
        public int AirlineCode;

        [DataMember]
        public string SifNo;

        [DataMember]
        public string ResultState;

        [DataMember]
        public string ErrorDiscription;


    }
}
