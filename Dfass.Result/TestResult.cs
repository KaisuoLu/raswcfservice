﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Results
{
    [DataContract]
    public class TestResult
    {
        [DataMember(Order = 1)]
        public int SifNo;

        [DataMember(Order = 2)]
        public string FileName;

        [DataMember(Order = 3)]
        public long FileLength;

        [DataMember(Order = 4)]
        public string Base64Content;

        [DataMember(Order = 5)]
        public string Status;

        [DataMember(Order = 6)]
        public string Description;


    }
}
