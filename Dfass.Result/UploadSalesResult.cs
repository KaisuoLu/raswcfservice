﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Results
{
    
    //[DataContract(Namespace = "http://192.168.151.9/RAS_Android_WcfService/")]
    //[DataContract(Namespace = "http://10.100.243.251/RAS_Android_WcfService/")]
    //[DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    //[DataContract(Namespace = "http://192.168.1.6/WcfService/")]
    [DataContract]
    public class UploadSalesResult
    {
        [DataMember]
        public int SifNo;

        [DataMember]
        public bool Status;

        [DataMember]
        public string Description;


    }
}
