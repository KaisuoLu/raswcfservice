﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Dfass.Models;

namespace Dfass.Results
{
    [DataContract]
    public class GetImageUpdatesResult
    {
        [DataMember]
        public string ServerUpdateTime;

        [DataMember]
        public IList<ImageInfo> UpdateImages;

    }
}
