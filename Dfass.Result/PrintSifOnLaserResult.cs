﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Results
{
    [DataContract]
    public class PrintSifOnLaserResult
    {
        [DataMember(Order = 1)]
        public string SifNo;

        [DataMember(Order = 2)]
        public string KitCode;

        [DataMember(Order = 3)]
        public string ResultState;

        [DataMember(Order = 4)]
        public string ResultDiscription;

        [DataMember(Order = 5)]
        public int PrintInMinutes;
    }
}
