﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Results
{
    [DataContract]
    public class AddCreditCardBlacklistResult
    {
        [DataMember]
        public string Status;

        [DataMember]
        public string Description;

        [DataMember]
        public int AddedBlacklistCount;


    }
}
