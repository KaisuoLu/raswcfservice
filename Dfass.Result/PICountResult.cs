﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Results
{
    [DataContract]
    public class PICountResult
    {
        [DataMember]
        public int FlightNo;

        [DataMember]
        public string FlightFrom;

        [DataMember]
        public string FlightTo;

        [DataMember]
        public DateTime FlightDate;

        [DataMember]
        public string CartNo;

        [DataMember]
        public string KitCode;

        [DataMember]
        public IList<Dfass.Models.ItemCount> ItemCounts;

        [DataMember]
        public string ResultState;

        [DataMember]
        public string ErrorDiscription;


    }
}
