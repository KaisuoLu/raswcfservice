﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Dfass.Models;

namespace Dfass.Results
{
    [DataContract]
    public class GetPreordersResult
    {
        [DataMember]
        public int FlightNo;

        [DataMember]
        public string FlightFrom;

        [DataMember]
        public string FlightTo;

        [DataMember]
        public string FlightDate;

        [DataMember]
        public IList<Preorder> Preorders;

        [DataMember]
        public string ResultState;

        [DataMember]
        public string ErrorDiscription;

    }
}
