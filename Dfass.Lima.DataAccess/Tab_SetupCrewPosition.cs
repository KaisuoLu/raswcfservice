//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dfass.Lima.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_SetupCrewPosition
    {
        public int CP_RecordNo { get; set; }
        public short CP_AirlineCode { get; set; }
        public string CP_PositionCode { get; set; }
        public string CP_PositionName { get; set; }
        public System.DateTime CP_TransDate { get; set; }
        public string CP_TransID { get; set; }
        public string CP_Status { get; set; }
    }
}
