//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dfass.HKAirlines.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_ItemCount
    {
        public string IC_SifNo { get; set; }
        public string IC_CartNo { get; set; }
        public string IC_Sku { get; set; }
        public short IC_PackerOut { get; set; }
        public short IC_CrewBegin { get; set; }
        public short IC_CrewBeginRouteAdjust { get; set; }
        public short IC_CrewSold { get; set; }
        public short IC_CrewSoldRoute { get; set; }
        public short IC_CrewReturnRouteAdjust { get; set; }
        public short IC_CrewReturn { get; set; }
        public short IC_ReturnTotal { get; set; }
        public short IC_ReturnDamaged { get; set; }
        public short IC_Replenish { get; set; }
        public bool IC_PI_Mode { get; set; }
        public short IC_CrewSoldUnit1 { get; set; }
        public short IC_CrewSoldUnit2 { get; set; }
        public System.DateTime IC_RecordedDate { get; set; }
        public Nullable<short> IC_Latest { get; set; }
        public Nullable<short> IC_LatestUplift { get; set; }
    }
}
