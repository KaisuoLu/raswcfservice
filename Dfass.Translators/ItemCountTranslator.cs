﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dfass.Models;
using System.Reflection;

namespace Dfass.Translators
{
    public class ItemCountTranslator
    {
        
        public static Dfass.Models.ItemCount TranslateToModel<T>(T ic)
        {
            ItemCount itemcount = new ItemCount();
            Type entityType = typeof(T);
            PropertyInfo proInfo = entityType.GetProperty("IC_SifNo");
            itemcount.SifNo = proInfo.GetValue(ic).ToString();
            /*
            itemcount.CartNo = ic.IC_CartNo;
            itemcount.Sku = ic.IC_Sku;
            itemcount.Name = "";
            itemcount.PackerOut = ic.IC_PackerOut;
            itemcount.CrewBegin = ic.IC_CrewBegin;
            itemcount.CrewBeginRouteAdjust = ic.IC_CrewBeginRouteAdjust;
            itemcount.CrewSold = ic.IC_CrewSold;
            itemcount.CrewSoldRoute = ic.IC_CrewSoldRoute;
            itemcount.CrewReturn = ic.IC_CrewReturn;
            itemcount.CrewReturnRouteAdjust = ic.IC_CrewBeginRouteAdjust;
            itemcount.ReturnTotal = ic.IC_ReturnTotal;
            itemcount.ReturnDamaged = ic.IC_ReturnDamaged;
            itemcount.Replenish = ic.IC_Replenish;
            itemcount.PI_Mode = ic.IC_PI_Mode;
            itemcount.CrewSoldUnit1 = ic.IC_CrewSoldUnit1;
            itemcount.CrewSoldUnit2 = ic.IC_CrewSoldUnit2;
            itemcount.RecordedDate = ic.IC_RecordedDate;
            itemcount.Latest = ic.IC_Latest??0;
            itemcount.LatestUplift = ic.IC_LatestUplift??0;
             */ 
            return itemcount;
        }
        
    }
}
