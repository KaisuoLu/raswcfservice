﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Dfass.Models;
using Dfass.Requests;
using Dfass.Results;

namespace Dfass.RasWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    //[ServiceContract(Namespace = "http://192.168.151.9/RAS_Android_WcfService/")]
    //[ServiceContract(Namespace = "http://192.168.1.6/WcfService/")]
    //[ServiceContract(Namespace = "http://WWW.DFASS.COM/WcfService/")]
    [ServiceContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public interface ISalesService
    {

        [OperationContract]
        UploadSalesResult UploadSales(UploadSalesRequest request);
        
        [OperationContract]
        UploadSalesResult AirlineUploadSales(UploadSalesRequest request);

        [OperationContract]
        UploadSalesResult UploadSales2(UploadSalesRequest request);

        [OperationContract]
        DownloadDatabaseResult DownloadDatabase(DownloadDatabaseRequest request);

        [OperationContract]
        bool UpdateDatabase(int airlinecode);

        [OperationContract]
        AddCreditCardBlacklistResult AddCreditCardBlacklist(AddCreditCardBlacklistRequest request);

        [OperationContract]
        PICountResult PICount(PICountRequest request);

        [OperationContract]
        PIUpdateCountResult PIUpdateCount(PIUpdateCountRequest request);

        [OperationContract]
        PrintOnLaserResult PrintOnLaser(PrintOnLaserRequest request);

        [OperationContract]
        PrintSifOnLaserResult PrintSifOnLaser(PrintSifOnLaserRequest request);



        [OperationContract]
        GetImageUpdatesResult GetImageUpdates(GetImageUpdatesRequest request);

        [OperationContract]
        GetPreordersResult GetPreorders(GetPreordersRequest request);


        [OperationContract]
        DownloadDatabaseResult[] Test(int airlinecode, string sifno, ProductItemUpdateCountRequest[] requests);

        [OperationContract]
        TestResult Test2(TestRequest request);

        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
