﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Dfass.Models;
using Dfass.Business;
using System.IO;
using Dfass.Results;
using Dfass.Requests;

using Dfass.Malaysia.DataAccess;


namespace Dfass.RasWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class SalesService : ISalesService
    {
        log4net.ILog log = null;
        ISalesBusiness businessSales = null;
        ISifAllocationBusiness businessSifAllocation = null;
        object lockDownload = new object();
        public SalesService()
        {
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        private void InitializeSalesBusiness(int airlinecode)
        {
            switch (airlinecode)
            {
                case 25:
                case 26:
                    businessSales = new SingaporeSalesBusiness();
                    break;
                case 1026:
                    businessSales = new HKAirlinesSalesBusiness();
                    break;
                case 1031://NokScoot airline
                    businessSales = new NokScootSalesBusiness();
                    break;
                case 1049://Malaysia airline
                    businessSales = new MalaysiaSalesBusiness();
                    break;

                case 1053://Air Macau airline
                    businessSales = new AirMacauSalesBusiness();
                    break;
                case 1055:
                    businessSales = new HawaiianSalesBusiness();
                    break;

                case 1056:
                    businessSales = new AviancaBogotaSalesBusiness();
                    break;

                case 1057://VietJet
                    businessSales = new VietJetSalesBusiness();
                    break;

                case 1058://AirCanada
                    businessSales = new AirCanadaSalesBusiness();
                    break;
                case 1060://ACBOB
                    businessSales = new ACBOBSalesBusiness();
                    break;

                case 1062:
                    businessSales = new SanJoseSalesBusiness();
                    break;
                case 1063:
                    businessSales = new LimaSalesBusiness();
                    break;
                case 1065:
                    businessSales = new VirginSalesBusiness();
                    break;
                case 9008:
                    businessSales = new DemoSalesBusiness();
                    break;
                default:
                    throw new Exception(string.Format("Airline {0} does not exist!", airlinecode));
            }

        }

        public UploadSalesResult UploadSales(UploadSalesRequest request)
        {
            InitializeSalesBusiness(request.AirlineCode);
            UploadSalesResult result = new UploadSalesResult();
            try
            {
                result.Status = businessSales.SaveSales(request);

            }
            catch (Exception ex)
            {
                log.Error(ex.Message +"\r\n");
                log.Info(request.XmlFlightRecorded);
                log.Info(request.XmlFlightRecorded);
                log.Info(request.XmlOrderHeader);
                log.Info(request.XmlOrderLine);
                log.Info(request.XmlPaymentLine);
                log.Info(request.XmlItemCount);
                log.Info(request.XmlCrewRecorded);
                log.Info(request.XmlSeals);
                log.Info(request.XmlSifStatus);
                throw ex;
            }
            return result;
        }

        public UploadSalesResult AirlineUploadSales(UploadSalesRequest request)
        {

            UploadSalesResult result = new UploadSalesResult();
            try
            {
                InitializeSalesBusiness(request.AirlineCode);

                result.Status = businessSales.SaveSales(request);

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

                log.Info(request.AirlineCode + request.SifPrefix);
                log.Info(request.XmlFlightRecorded);
                log.Info(request.XmlFlightRecorded);
                log.Info(request.XmlOrderHeader);
                log.Info(request.XmlOrderLine);
                log.Info(request.XmlPaymentLine);
                log.Info(request.XmlItemCount);
                log.Info(request.XmlCrewRecorded);
                log.Info(request.XmlSeals);
                log.Info(request.XmlSifStatus);                
                throw ex;
            }
            return result;
        }

        private void WriteLogger(string airlineCode, string sifPrefix, string xmlContent)
        {
            var root = System.Configuration.ConfigurationManager.AppSettings["LogFilesRootPath"];
            DateTime now = DateTime.Now;
            string filepathname = Path.Combine(root, DateTime.Now.ToLongTimeString()+"_"+airlineCode+"_"+sifPrefix+"_"+xmlContent.GetType().Name+".log");
            using (StreamWriter writer = new StreamWriter(filepathname, true))
            {
                writer.Write(xmlContent);
            }
            
        }

        public UploadSalesResult UploadSales2(UploadSalesRequest request)
        {
            InitializeSalesBusiness(request.AirlineCode);
            UploadSalesResult result = new UploadSalesResult();
            try
            {
                result.Status=businessSales.SaveSales(request);

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Info(request.XmlFlightRecorded);
                log.Info(request.XmlFlightRecorded);
                log.Info(request.XmlOrderHeader);
                log.Info(request.XmlOrderLine);
                log.Info(request.XmlPaymentLine);
                log.Info(request.XmlItemCount);
                log.Info(request.XmlCrewRecorded);
                log.Info(request.XmlSeals);
                log.Info(request.XmlSifStatus);
                throw ex;
            }
            return result;
        }


        public DownloadDatabaseResult DownloadDatabase(DownloadDatabaseRequest request)
        {
            lock(lockDownload)
            {
                InitializeSalesBusiness(request.AirlineCode);
                string pathWcf = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                return businessSales.DownloadDatabase(pathWcf, request);
            }
        }

        public bool UpdateDatabase(int airlinecode)
        {
            InitializeSalesBusiness(airlinecode);

            return businessSales.CreateDatabase(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath,airlinecode);
        }

        public AddCreditCardBlacklistResult AddCreditCardBlacklist(AddCreditCardBlacklistRequest request)
        {
            SharedDbSetupCreditCardBlacklistBusiness business = new SharedDbSetupCreditCardBlacklistBusiness();
            return business.AddCreditCardBlacklist(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath,request);
        }

        public PICountResult PICount(PICountRequest request)
        {
            InitializeSalesBusiness(request.AirlineCode);
            return businessSales.PICount(request);
        }

        public PIUpdateCountResult PIUpdateCount(PIUpdateCountRequest request)
        {
            InitializeSalesBusiness(request.AirlineCode);

            //request.PICountInfos = picountinfos;
            return businessSales.PIUpdateCount(request);
        }

        public PrintOnLaserResult PrintOnLaser(PrintOnLaserRequest request)
        {
            businessSales = new SalesBusiness();
            var url = System.Configuration.ConfigurationManager.AppSettings["PrintOnLaserUrl"];

            return businessSales.PrintOnLaser(url, request);
        }

        public PrintSifOnLaserResult PrintSifOnLaser(PrintSifOnLaserRequest request)
        {
            InitializeSalesBusiness(request.AirlineCode);
            var url = System.Configuration.ConfigurationManager.AppSettings["PrintSifOnLaserUrl"];

            return businessSales.PrintSifOnLaser(url, request);

        }

        public GetImageUpdatesResult GetImageUpdates(GetImageUpdatesRequest request)
        {
            //Get airline iamge path
            GetImageUpdatesResult result = new GetImageUpdatesResult();
            var root=System.Configuration.ConfigurationManager.AppSettings["ProductImageRootPath"];
            string imagepath = Path.Combine(root, request.AirlineCode.ToString("D4"));

            //Select images' last access time after last change time.
            DirectoryInfo di=new DirectoryInfo(imagepath);
            result.UpdateImages = di.GetFiles().Where(f => f.LastAccessTime > DateTime.Parse(request.LastSyncTime)).Select(s => new ImageInfo() {
                ProductCode=s.Name.Replace(s.Extension,""),
                Image=s.Name,
                Lag="1"
            }).ToList<ImageInfo>();

            //when there is new images/chagnes, write back LastSync time.
            if (result.UpdateImages.Count > 0)
                result.ServerUpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            else
                result.ServerUpdateTime = request.LastSyncTime;
            return result;
        }

        public GetPreordersResult GetPreorders(GetPreordersRequest request)
        {
            GetPreordersResult result = null;
            InitializeSalesBusiness(request.AirlineCode);
            try
            {
                result = businessSales.GetPreorders(request);
            }
            catch(Exception ex)
            {
                if (result == null)
                    result = new GetPreordersResult();
                result.ResultState = "ERROR";
                result.ErrorDiscription = ex.Message;
            }

            return result;
        }


        public DownloadDatabaseResult[] Test(int airlinecode, string sifno, ProductItemUpdateCountRequest[] requests)
        {
            IPaymentLineBusiness bsPL=new MalaysiaPaymentLineBusiness(new IRS_BO_MalaysiaEntities());
            Tab_PaymentLine pl = bsPL.GetSingle(p => p.PA_SifNo == "ZZZ 100062");
            string cdn = Encoding.ASCII.GetString(pl.PA_CardNoBX);
            if (cdn.IndexOf('\0') > 0)
                cdn = cdn.Substring(0, cdn.IndexOf("\0"));
            string cdn2 = Dfass.Utils.Util.DecryptNumber(cdn);

            string cc = cdn2;

            DownloadDatabaseResult d1=new DownloadDatabaseResult();
            //d1.FileName=request.SifPrefix;
            foreach (var r in requests)
            {
                d1.Description = r.ToString();
            }
            DownloadDatabaseResult d2 = new DownloadDatabaseResult();
            d2.FileName = "Test2";
            DownloadDatabaseResult d3 = new DownloadDatabaseResult();
            d3.FileName = "Test3";
            DownloadDatabaseResult[] ss = new DownloadDatabaseResult[] { d1,d2,d3 };
            return ss;
        }

        public TestResult Test2(TestRequest request)
        {
            TestResult result = new TestResult();
            if (request.SubRequest != null)
                result.Status = "Has sub request";
            else
                result.Status = "ERROR";
            return result;
        }
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
