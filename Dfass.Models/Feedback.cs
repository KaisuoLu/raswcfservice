﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public class Feedback
    {
        [DataMember(Order=1)]
        public string SifNo;

        [DataMember(Order = 2)]
        public int Sector;

        [DataMember(Order = 3)]
        public string Type;

        [DataMember(Order = 4)]
        public string Area;

        [DataMember(Order = 5)]
        public string SubArea;

        [DataMember(Order = 6)]
        public string Message;

        [DataMember(Order = 7)]
        public string CrewId;

        [DataMember(Order = 8)]
        public string EnterDateTime;

        [DataMember(Order = 9)]
        public int DeviceSyncKey;

    }
}
