﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public class SealInfo
    {
        [DataMember(Order=1)]
        public string CartNo;

        [DataMember(Order=2)]
        public string SifNo;

        [DataMember(Order = 3)]
        public int Sector;

        [DataMember(Order = 4)]
        public int DisplaySeq;

        [DataMember(Order = 5)]
        public string SealNo;

        [DataMember(Order = 6)]
        public string Status;

        [DataMember(Order = 7)]
        public string Type;

        [DataMember(Order = 8)]
        public string Color;

    }
}
