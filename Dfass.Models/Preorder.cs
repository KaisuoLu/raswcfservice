﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    public class Preorder
    {
        public string SifNo { get; set; }
        public int FlightNo { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime DepartureDate { get; set; }
        public decimal SaleTotal { get; set; }
        public string PessengerName { get; set; }
        public string SeatNo { get; set; }
        public string DeliverInfo { get; set; }
        public string Reference { get; set; }

        public IList<OrderLine> OrderLines { get; set; }

        public IList<PaymentLine> PaymentLines { get; set; }
    }
}
