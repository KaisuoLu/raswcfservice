﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    [DataContract]
    public class ImageInfo
    {
        [DataMember]
        public string ProductCode;

        [DataMember]
        public string CatalogueCode;

        [DataMember]
        public byte Desc;

        [DataMember]
        public string Image;


        [DataMember]
        public string Thumb;

        [DataMember]
        public string Lag;

    }
}
