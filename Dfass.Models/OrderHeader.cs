﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    public class OrderHeader
    {
        public string OH_SifNo { get; set; }
        public byte OH_Sector { get; set; }
        public short OH_OrderNo { get; set; }
        public decimal OH_Total { get; set; }
        public bool OH_Voided { get; set; }
        public string OH_CrewSale_CrewId { get; set; }
        public System.DateTime OH_OrderTime { get; set; }
        public bool OH_Credit { get; set; }
        public byte OH_OriginalSector { get; set; }
        public string OH_Seat { get; set; }
        public string OH_Type { get; set; }
        public short OH_DeviceId { get; set; }
        public int OH_DeviceSyncKey { get; set; }
        public string OH_Passport { get; set; }
        public string OH_Service { get; set; }
        public string OH_PaxName { get; set; }
        public string OH_PointCardNo { get; set; }
        public string OH_PointCardHolderName { get; set; }
        public Nullable<System.DateTime> OH_PointCardExpireDate { get; set; }
        public string OH_PointCardType { get; set; }
        public string OH_PointCardStatus { get; set; }
        public Nullable<long> OH_PointCardPoints { get; set; }
        public Nullable<long> OH_PointCardVoucherPoints { get; set; }
        public string OH_PaxClass { get; set; }
        public string OH_PhoneNo { get; set; }

        public OrderHeader()
        {
        }
    }
}
