﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    public class CartAssignment
    {
        public long RecordNo;
        public string FlightNo;
        public string FlightFrom;
        public string FlightTo;
        public DateTime DepartureDateTime;
        public string AircraftType;
        public string KitCode;
        public string CartAssignmentStatus;

        public CartAssignment()
        {
        }

    }
}
