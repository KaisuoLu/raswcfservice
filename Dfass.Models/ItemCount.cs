﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    [DataContract]
    public class ItemCount
    {
        [DataMember]
        public string SifNo;

        [DataMember]
        public string CartNo;

        [DataMember]
        public byte CategoryId;

        [DataMember]
        public string CategoryName;


        [DataMember]
        public string Sku;

        [DataMember]
        public string Name;

        [DataMember]
        public short PackerOut;

        [DataMember]
        public short CrewBegin;

        [DataMember]
        public short CrewBeginRouteAdjust;

        [DataMember]
        public short CrewSold;

        [DataMember]
        public short CrewSoldRoute;

        [DataMember]
        public short CrewReturnRouteAdjust;

        [DataMember]
        public short CrewReturn;

        [DataMember]
        public short ReturnTotal;

        [DataMember]
        public short ReturnDamaged;

        [DataMember]
        public short Replenish;

        [DataMember]
        public bool PI_Mode;

        [DataMember]
        public short CrewSoldUnit1;

        [DataMember]
        public short CrewSoldUnit2;

        [DataMember]
        public System.DateTime RecordedDate;

        [DataMember]
        public short Latest;

        [DataMember]
        public short LatestUplift;

    }
}
