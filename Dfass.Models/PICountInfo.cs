﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Models
{
    [DataContract(Namespace = "http://67.22.98.226/RAS_Android_WcfService/")]
    public class PICountInfo
    {
        [DataMember(Order=1)]
        public string Sku;

        [DataMember(Order = 2)]
        public int PackerOut;

        [DataMember(Order = 3)]
        public int ReturnTotal;

        [DataMember(Order = 4)]
        public int ReturnDamaged;

        [DataMember(Order = 5)]
        public int Replenish;

        [DataMember(Order = 6)]
        public int Latest;

    }
}
