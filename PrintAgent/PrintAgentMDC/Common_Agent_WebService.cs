﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PrintAgentMDC
{

	partial class Form1 : Form
	{

		enum DeviceRequest
		{
			end, getPrintRequest, setPrinterStatus
		}

		enum ServerResponse
		{
			end, serverMessage, wakeUp, printSif, printPI_simple, serverRedirect, notUsed_HHC_uploadReport
		}

	}
}
