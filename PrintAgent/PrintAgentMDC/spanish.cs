﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrintAgentMDC
{
	class Spanish
	{

		public static string[] phrase = new string[74];

		static Spanish()
		{
			phrase[0] = "Formulario de ventas y Inventario.";
			phrase[1] = "PI hoja ";
			phrase[2] = "múltiples sifs fueron cargados de la HHC";
			phrase[3] = "Reporte cargado de HHC";
			phrase[4] = "Carrito #";
			phrase[5] = "Ref #";
			phrase[6] = "agente";
			phrase[7] = "HHC";
			phrase[8] = "impreso";
			phrase[9] = "Completado por";
			phrase[10] = "vuelo";
			phrase[11] = "Preparado por";
			phrase[12] = "en";
			phrase[13] = "nombre";
			phrase[14] = "Siguiente No. de  SIF.";
			phrase[15] = "fecha";
			phrase[16] = "Reposicion de FH no.";
			phrase[17] = "firma";
			phrase[18] = "FH fecha";
			phrase[19] = "Notas";
			phrase[20] = "página";
			phrase[21] = "Lista de ventas";
			phrase[22] = "Lista de pagos";
			phrase[23] = "Vuelo de licitación";
			phrase[24] = "Planilla de sif";
			phrase[25] = "Las ventas cont / ...";
			phrase[26] = "Planilla cont / ...";
			phrase[27] = "tarjetas de crédito y efectivo cont / ...";
			phrase[28] = "N ° de vuelo.";
			phrase[29] = "Desde / hacia";
			phrase[30] = "salida";
			phrase[31] = "vendedores";
			phrase[32] = "asiento";
			phrase[33] = "tipo";
			phrase[34] = "N º de pedido";
			phrase[35] = "tiempo";
			phrase[36] = "Cantidad";
			phrase[37] = "sku";
			phrase[38] = "descripción";
			phrase[39] = "cada";
			phrase[40] = "total";
			phrase[41] = "Cantidad";
			phrase[42] = "cantidad de base";
			phrase[43] = "artículo #";
			phrase[44] = "precio";
			phrase[45] = "abordo";
			phrase[46] = "comenzar";
			phrase[47] = "venta";
			phrase[48] = "final";
			phrase[49] = "volver";
			phrase[50] = "damages"; //"daños y perjuicios";
			phrase[51] = "uplift"; //"levantamiento(reposicion)";
			phrase[52] = "total vendido";
			phrase[53] = "Conteo de retorno";
			phrase[54] = "PI conteo";
			phrase[55] = "despojado";
			phrase[56] = "Sector 1 cuenta";
			phrase[57] = "Sector 2 cuentas";
			phrase[58] = "categoría";
			phrase[59] = "ubicación";
			phrase[60] = "transacciones de tarjetas de crédito";
			phrase[61] = "transacciones en efectivo";
			phrase[62] = "total de transacciones";
			phrase[63] = "tarjetas de crédito";
			phrase[64] = "lellos de clausura";
			phrase[65] = "Sellos";
			phrase[66] = "reembolso";
			phrase[67] = "venta";
			phrase[68] = "vuelo registrado";
			phrase[69] = "carro";
			phrase[70] = "cupón";
			phrase[71] = "tarjeta de crédito";
			phrase[72] = "efectivo";
		}

	}
}
