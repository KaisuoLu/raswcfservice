﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PrintAgentMDC
{

	static class a
	{

		static Bondroom _bondroom;

		static public int groupCountItems;
		static public bool ide, piSoldFigures, piCartInboundFigures, spanish;
		static public DocumentType documentType;
		static public List<Seller> sellers = new List<Seller>(5);
		static public List<CurrencyTotal> sifCurrencyTotalRows = new List<CurrencyTotal>(5);

		public static Bondroom bondroom { get { lock (lockObject) { return _bondroom; } } set { lock (lockObject) { _bondroom = value; } } }
		static public object lockObject = new object();

		static public bool specialLocationSorting
		{
			get
			{
				switch (bondroom.airline)
				{
					case Airline.airAsiaJapan_DF_2908://WARNING: gets mapped to 2903 because of shared agent
					case Airline.airAsiaJapan_DP_2903:
					case Airline.peach_DF_904://WARNING: gets mapped to 903 because of shared agent
					case Airline.peach_DP_903:
						return true;
					default: 
                        return false;
				}
			}
		}

		static public string ohTypeText(string ohType, bool credit)
		{
			if (credit) return phrase(66, "Refund");
			switch (ohType)
			{
				case "S": return phrase(67, "Sale");
				case "P": return "PO";
                case "O": return "Tr.I";
                case "I": return "Tr.O";
				default: return ohType;
			}
		}

		static public string phrase(string phrase)
		{
			return phrase;//ok
		}

		static public string phrase(int phraseId, string phrase)
		{
			if (spanish) return Spanish.phrase[phraseId];
			//if(ide) return "#" + phraseId;
			return phrase;
		}

		static public bool byCategory
		{
			get
			{

				switch (bondroom.airline)
				{

					//sync with Agent/POS (only required for DF?)
					case Airline.scootBOB:
					case Airline.jetstar_bob:

					case Airline.guamFreedom_dutyfree_1001:
					case Airline.tiger_dutyFree:
					case Airline.jetstar_dutyfree:
					case Airline.airAsiaX_48:
					case Airline.airAsiaKL_1004:
					case Airline.airAsiaX_merch_1002:
					case Airline.tonlesap_1008:
					case Airline.palau_1009:
					case Airline.aerosur_24:
					case Airline.airAsiaJapan_DP_2903:
					case Airline.fat_1012:
					case Airline.airAsiaJapan_DF_2908:
					case Airline.peach_DP_903:
					case Airline.gulfair_32:
					case Airline.garuda_idr_DP_1014:
					case Airline.garuda_idr_DF_1015:
					case Airline.mru_1016:
                    case Airline.mrd_1017:
					case Airline.usairways:
                    case Airline.HongKongDF_1026:
                    case Airline.Virgin_1030:
                    case Airline.aircanada_14:
                    case Airline.RoyalJordanian1041:
                    case Airline.AirAsiaPhilippines1043:
                    case Airline.RwandAir1045:
                    case Airline.TransAsia1046:
                    case Airline.RoyalWings1047:
                    case Airline.Malaysia1049:
                    case Airline.AirMacau1053:
						return false;

					default:
						return true;
				}
			}
		}

		static public bool byLocation
		{
			get
			{
				return !byCategory;
			}
		}

		[STAThread]
		static void Main(string[] args)
		{

			ide = args != null && args.Length != 0 && args[0] == "ide";

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1());
		}

	}

	public class Sale
	{

		public string sifNo;
		public string cart;
		public bool voided;
		public bool credit;
		public int sector;//todo3 AT change to sector not sector orig

		public string flightPrefix, from, to;
		public int flightNumberInt;
		public DateTime departureDate;

		public string kitCode;
		public string kitPOSDesc;
		public string seat;
		public string ohType;
		public int orderNo;
		public int quantity;
		public string olType;
		public string sku;
		public string posDesc;
		public string posDescAlt;
		public decimal priceEach;
		public DateTime orderTime;

		public string groupId
		{
			get
			{
				return sifNo + "/" + sector;
			}
		}

		static public string phrase(string phrase)
		{
			return a.phrase(phrase);
		}

		static public string phrase(int phraseId, string phrase)
		{
			return a.phrase(phraseId, phrase);
		}

		public string groupName
		{
			get
			{
				return "Sif " + sifNo + " " + phrase(68, "flight recorded")+" #" + sector + " " + flightPrefix + flightNumberInt.ToString("0000") + " " + from + "-" + to + " "+phrase(15, "Date")+":" + departureDate.ToString("ddd d-MMM-yyyy") + " "+phrase(69, "Cart")+":" + cart;
			}
		}

		public string[] fields
		{

			get
			{

				string kit = "" + kitCode;
				//if (kitPOSDesc != null) kit += "-" + kitPOSDesc;

				bool possitive = !credit;

				if (olType != "P") possitive = !possitive;

				string symbol = possitive ? "" : "-";

				return new string[] { kit, seat, a.ohTypeText(ohType, credit), orderNo.ToString(), orderTime.ToString(Form1.timeFormat), quantity.ToString() + "x", sku, posDesc, 
					symbol+ ((float)priceEach).ToString(), 
					symbol+ ((float)(quantity * priceEach)).ToString() 
				};

			}

		}

	}

	struct Seller
	{
		public int sector;//todo3 change to sector not sector orig for AT
		public string crewId;
		public string firstname,surname;

		public override string ToString()
		{
			return (crewId + " " + (firstname ?? "") + " " + surname).Trim();
		}

	}

	class CurrencyTotal
	{
		public string currency;
		public decimal value;
		public CurrencyTotal(string currency, decimal value)
		{
			this.currency = currency;
			this.value = value;
		}
	}

	class Flight
	{

		public string flightPrefix, from, to;
		public int flightNumberInt, sector;//todo3 change to sector/sectorText not sector orig for AT
		public DateTime departureDate;

		public decimal creditCardsBase;
		public List<CurrencyTotal> currencyTotalRows;

		public Flight(string flightPrefix, int flightNumberInt, string from, string to, DateTime departureDate, int sector)
		{
			this.flightPrefix = flightPrefix;
			this.flightNumberInt = flightNumberInt;
			this.from = from;
			this.to = to;
			this.departureDate = departureDate;
			this.sector = sector;
			currencyTotalRows = new List<CurrencyTotal>();
			creditCardsBase = 0;
		}

		public string[] fieldsWithTender
		{

			get
			{

				string[] sa = new string[] { sector.ToString(), flightPrefix + flightNumberInt.ToString("0000"), from + " - " + to, departureDate.ToString("d-MMM-yyyy"), ((float)creditCardsBase).ToString() };

				string[] sa2 = new string[sa.Length + a.sifCurrencyTotalRows.Count];

				for (int i = 0; i < sa.Length; i++)
				{
					sa2[i] = sa[i];
				}

				for (int i = 0; i < a.sifCurrencyTotalRows.Count; i++)
				{
					sa2[i + sa.Length] = ((float)getValue(this.currencyTotalRows, a.sifCurrencyTotalRows[i].currency)).ToString();
				}

				return sa2;

			}

		}

		decimal getValue(List<CurrencyTotal> rows, string currency)
		{
			foreach (CurrencyTotal row in rows)
			{
				if (row.currency == currency)
				{
					return row.value;
				}
			}
			return 0;
		}

		public string[] fieldsSimple
		{
			get
			{
				
				string sellersText = null;

				int count = 0;

				foreach(Seller seller in a.sellers)
				{
					if (seller.sector == sector)
					{
						count++;
						if (sellersText == null)
						{
							sellersText = seller.ToString();
						}
					}

				}

				if(count>1)
				{
					sellersText += " and " + (a.sellers.Count - 1) + " more";
				}

				return new string[] { sector.ToString(), flightPrefix + flightNumberInt.ToString("0000"), from + " - " + to, departureDate.ToString("ddd d-MMM-yyyy"), sellersText };

			}
		}

	}

	public class Tender
	{

		public string sifNo;
		public string cart;
		public bool voided;
		public bool credit;
		public int sector;//todo3 change to sector not sector orig for AT

		public string flightPrefix, from, to;
		public int flightNumberInt;
		public DateTime departureDate;

		public string seat;
		public string ohType;
		public int orderNo;
		public string tenderType;
		public string currency;
		public decimal amount,amountBase;
		public DateTime orderTime;

		public string groupId
		{
			get
			{
				return sifNo + "/" + sector;
			}
		}

		static public string phrase(int phraseId, string phrase)
		{
			return a.phrase(phraseId, phrase);
		}

		static public string phrase(string phrase)
		{
			return a.phrase(phrase);
		}

		public string groupName
		{
			get
			{
				return "Sif " + sifNo + " " + phrase(68, "flight recorded") + " #" + sector + " " + flightPrefix + flightNumberInt.ToString("0000") + " " + from + "-" + to + " " + phrase(15, "Date") + ":" + departureDate.ToString("ddd d-MMM-yyyy") + " " + phrase(69, "Cart") + ":" + cart;
			}
		}

		public decimal creditCardBase
		{
			get
			{
				if (tenderType != "CC") return 0;
				return amountBase;
			}
		}

		public string[] fields
		{

			get
			{

				string tenderTypeDesc = tenderType;
				switch (tenderType)
				{
					case "VO": tenderTypeDesc = phrase(70, "Coupon"); break;
					case "CC": tenderTypeDesc = phrase(71, "Credit card"); break;
					case "CA": tenderTypeDesc = phrase(72, "Cash"); break;
				}

				return new string[] { seat, a.ohTypeText(ohType, credit), orderNo.ToString(), orderTime.ToString(Form1.timeFormat), tenderTypeDesc, currency + " " + ((float)amount).ToString(), 
					 ((float)(amountBase)).ToString() 
				};

			}

		}

	}

}
