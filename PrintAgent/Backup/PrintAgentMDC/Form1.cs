﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using System.Threading;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.IO.Compression;
using System.Net;
using System.Drawing.Printing;
using Microsoft.Win32;

namespace PrintAgentMDC
{

	public enum DocumentType { Sif, PI }

	public enum Airline
	{
		ts_4 = 4,
		aa_7 = 7,
		usairways = 8,
		aircanada_14 = 14,
		avianca_15 = 15,
		aerosur_24 = 24,
		pluna = 31,
		gulfair_32 = 32,
		tiger_dutyFree = 35,
		jetstar_dutyfree = 36,
		tiger_bob = 38,
		jetstar_bob = 39,
		canjet_dutyfree_40 = 40,
		airAsiaX_48 = 48,

		peach_BOB_902 = 902,
		peach_DP_903 = 903,
		peach_DF_904 = 904,

		scootDF_1000 = 1000,
		guamFreedom_dutyfree_1001 = 1001,
		airAsiaX_merch_1002 = 1002,
		airAsiaKL_1004 = 1004,

		hawaiian_BOB_1902 = 1902,
		hawaiian_DP_1903 = 1903,
		hawaiian_DF_1904 = 1904,

		scootBOB = 1006,
		tonlesap_1008 = 1008,
		palau_1009 = 1009,
		hainan_1010 = 1010,
		caribbeanJamaica_1011 = 1011,//Air Jamaica / Caribbean Airlines
		fat_1012 = 1012,
		garuda_idr_DP_1014 = 1014,
		garuda_idr_DF_1015 = 1015,
		mru_1016 = 1016,
        mrd_1017 = 1017,

        FlyJamaica_1020 = 1020,
        SanJose_1021 = 1021,
        Lima_l022 = 1022,
        Tame_1023 = 1023,
        Aerogal_1024=1024,
        Hawaiian_1025 = 1025,
        Virgin_1030 = 1030,
        Tianjin_1033=1033,
        Eastern_1034 = 1034,
        Eastern_1035 = 1035,
        VivaColombia1037 = 1037,
        VivaColombia1038 = 1038,
        ThaiSmile_1040 = 1040,
        RoyalJordanian1041 = 1041,
        Caribbean_1042 = 1042,
        AirAsiaPhilippines1043 =1043,
        WestJet1044 =1044,
        RwandAir1045=1045,
        TransAsia1046=1046,
        RoyalWings1047 = 1047,
        Malaysia1049 = 1049,
        AirMacau1053 = 1053,
        ELAL_1050 =1050,
        ELAL_UP_1051 =1051,
        
        airAsiaJapan_BOB_2902 = 2902,
		airAsiaJapan_DP_2903 = 2903,
		airAsiaJapan_DF_multiService_2904_notUsed = 2904,

		airAsiaJapan_DF_2908 = 2908,
        FijiAirways_3001 = 3001,
        AirNiugini_3002 = 3002
	}

	public partial class Form1 : Form
	{
        //AirCanada AX
        public const int AX = 98998;

		//these are effectively consts

        //Note: always point to America Server
        public static string serverUrl_america = @"http://192.168.227.81/Device_WebService/";
        public static string serverUrl_singapore = @"http://192.168.227.81/Device_WebService/";

        ////Local Server
        //public static string serverUrl_america = @"http://localhost:50198/";
        //public static string serverUrl_singapore = @"http://localhost:50198/";

		public static string serverUrl_active = serverUrl_singapore;

		public const string dateFormatA = "d-MMM-yyyy";
		public const string timeFormat = "HH:mm";
		public const string dateTimeFormat = dateFormatA + " " + timeFormat;

		public delegate void status_delegate(string text);

		public status_delegate serverStatus_handler;
		public status_delegate serverMessage_handler;

		int _seconds;

		BinaryWriter bwRequest;
		BinaryReader brResponse;

		DateTime wakeUpTime = DateTime.Now;

		float currentTop;
		int currentLeft;
		Graphics graphics;

		string cartNo, sifNo, flightPrefix, from, to, packedOrCompletedBy, hhcVersion, kitDesc, serialNo;
		int flightNumberInt;
		DateTime departureDate;
		string airlineShortDesc_forAgentPrintout;
		int copies = 1;

		List<string> seals = new List<string>(5);
		List<Item> items = new List<Item>(100);
		List<Flight> flights = new List<Flight>(5);
		List<Sale> sales = new List<Sale>(64);
		List<Tender> tenders = new List<Tender>(64);
		decimal sifCreditCardsBase;
		int sifCreditCardTransactions, sifCashTransactions, sifTransactions;

		int groupCountSales, groupCountTenders;

		int module_startAtRow, module_groupsRemaining;
		string module_currentGroupId;
		int pageNumber, outputPageCheck;
		int copyThatIsPrinting;

		enum Section { items, signature, flightsTender, sifTender, sales, tenders, end }
		Section section;

		enum TableMode { flightsSimple, items, flightsTender, sifTender, sales, tenders }

		public const string handshakeString = "handheld.io.irs.dfass.com";

		private bool ignoreDropdownEvent;

		Font font10 = new Font("Calibri", 10);
		Font fontLarge = new Font("Calibri", 16);
		Brush brushBlack = new SolidBrush(Color.Black);
		Brush brushWatermark = new SolidBrush(Color.FromArgb(230, 230, 230));

		Pen pen = new Pen(Color.Gray);
		Font fontWatermark1 = new Font("Calibri", 300);
		Font fontWatermark2 = new Font("Calibri", 160);
		PrintDocument pd;

		Font fontSub = new Font("Calibri", 14);
		Font font10strikeout = new Font("Calibri", 10, FontStyle.Strikeout);
		Font font10bold = new Font("Calibri", 10, FontStyle.Bold);
		Font font7 = new Font("Calibri", 7);

		Brush brushSilver = new SolidBrush(Color.Silver);
		Brush brushRed = new SolidBrush(Color.Red);

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{

			font10.Dispose();
			fontLarge.Dispose();
			brushBlack.Dispose();
			brushWatermark.Dispose();

			pen.Dispose();
			fontWatermark1.Dispose();
			fontWatermark2.Dispose();
			pd.Dispose();

			fontSub.Dispose();
			font10strikeout.Dispose();
			font10bold.Dispose();
			font7.Dispose();

			brushSilver.Dispose();
			brushRed.Dispose();

		}

		Brush brush
		{
			get
			{
				return brushBlack;
			}
		}

		bool ide
		{
			get
			{
				return a.ide;
			}
		}

		public Form1()
		{
			InitializeComponent();
		}

		void serverStatus_method(string text) { labelStatusServer.Text = text; labelStatusServer.Refresh(); }

		void serverMessage_method(string text)
		{
			labelBig.Text = DateTime.Now.ToString(dateTimeFormat) + "\n" + text;
		}

		List<Bondroom> bondrooms
		{
			get
			{

				List<Bondroom> list = new List<Bondroom>();

				list.Add(new Bondroom(4, "YHZ"));
				list.Add(new Bondroom(4, "YEG"));
				list.Add(new Bondroom(4, "YOW"));
				list.Add(new Bondroom(4, "YQB"));
				list.Add(new Bondroom(4, "YUL"));
				list.Add(new Bondroom(4, "YVR"));
				list.Add(new Bondroom(4, "YWG"));
				list.Add(new Bondroom(4, "YYZ"));
				list.Add(new Bondroom(4, "YXE"));
				list.Add(new Bondroom(4, "YYC"));
				list.Add(new Bondroom(4, "YQR"));
				list.Add(new Bondroom(4, "ccc"));

				list.Add(new Bondroom(7, "JFK"));
				list.Add(new Bondroom(7, "DFW"));
				list.Add(new Bondroom(7, "RDU")); 
				list.Add(new Bondroom(7, "MIA"));
				list.Add(new Bondroom(7, "ORD"));
				list.Add(new Bondroom(7, "LAX"));

				list.Add(new Bondroom(8, "PHL"));
				list.Add(new Bondroom(8, "CLT"));
				list.Add(new Bondroom(8, "PHX"));

                list.Add(new Bondroom(14, "YYZ"));
                list.Add(new Bondroom(14, "YUL"));
                list.Add(new Bondroom(14, "YVR"));
                list.Add(new Bondroom(14, "YYC"));
                list.Add(new Bondroom(14, "YEG"));
                list.Add(new Bondroom(14, "YHZ"));
                list.Add(new Bondroom(14, "YOW"));

				list.Add(new Bondroom(15, "BOG"));

				//list.Add(new Bondroom(24, null));
				//list.Add(new Bondroom(31, null));

				list.Add(new Bondroom(32, "BAH"));
				list.Add(new Bondroom(36, null));

				list.Add(new Bondroom(40, "YHZ"));
				list.Add(new Bondroom(40, "YEG"));
				list.Add(new Bondroom(40, "YOW"));
				list.Add(new Bondroom(40, "YQB"));
				list.Add(new Bondroom(40, "YUL"));
				list.Add(new Bondroom(40, "YVR"));
				list.Add(new Bondroom(40, "YWG"));
				list.Add(new Bondroom(40, "YYZ"));
				list.Add(new Bondroom(40, "YQR"));
				list.Add(new Bondroom(40, "ccc"));
                //list.Add(new Bondroom(41, "YYZ"));

				list.Add(new Bondroom(48, null));

				list.Add(new Bondroom(903, null));

				list.Add(new Bondroom(1001, null));
				list.Add(new Bondroom(1002, null));

				//list.Add(new Bondroom(1003, "DPS"));
				//list.Add(new Bondroom(1003, "CGK"));

				//list.Add(new Bondroom(1004, null));
				//list.Add(new Bondroom(1005, null));
				//list.Add(new Bondroom(1007, null));

				//list.Add(new Bondroom(1008, null));

				//list.Add(new Bondroom(1010, null));

				list.Add(new Bondroom(1011, "MBJ"));
				list.Add(new Bondroom(1011, "KIN"));

				list.Add(new Bondroom(1012, "KHH"));
				list.Add(new Bondroom(1012, "MZG"));
				list.Add(new Bondroom(1012, "TPE"));

				list.Add(new Bondroom(1014, "DPS"));//see "1014 & 1015 DPS share the same printer"

				list.Add(new Bondroom(1014, "CGK"));
				list.Add(new Bondroom(1015, "CGK"));

				list.Add(new Bondroom(1016, "MRU"));
				list.Add(new Bondroom(1017, "MRD"));

				list.Add(new Bondroom(1018, null));
				list.Add(new Bondroom(1019, null));

                list.Add(new Bondroom(1020, "MBJ"));
                list.Add(new Bondroom(1020, "KIN"));

                list.Add(new Bondroom(1021, "SJC"));
                list.Add(new Bondroom(1022, "LIM"));
                list.Add(new Bondroom(1023, "UIO"));
                list.Add(new Bondroom(1024,"GLG"));
				list.Add(new Bondroom(1025, "HNL"));

				//list.Add(new Bondroom(2903, "NRT"));
				//list.Add(new Bondroom(2903, "NGO"));

                list.Add(new Bondroom(1030, "LGW")); //Virgin Airline
                list.Add(new Bondroom(1033, "TSN"));
                list.Add(new Bondroom(1034, "EAL"));
                list.Add(new Bondroom(1034, "MIA"));
                list.Add(new Bondroom(1037, "BVC"));
                list.Add(new Bondroom(1040, "BKK"));

                list.Add(new Bondroom(1041, "AMM"));

                list.Add(new Bondroom(1042, "MBJ"));
                list.Add(new Bondroom(1042, "KIN"));

                list.Add(new Bondroom(1043, "MNL"));
                list.Add(new Bondroom(1043, "CEB"));
                list.Add(new Bondroom(1043, "KLO"));

                list.Add(new Bondroom(1044, "YYZ"));
                list.Add(new Bondroom(1044, "YYC"));
                list.Add(new Bondroom(1044, "YEG"));
                list.Add(new Bondroom(1044, "YVR"));
                list.Add(new Bondroom(1044, "YWG"));

                list.Add(new Bondroom(1045, "KGL"));

                list.Add(new Bondroom(1046, "KHH"));
                list.Add(new Bondroom(1046, "TPE"));
                list.Add(new Bondroom(1046, "TSA"));

                list.Add(new Bondroom(1047, "AMM"));
                list.Add(new Bondroom(1049, "MFC"));
                list.Add(new Bondroom(1053, "MFM"));
                list.Add(new Bondroom(1053, "MFC"));

                list.Add(new Bondroom(3001, "NAN"));
                list.Add(new Bondroom(3002, "POM"));
                list.Add(new Bondroom(3903, "MNL"));
                list.Add(new Bondroom(3904, "MNL"));

				return list;

			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{

			///setting the value of the combobox with the value read
			///from the registry.
			this.bondroom = ReadRegistry();

			int selectIndex = -1;
			foreach (Bondroom bondroom in bondrooms)
			{

				dropdownBondroom.Items.Add(bondroom);//ok

				if (this.bondroom != null && bondroom.airlineCode == this.bondroom.airlineCode && bondroom.sifPrefix == this.bondroom.sifPrefix)
				{
					selectIndex = dropdownBondroom.Items.Count - 1;//set to the most recently added item //ok
				}

			}

			try
			{
				ignoreDropdownEvent = true;
				dropdownBondroom.SelectedIndex = selectIndex;//ok
				a.spanish = this.bondroom != null && (bondroom.airlineCode == 24 || bondroom.airlineCode == 31);
				dropdownLang.SelectedIndex = a.spanish ? 1 : 0;
			}
			finally
			{
				ignoreDropdownEvent = false;
			}

			dropdownBondroom.Enabled = selectIndex == -1;//ok
			dropdownLang.Enabled = selectIndex == -1;

			labelBig.Text = "";

			//printing

			pd = new PrintDocument();
			pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);

			//debug

			bool debugPrint = false, localServer = false;

			//todo2
			//debugPrint = ide;
			//localServer = ide;

			if (localServer)
			{
				serverUrl_active = @"http://127.0.0.1:50198/";
			}

			if (debugPrint)
			{

				bondroom = new Bondroom(24, "3KD");

				cartNo = "CART123";
				sifNo = "ZZZ 123456";
				flightPrefix = "TR";
				flightNumberInt = 0;
				from = "BHX";
				to = "LHR";
				departureDate = DateTime.Now.Date;
				packedOrCompletedBy = "dbl";
				hhcVersion = "0.0.200";
				a.piCartInboundFigures = true;

				for (int i = 0; i < 55; i++)
				{
					Item item = new Item();
					item.sku = "Sku" + i;
					item.buttonText = "Description" + i;
					items.Add(item);
				}

				for (int i = 0; i < 5; i++)
				{
					flights.Add(new Flight("DL", 123, "GTW", "SIN", DateTime.Now, i));
				}

				groupCountSales = 2;
				for (int i = 0; i < 2; i++)
				{
					Sale sale = new Sale();
					sale.sku = "sku" + i;
					sale.sifNo = "sif" + i;
					sales.Add(sale);
				}

				groupCountTenders = 2;
				for (int i = 0; i < 2; i++)
				{
					Tender tender = new Tender();
					tender.sifNo = "sif" + i;
					tenders.Add(tender);
				}

				startPrint(DocumentType.PI);

				Close();

			}
			else
			{

				labelSoftwareVersion.Text = "Version " + GetVersion();

				serverStatus_handler = serverStatus_method;
				serverMessage_handler = serverMessage_method;

				ThreadPool.QueueUserWorkItem(new WaitCallback(main_thread));

			}

		}

		static public string phrase(int phraseId, string phrase)
		{
			return a.phrase(phraseId, phrase);
		}

		static public string phrase(string phrase)
		{
			return a.phrase(phrase);
		}

		Bondroom bondroom
		{
			get
			{
				return a.bondroom;
			}
			set
			{
				a.bondroom = value;
			}
		}

		public void main_thread(object stateInfo)
		{

			for (; ; )
			{

				if (bondroom == null)
				{
					serverStatus("Idle");
					seconds = 1;
				}

				if (bondroom != null)
				{
					if (seconds < 10) seconds = 10;
					handleTick();
				}

				//sleep

				for (int i = 0; i < seconds; i++)
				{
					Thread.Sleep(1000);
				}

				//seconds validate

				if (wakeUpTime > DateTime.Now)
				{
					wakeUpTime = DateTime.Now;
				}

				//seconds adjust

				if (wakeUpTime.AddHours(1) < DateTime.Now)
				{
					seconds = 60 * 5;
				}
				else if (wakeUpTime.AddMinutes(45) < DateTime.Now)
				{
					seconds = 60 * 1;
				}
				else if (wakeUpTime.AddMinutes(30) < DateTime.Now)
				{
					seconds = 30;
				}
				else if (wakeUpTime.AddMinutes(10) < DateTime.Now)
				{
					seconds = 10;
				}

			}

		}

		void sendGetPrintRequest(int airlineCode)
		{

			int minutes = (int)seconds / 60;
			if (seconds < 60) minutes = 1;

			bwRequest.Write((byte)DeviceRequest.setPrinterStatus);
			bwRequest.Write(bondroom.sifPrefix ?? "");
            bwRequest.Write((Int16)airlineCode);
			bwRequest.Write(true);
			bwRequest.Write(printerName ?? "");
			bwRequest.Write((byte)minutes);

			bwRequest.Write((byte)DeviceRequest.getPrintRequest);
			bwRequest.Write(bondroom.sifPrefix ?? "");
            bwRequest.Write((Int16)airlineCode);

		}

		string printerName
		{
			get
			{
				try
				{
					return "Printer name: " + pd.PrinterSettings.PrinterName + "\n\nMachine name: " + Environment.MachineName + "\n\nStatus: " + (pd.PrinterSettings.IsValid ? "Online" : "Offline");
				}
				catch
				{
					return "Unknown printer";
				}
			}
		}

		void handleTick()
		{
            serverStatus("Checking" + secondsSuffix);

            PrintForAirline(bondroom.airlineCode);

            if (bondroom.airlineCode == 1034)
                PrintForAirline(1035);

		}

		void PrintForAirline(int airlineCode)
		{
			const int protocolVersion = 14;
            /*
             * 14=agent ver 1.1.24 Added IC_CrewBeginRouteAdjust to be transfered.
             * 13=agent ver 1.0.53 serial no.
             * 12=agent ver 1.0.38 sellers
             * 11=agent ver 1.0.37 sales
             * 10=agent ver 1.0.29 kit desc
             */            
            
            //send request
            #region Request
            try
			{

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serverUrl_active + "AgentService.aspx");
				request.Method = "POST";

				MemoryStream ms = new MemoryStream();

				bwRequest = new BinaryWriter(ms);

				bwRequest.Write((Int16)protocolVersion);
				bwRequest.Write(handshakeString);

				bwRequest.Flush();

				using (GZipStream output = new GZipStream(ms, CompressionMode.Compress, true))
				{

					bwRequest = new BinaryWriter(output);

					sendGetPrintRequest(airlineCode);

					bwRequest.Write((byte)DeviceRequest.end);

					bwRequest.Flush();

				}

				request.ContentLength = ms.Length;

				Stream requestStream = request.GetRequestStream();

				ms.Position = 0;

				byte[] ba = new byte[8192];

				for (; ; )
				{
					int bytesRead = ms.Read(ba, 0, ba.Length);
					if (bytesRead == 0) break;
					requestStream.Write(ba, 0, bytesRead);
				}

				requestStream.Flush();
				requestStream.Close();

				WebResponse response = request.GetResponse();

				Stream responseStream = response.GetResponseStream();

				string serverMessage = null;

				using (GZipStream input = new GZipStream(responseStream, CompressionMode.Decompress))
				{

					brResponse = new BinaryReader(input);

					for (; ; )
					{

						ServerResponse command = (ServerResponse)brResponse.ReadByte();

						switch (command)
						{

							case ServerResponse.serverRedirect:
								if (serverUrl_active == serverUrl_america)
								{
									serverUrl_active = serverUrl_singapore;
								}
								else
								{
									serverUrl_active = serverUrl_america;
								}
								break;

							case ServerResponse.serverMessage:
								serverMessage = readProcessServerMessage();
								break;

							case ServerResponse.wakeUp:

								wakeUpTime = DateTime.Now;
								seconds = 10;

								break;

							case ServerResponse.printPI_simple:

								wakeUpTime = DateTime.Now;
								seconds = 10;

								readProcess_print(DocumentType.PI);

								break;

							case ServerResponse.printSif:

								wakeUpTime = DateTime.Now;
								seconds = 10;

								readProcess_print(DocumentType.Sif);

								break;

							case ServerResponse.end:
								break;

							default: throw new ApplicationException();

						}

						if (command == ServerResponse.end || command == ServerResponse.serverMessage || command == ServerResponse.serverRedirect)
						{
							break;
						}

					}

					brResponse.Close();

				}

				if (serverMessage != null)
				{
					seconds = 60 * 5;
					this.Invoke(serverMessage_handler, "Message from server:\n" + serverMessage);
					serverStatus("Error" + secondsSuffix);
					return;
				}

				serverStatus("Connected" + secondsSuffix);

			}
			catch (Exception ex)
			{
				seconds = 10;
				serverStatus("Attempting to connect (every " + seconds + " seconds)");
            }
            #endregion
        }

		void readProcess_print(DocumentType documentType)
		{

			a.documentType = documentType;

			//reset variables

			cartNo = null;
			sifNo = null;
			packedOrCompletedBy = null;

			seals.Clear();
			flights.Clear();
			a.sellers.Clear();
			items.Clear();
			sales.Clear();
			tenders.Clear();
			sifCreditCardsBase = 0;
			sifCreditCardTransactions = 0;
			sifCashTransactions = 0;
			sifTransactions = 0;
			a.sifCurrencyTotalRows.Clear();
			copies = 1;

			//processing

			if (airline == 24 || ide)
			{
				string[] currencies = new string[] { "USD", "BOB", "EUR" };
				foreach (string currency in currencies)
				{
					a.sifCurrencyTotalRows.Add(new CurrencyTotal(currency, 0));
				}
			}

			cartNo = brResponse.ReadString();
			sifNo = brResponse.ReadString();

			airlineShortDesc_forAgentPrintout = brResponse.ReadString(); if (airlineShortDesc_forAgentPrintout.Length == 0) airlineShortDesc_forAgentPrintout = null;

			if (documentType == DocumentType.Sif)
			{
				flightPrefix = brResponse.ReadString(); if (flightPrefix.Length == 0) flightPrefix = null;
				flightNumberInt = brResponse.ReadInt16();
				from = brResponse.ReadString(); if (from.Length == 0) from = null;
				to = brResponse.ReadString(); if (to.Length == 0) to = null;
				departureDate = readDate();
			}

			packedOrCompletedBy = brResponse.ReadString(); if (packedOrCompletedBy.Length == 0) packedOrCompletedBy = null;

			hhcVersion = brResponse.ReadString(); if (hhcVersion.Length == 0) hhcVersion = null;

			copies = brResponse.ReadByte();
			kitDesc = brResponse.ReadString();
			serialNo = brResponse.ReadString(); if (serialNo.Length == 0) serialNo = null;

			//seals

			while (brResponse.ReadBoolean())
			{
				seals.Add(brResponse.ReadString());
			}

			//PI

			if (documentType == DocumentType.PI)
			{

				//flights

				while (brResponse.ReadBoolean())
				{
					string fr_flightPrefix = brResponse.ReadString();
					int fr_flightNumberInt = brResponse.ReadInt16();
					string fr_from = brResponse.ReadString();
					string fr_to = brResponse.ReadString();
					DateTime fr_departureDate = readDate();
					int sector = brResponse.ReadInt16();
					Flight flight = new Flight(fr_flightPrefix, fr_flightNumberInt, fr_from, fr_to, fr_departureDate, sector);
					flights.Add(flight);
				}

				while (brResponse.ReadBoolean())
				{

					Seller seller = new Seller();

					seller.sector = brResponse.ReadByte();//todo3 change to sector not sector orig for AT
					seller.crewId = brResponse.ReadString();
					seller.firstname = brResponse.ReadString(); if (seller.firstname.Length == 0) seller.firstname = null;
					seller.surname = brResponse.ReadString(); if (seller.surname.Length == 0) seller.surname = null;

                    if (a.sellers.Where(b=> b.crewId == seller.crewId).Count() == 0)
					    a.sellers.Add(seller);

				}

			}

			//categories

			SortedDictionary<int, string> categoryById = new SortedDictionary<int, string>();

			while (brResponse.ReadBoolean())
			{
				int categoryId = brResponse.ReadByte();
                try
                {
                    string desc = brResponse.ReadString();
                    if (!categoryById.ContainsKey(categoryId))
                    {
                        categoryById.Add(categoryId, desc);
                    }
                }
                catch { }
			}

			//items

			a.piSoldFigures = false;
			a.piCartInboundFigures = false;

			while (brResponse.ReadBoolean())
			{

				Item item = new Item();

				item.sku = brResponse.ReadString();
                //AirCanada AX
                if (airline == AX)
                {
                    item.airline = 14;
                    item.skuAX = brResponse.ReadString();
                }
				item.buttonText = brResponse.ReadString();
				item.retailPrice = (float)brResponse.ReadDecimal();
				item.location = brResponse.ReadString(); if (item.location.Length == 0) item.location = null; if (item.location == null) item.location = "(blank)";
				item.inMoreDrawers = brResponse.ReadString().Length != 0;
				item.categoryId = brResponse.ReadByte();

				if (categoryById.ContainsKey(item.categoryId))
				{
					item.category = categoryById[item.categoryId];
				}

				if (brResponse.ReadBoolean())
				{
					switch (documentType)
					{
						case DocumentType.Sif:
							item.packerOut = brResponse.ReadInt16();
							break;
						case DocumentType.PI:
                            if (bondroom.airline == Airline.Virgin_1030)
                                item.displayTransferCount = true;
							item.packerOut = brResponse.ReadInt16();
							item.crewSold = brResponse.ReadInt16();
							item.crewReturn = brResponse.ReadInt16();
							item.returnTotal = brResponse.ReadInt16();
							item.returnDamaged = brResponse.ReadInt16();
							item.replenish = brResponse.ReadInt16();
                            item.crewBeginRouteAdjust = brResponse.ReadInt16();
							if (item.crewSold != 0 || item.crewReturn != 0)
							{
								a.piSoldFigures = true;
							}

							if (item.returnTotal != 0 || item.returnDamaged != 0 || item.replenish != 0)//todo4 issue where backoffice? had filled in replenish but nothing else, may need to just check returnTotal and filter the zeros in returnDamaged and Replenish if returnTotal is all zeros
							{
								a.piCartInboundFigures = true;
							}

							break;
						default: throw new ApplicationException();
					}

				}

				items.Add(item);

			}

			if (items.Count > 0)
			{

				List<string> groups = new List<string>(10);

				foreach (Item item in items)
				{
					if (!groups.Contains(item.groupId))
					{
						groups.Add(item.groupId);
					}
				}

				a.groupCountItems = groups.Count;

			}

			items.Sort();

			if (documentType == DocumentType.PI)
			{

				//sales

				Sale previousSale = null;

				groupCountSales = 1;

				while (brResponse.ReadBoolean())
				{

					Sale sale = new Sale();

					sale.sifNo = brResponse.ReadString();
					sale.voided = brResponse.ReadBoolean();
					sale.credit = brResponse.ReadBoolean();
					sale.sector = brResponse.ReadByte();
					sale.flightPrefix = brResponse.ReadString();
					sale.flightNumberInt = brResponse.ReadInt16();
					sale.from = brResponse.ReadString();
					sale.to = brResponse.ReadString();
					sale.departureDate = readDate();
					sale.kitCode = brResponse.ReadString(); if (sale.kitCode.Length == 0) sale.kitCode = null;
					sale.kitPOSDesc = brResponse.ReadString(); if (sale.kitPOSDesc.Length == 0) sale.posDesc = null;
					sale.seat = brResponse.ReadString(); if (sale.seat.Length == 0) sale.seat = null;
					if (noSeat) sale.seat = null;
					sale.ohType = brResponse.ReadString();
					sale.orderNo = brResponse.ReadInt16();
					sale.quantity = brResponse.ReadInt16();
					sale.olType = brResponse.ReadString();
					sale.sku = brResponse.ReadString();
					sale.posDesc = brResponse.ReadString(); if (sale.posDesc.Length == 0) sale.posDesc = null;
					sale.posDescAlt = brResponse.ReadString(); if (sale.posDescAlt.Length == 0) sale.posDescAlt = null;
					sale.priceEach = brResponse.ReadDecimal();
					sale.orderTime = readDate();
					sale.cart = brResponse.ReadString(); if (sale.cart.Length == 0) sale.cart = null;

					sales.Add(sale);
					if (ide && false)
					{
						sales.Add(sale);
						sales.Add(sale);
						sales.Add(sale);
						sales.Add(sale);
					}

					if (previousSale != null)
					{
						if (previousSale.groupId != sale.groupId)
						{
							groupCountSales++;
						}
					}

					previousSale = sale;

				}

				//sales end

				//tender

				Tender previousTender = null;

				groupCountTenders = 1;

				while (brResponse.ReadBoolean())
				{

					Tender tender = new Tender();

					tender.sifNo = brResponse.ReadString();
					tender.voided = brResponse.ReadBoolean();
					tender.credit = brResponse.ReadBoolean();
					tender.sector = brResponse.ReadByte();//todo3 change to sector not sector orig for AT
					tender.flightPrefix = brResponse.ReadString();
					tender.flightNumberInt = brResponse.ReadInt16();
					tender.from = brResponse.ReadString();
					tender.to = brResponse.ReadString();
					tender.departureDate = readDate();
					tender.seat = brResponse.ReadString(); if (tender.seat.Length == 0) tender.seat = null;
					if (noSeat) tender.seat = null;
					tender.ohType = brResponse.ReadString();
					tender.orderNo = brResponse.ReadInt16();
					tender.tenderType = brResponse.ReadString();
					tender.currency = brResponse.ReadString();
					tender.amount = brResponse.ReadDecimal();
					tender.amountBase = brResponse.ReadDecimal();
					tender.orderTime = readDate();
					tender.cart = brResponse.ReadString(); if (tender.cart.Length == 0) tender.cart = null;

					tenders.Add(tender);
					if (ide && false)
					{
						tenders.Add(tender);
						tenders.Add(tender);
						tenders.Add(tender);
						tenders.Add(tender);
					}

					if (previousTender != null)
					{
						if (previousTender.groupId != tender.groupId)
						{
							groupCountTenders++;
						}
					}

					previousTender = tender;

				}

				//tender end

				//tender summary

				generateTenderSummary();

			}

			startPrint(documentType);

		}

		void generateTenderSummary()
		{

			if (documentType != DocumentType.PI) return;

			foreach (Tender tender in tenders)
			{

				sifTransactions++;

				switch (tender.tenderType)
				{
					case "CC": sifCreditCardTransactions++; break;
					case "CA": sifCashTransactions++; break;
				}

				if (!tender.voided)
				{

					for (int i2 = 0; i2 < flights.Count; i2++)
					{
						Flight flight = flights[i2];
						if (flight.sector == tender.sector)
						{
							flight.creditCardsBase += tender.creditCardBase;
							bool currencyFound = false;
							for (int i = 0; i < flight.currencyTotalRows.Count; i++)
							{
								CurrencyTotal currencyTotal = flight.currencyTotalRows[i];
								if (currencyTotal.currency == tender.currency)
								{
									currencyFound = true;
									currencyTotal.value += tender.amount;
									break;
								}
							}
							if (!currencyFound)
							{
								flight.currencyTotalRows.Add(new CurrencyTotal(tender.currency, tender.amount));
							}
							break;
						}
					}

					sifCreditCardsBase += tender.creditCardBase;

					bool sifCurrencyFound = false;
					for (int i = 0; i < a.sifCurrencyTotalRows.Count; i++)
					{
						CurrencyTotal currencyTotal = a.sifCurrencyTotalRows[i];
						if (currencyTotal.currency == tender.currency)
						{
							sifCurrencyFound = true;
							currencyTotal.value += tender.amount;
							break;
						}
					}
					if (!sifCurrencyFound)
					{
						a.sifCurrencyTotalRows.Add(new CurrencyTotal(tender.currency, tender.amount));
					}

				}

			}

		}

		void startPrint(DocumentType documentType)
		{
			a.documentType = documentType;
			pd.DocumentName = sifNo;
			outputPageCheck = 0;
			module_startAtRow = 0;
			pageNumber = 1;
			module_currentGroupId = null;
			copyThatIsPrinting = 1;
			setFirstSection();
			pd.Print();
		}

		DocumentType documentType
		{
			get
			{
				return a.documentType;
			}
		}

		void pd_PrintPage(object sender, PrintPageEventArgs e)
		{

			try
			{

				graphics = e.Graphics;

				currentTop = 40;
				currentLeft = 40;

				//watermark

				//headings

				if (module_startAtRow == 0 || true)
				{

					string heading;

					switch (documentType)
					{
						case DocumentType.Sif:
							heading = phrase(0, "Sales and Inventory form");
							break;
						case DocumentType.PI:
							heading = phrase(1, "PI sheet");
							break;
						default: throw new ApplicationException();
					}

					heading += " - " + sifNo;

					if (airlineShortDesc_forAgentPrintout != null)
					{
						heading += " - " + airlineShortDesc_forAgentPrintout;
					}

					graphics.DrawString(heading, fontLarge, brush, 40, 40);
					currentTop += 30;

					printField(phrase(4, "Cart #"), cartNo);
					printField(phrase(5, "Ref #"), sifNo);

					switch (documentType)
					{
						case DocumentType.PI:
							printField(phrase(6, "Agent"), "v" + GetVersion());
							if (hhcVersion != null)
							{
								printField(phrase(7, "HHC"), "v" + hhcVersion);
							}
							if (serialNo != null)
							{
								printField(phrase(74, "HHC serial#"), serialNo);
							}
							printField(phrase(8, "Printed"), DateTime.Now.ToString("ddd d-MMM-yyyy"));
							if (packedOrCompletedBy != null)
							{
								printField(phrase(9, "Completed by"), packedOrCompletedBy);
							}
							break;
						case DocumentType.Sif:

							string format = airline == 4 ? "d-MMM-yy" : "ddd d-MMM-yyyy";

							if (flightPrefix == null)
							{
								printField(phrase(73, "Departs"), departureDate.ToString(format));
							}
							else
							{
								if (airline == 8 || airline == 7)
								{
									printField(phrase(73, "Departs"), departureDate.ToString(format));
								}
								printField(phrase(10, "Flight"), flightPrefix + flightNumberInt.ToString("0000") + " / " + from + " - " + to);
							}

							printField(phrase(11, "Prepared by"), packedOrCompletedBy);
							printField(phrase(12, "on"), DateTime.Now.ToString(format));

							currentTop += font10.Height;
							currentLeft = 40;

							printField("Agent", "v" + GetVersion());
							if (hhcVersion != null)
							{
								printField("HHC", "v" + hhcVersion);
							}

							if (serialNo != null)
							{
								printField(phrase(74, "HHC serial#"), serialNo);
							}

							printField("Kit", kitDesc);

							break;
					}

					currentTop += font10.Height;

					printSeals();

					switch (documentType)
					{
						case DocumentType.PI:
							int startAtRow = 0;
							string currentGroupId = null;
							int groupsRemaining = 0;
							drawTable(e, TableMode.flightsSimple, ref startAtRow, ref currentGroupId, ref groupsRemaining);
							break;
						case DocumentType.Sif:
							//the Sif table is flush with seals
							break;
					}

				}

				//items

				if (section == Section.items)
				{
					printTable(e);
				}

				if (section == Section.signature)
				{

					printSignature(e);

				}
				else
				{
					if (section == Section.flightsTender)
					{
						printTable(e);
					}
					if (section == Section.sifTender)
					{
						printTable(e);
					}
					if (section == Section.sales)
					{
						printTable(e);
					}
					if (section == Section.tenders)
					{
						printTable(e);
					}
				}

				//next page

				if (section == Section.end)
				{
					pageNumber = 1;
					e.HasMorePages = copyThatIsPrinting < copies;
					copyThatIsPrinting++;
					setFirstSection();
				}
				else
				{
					e.HasMorePages = true;
					pageNumber++;
				}

				outputPageCheck++;
				if (outputPageCheck > 20)
				{
					e.HasMorePages = false;
				}

			}
			finally
			{
				graphics = null;
			}

		}

		void setFirstSection()
		{
			section = Section.items;
			module_groupsRemaining = getGroupCount(section);
		}

		void printSignature(PrintPageEventArgs e)
		{

			float outputTo = currentTop + font10.Height * 16;
			float limit = e.PageBounds.Height - 40;

			if (outputTo > limit)
			{
				return;
			}

			currentLeft = 40;
			printField(phrase(13, "Name"));
			printBox(0);

			currentLeft = 40 + 100 + 250 + 50;
			printField(phrase(14, "Next SIF no."));
			printBox(20);

			currentTop += font10.Height * 4;
			currentLeft = 40;
			printField(phrase(15, "Date"));
			printBox(0);

			currentLeft = 40 + 100 + 250 + 50;
			printField(phrase(16, "Uplift FH no."));
			printBox(20);

			currentTop += font10.Height * 4;
			currentLeft = 40;
			printField(phrase(17, "Signature"));
			printBox(0);

			currentLeft = 40 + 100 + 250 + 50;
			printField(phrase(18, "FH date"));
			printBox(20);

			currentTop += font10.Height * 4;
			currentLeft = 40;
			printField(phrase(19, "Notes"));
			printBox(0, (font10.Height * 7), 670);

			currentTop += font10.Height * 7;

			advanceSection(ref module_startAtRow, ref module_currentGroupId, ref module_groupsRemaining);

		}

		void printBox(int gap)
		{
			graphics.DrawRectangle(pen, currentLeft + 60 + gap, currentTop, 250, font10.Height * 3);
		}

		void printBox(int gap, int deep, int wide)
		{
			graphics.DrawRectangle(pen, currentLeft + 60 + gap, currentTop, wide, deep);
		}

		TableMode convertMode(Section mode)
		{
			switch (mode)
			{
				case Section.items: return TableMode.items;
				case Section.sales: return TableMode.sales;
				case Section.tenders: return TableMode.tenders;
				case Section.flightsTender: return TableMode.flightsTender;
				case Section.sifTender: return TableMode.sifTender;
				default: throw new ApplicationException();
			}
		}

		int getGroupCount(Section mode)
		{
			return getGroupCount(convertMode(mode));
		}

		int getGroupCount(TableMode mode)
		{
			switch (mode)
			{
				case TableMode.items: return a.groupCountItems;
				case TableMode.sales: return groupCountSales;
				case TableMode.tenders: return groupCountTenders;
				case TableMode.flightsSimple: return 0;
				case TableMode.flightsTender: return 0;
				case TableMode.sifTender: return 0;
				default: throw new ApplicationException();
			}
		}

		void advanceSection(ref int startAtRow, ref string currentGroupId, ref int groupsRemaining)
		{
			startAtRow = 0;
			currentGroupId = null;
			for (; ; )
			{
				switch (section)
				{
					case Section.items:
						section = documentType == DocumentType.PI ? Section.signature : Section.end;
						return;
					case Section.signature:
						section = Section.flightsTender;
						groupsRemaining = getGroupCount(section);
						if (flights.Count > 0) return;
						break;
					case Section.flightsTender:
						section = Section.sifTender;
						groupsRemaining = getGroupCount(section);
						if (flights.Count > 0) return;
						break;
					case Section.sifTender:
						section = Section.sales;
						groupsRemaining = getGroupCount(section);
						if (sales.Count > 0) return;
						break;
					case Section.sales:
						section = Section.tenders;
						groupsRemaining = getGroupCount(section);
						if (tenders.Count > 0) return;
						break;
					default:
						section = Section.end;
						return;
				}
			}
		}

		void printTable(PrintPageEventArgs e)
		{

			//check for no more items

			if (module_startAtRow == getRowCount(section))
			{
				module_startAtRow = 0;
				module_currentGroupId = null;
				advanceSection(ref module_startAtRow, ref module_currentGroupId, ref module_groupsRemaining);
				return;
			}

			//draw

			drawTable(e, convertMode(section), ref module_startAtRow, ref module_currentGroupId, ref module_groupsRemaining);

		}

		int getRowCount(Section mode)
		{
			return getRowCount(convertMode(mode));
		}

		int getRowCount(TableMode mode)
		{

			switch (mode)
			{
				case TableMode.sales:
					return sales.Count;
				case TableMode.tenders:
					return tenders.Count;
				case TableMode.flightsSimple:
				case TableMode.flightsTender:
					return flights.Count;
				case TableMode.sifTender:
					return 1;
				case TableMode.items:
					return items.Count;
				default:
					throw new ApplicationException();
			}

		}

		void drawTable(PrintPageEventArgs e, TableMode mode, ref int startAtRow, ref string currentGroupId, ref int groupsRemaining)
		{

			int headingRows = (documentType == DocumentType.Sif && mode == TableMode.items) ? 2 : 1;
			float height = e.PageBounds.Height - currentTop - 40;
			int rowCount = getRowCount(mode);
			int groupCount = getGroupCount(mode);

			//row height

			float rowHeight = (float)font10.Height * 1.2F;

			if (rowCount > 100 && mode == TableMode.items && documentType == DocumentType.Sif)
			{
				rowHeight = (float)font10.Height * 1.13F;
			}

			bool multiplePage = true;

			if ((rowCount + groupCount + headingRows) * font10.Height < height && mode == TableMode.items)//squeeze
			{
				multiplePage = false;
				rowHeight = height / (rowCount + groupCount + headingRows);
				if (rowHeight > (float)font10.Height * 1.2F)
				{
					multiplePage = true;
					rowHeight = (float)font10.Height * 1.2F;
				}
			}

			//watermark

			if (currentTop < 200)
			{

				string watermark = null;
				SizeF size;
				bool printPageNo = false;

				if (copies > 1)
				{
					if (!multiplePage)
					{
						watermark = copyThatIsPrinting.ToString();
						size = graphics.MeasureString(watermark, fontWatermark1);
						graphics.DrawString(watermark, fontWatermark1, brushWatermark, e.PageSettings.PaperSize.Width - 40 - size.Width, 40);
					}
					else
					{
						printPageNo = true;
					}
				}
				else
				{
					printPageNo = true;
				}

				if (printPageNo)
				{
					watermark = phrase(20, "Page") + " " + pageNumber.ToString();
					size = graphics.MeasureString(watermark, fontWatermark2);
					graphics.DrawString(watermark, fontWatermark2, brushWatermark, (e.PageSettings.PaperSize.Width - size.Width) / 2, 300);
				}

			}

			//new page

			if (mode != TableMode.flightsSimple)
			{
				if (startAtRow == 0 && (currentTop + rowHeight * 5) > e.PageBounds.Height - 40)
				{
					return;
				}
			}

			//sub heading

			if (startAtRow == 0)
			{

				if (mode == TableMode.sales)
				{
					graphics.DrawString(phrase(21, "List of sales"), fontLarge, brush, 40, currentTop);
					currentTop += fontLarge.Height * 1.2F;
				}

				if (mode == TableMode.tenders)
				{
					graphics.DrawString(phrase(22, "List of payments"), fontLarge, brush, 40, currentTop);
					currentTop += fontLarge.Height * 1.2F;
				}

				if (mode == TableMode.flightsTender)
				{
					graphics.DrawString(phrase(23, "Flight tender"), fontLarge, brush, 40, currentTop);
					currentTop += fontLarge.Height * 1.2F;
				}

				if (mode == TableMode.sifTender)
				{
					graphics.DrawString(phrase(24, "Sif tender"), fontLarge, brush, 40, currentTop);
					currentTop += fontLarge.Height * 1.2F;
				}

			}
			else
			{

				if (mode == TableMode.sales)
				{
					graphics.DrawString(phrase(25, "Sales cont/..."), font10bold, brush, 40, currentTop);
					currentTop += font10bold.Height;
				}

				if (mode == TableMode.tenders)
				{
					graphics.DrawString(phrase(26, "Tender cont/..."), font10bold, brush, 40, currentTop);
					currentTop += font10bold.Height;
				}

				if (mode == TableMode.flightsTender)
				{
					graphics.DrawString(phrase(27, "Credit cards & cash cont/..."), fontLarge, brush, 40, currentTop);
					currentTop += fontLarge.Height * 1.2F;
				}

			}

			//headings

			float headings0top = currentTop;

			string[] headings1;

			switch (mode)
			{
				case TableMode.flightsSimple:
					headings1 = new string[] { "#", phrase(28, "Flight no."), phrase(29, "From/to"), phrase(30, "Departure"), phrase(31, "Sellers") };
					break;
				case TableMode.sales:
					headings1 = new string[] { "Kit", phrase(32, "Seat"), phrase(33, "Type"), phrase(34, "Order#"), phrase(35, "Time"), phrase(36, "Qty"), phrase(37, "Sku"), phrase(38, "Description"), phrase(39, "Each"), phrase(40, "Total") };
					if (noSeat)
					{
						headings1[1] = null;
					}
					break;
				case TableMode.tenders:
					headings1 = new string[] { phrase(32, "Seat"), phrase(33, "Type"), phrase(34, "Order#"), phrase(35, "Time"), phrase(33, "Type"), phrase(41, "Amount"), phrase(42, "Amount base") };
					if (noSeat)
					{
						headings1[0] = null;
					}
					break;
				case TableMode.flightsTender:
					headings1 = flightsTenderHeadings;
					break;
				case TableMode.sifTender:
					headings1 = sifTenderHeadings;
					break;
				case TableMode.items:
				default:
					switch (documentType)
					{
						case DocumentType.Sif:
                            //AirCanada AX
                            if(airline!=AX)
							    headings1 = new string[] { phrase(43, "Item #"), "", phrase(44, "Price"), phrase(45, "Board"), phrase(46, "Begin"), phrase(47, "Sales"), phrase(48, "End"), phrase(46, "Begin"), phrase(47, "Sales"), phrase(48, "End"), phrase(49, "Return"), phrase(50, "Damages"), phrase(51, "Uplift") };
                            else
							    headings1 = new string[] { phrase(43, "Item #"),"Item AX #", "", phrase(44, "Price"), phrase(45, "Board"), phrase(46, "Begin"), phrase(47, "Sales"), phrase(48, "End"), phrase(46, "Begin"), phrase(47, "Sales"), phrase(48, "End"), phrase(49, "Return"), phrase(50, "Damages"), phrase(51, "Uplift") };

							currentTop += rowHeight;
							break;
						case DocumentType.PI:
                            //AirCanada AX
                            if (airline != AX)
							    headings1 = new string[] { phrase(43, "Item #"), "", phrase(44, "Price"), phrase(45, "Board"), phrase(52, "Total sold"), phrase(53, "Return count"), phrase(54, "PI count"), phrase(50, "Damages"), phrase(51, "Uplift"), phrase(55, "Stripped") };
                            else
							    headings1 = new string[] { phrase(43, "Item #"),"Item AX #", "", phrase(44, "Price"), phrase(45, "Board"), phrase(52, "Total sold"), phrase(53, "Return count"), phrase(54, "PI count"), phrase(50, "Damages"), phrase(51, "Uplift"), phrase(55, "Stripped") };

							break;
						default: throw new ApplicationException();
					}
					break;
			}

			//X widths

			int[] widths = new int[headings1.Length];

			expandWidths(true, headings1, ref widths, mode);

			for (int i = 0; i < rowCount; i++)
			{
				switch (mode)
				{
					case TableMode.items: expandWidths(false, items[i].fields, ref widths, mode); break;
					case TableMode.sales: expandWidths(false, sales[i].fields, ref widths, mode); break;
					case TableMode.tenders: expandWidths(false, tenders[i].fields, ref widths, mode); break;
					case TableMode.flightsSimple: expandWidths(false, flights[i].fieldsSimple, ref widths, mode); break;
					case TableMode.flightsTender: expandWidths(false, flights[i].fieldsWithTender, ref widths, mode); break;
					case TableMode.sifTender: expandWidths(false, sifTenderFields, ref widths, mode); break;
				}
			}

			//print lines

			int maxX = e.PageSettings.PaperSize.Width - 80 - 30;

			//measure height of boxes

			float measureTop = currentTop;

			measureTop += rowHeight;

			for (int i = startAtRow; i < (rowCount + groupsRemaining); i++)
			{
				measureTop += rowHeight;
				if (multiplePage && (measureTop + rowHeight) > (e.PageBounds.Height - 40))
				{
					break;
				}
			}

			//draw vertical lines

			int x = 0;
			for (int step = 0; ; step++)
			{

				if (x > maxX) break;

				float lineTop = currentTop;

				pen.Width = 1;

				if (documentType == DocumentType.Sif && mode == TableMode.items)
				{
                    //AirCanada AX
                    if (airline != AX)
                    {
                        switch (step)
                        {
                            case 4:
                                lineTop = headings0top;
                                pen.Width = 1;
                                break;
                            case 7:
                            case 10:
                                lineTop = headings0top;
                                pen.Width = 3;
                                break;
                            case 13:
                                lineTop = currentTop;
                                pen.Width = 3;
                                break;
                            default:
                                lineTop = currentTop;
                                pen.Width = 1;
                                break;
                        }
                    }
                    else
                    {
                        switch (step)
                        {
                            case 5:
                                lineTop = headings0top;
                                pen.Width = 1;
                                break;
                            case 8:
                            case 11:
                                lineTop = headings0top;
                                pen.Width = 3;
                                break;
                            case 14:
                                lineTop = currentTop;
                                pen.Width = 3;
                                break;
                            default:
                                lineTop = currentTop;
                                pen.Width = 1;
                                break;
                        }
                    }
				}

				if (step == 0)
				{
					x = 40;
				}
				else if ((step - 1) < headings1.Length)
				{
					x += widths[step - 1];
				}
				else
				{
					x += 30;
				}

				graphics.DrawLine(pen, x, lineTop, x, measureTop);

			}

			int rightX = x;

			if (documentType == DocumentType.Sif && mode == TableMode.items)
			{

				//very top line

				int x1 = 40 + widths[0] + widths[1] + widths[2] + widths[3];
				int x2 = x1 + widths[4] + widths[5] + widths[6];
				int x3 = x2 + widths[7] + widths[8] + widths[9];

                //AirCanada AX
                if (airline == AX)
                {
                    x1 = 40 + widths[0] + widths[1] + widths[2] + widths[3] + widths[4];
                    x2 = x1 + widths[5] + widths[6] + widths[7];
                    x3 = x2 + widths[8] + widths[9] + widths[10];

                }

				graphics.DrawLine(pen, x1, headings0top, x3, headings0top);

				//sector headings

				graphics.DrawString(phrase(56, "Sector 1 counts"), font10, brush, x1, headings0top);
				graphics.DrawString(phrase(57, "Sector 2 counts"), font10, brush, x2, headings0top);

			}

			//print rows

			graphics.DrawLine(pen, 40, currentTop, rightX, currentTop);

			printItemsRow(headings1, headings1, widths, rowHeight, false, mode, true);

			//print items

			for (int i = startAtRow; i < rowCount; i++)
			{

				string[] fields;
				string groupId;
				string groupName;
				bool strikeout;
				Flight flight;

				switch (mode)
				{
					case TableMode.flightsSimple:
						flight = flights[i];
						fields = flight.fieldsSimple;
						groupId = null;
						groupName = null;
						strikeout = false;
						break;
					case TableMode.flightsTender:
						flight = flights[i];
						fields = flight.fieldsWithTender;
						groupId = null;
						groupName = null;
						strikeout = false;
						break;
					case TableMode.sifTender:
						fields = sifTenderFields;
						groupId = null;
						groupName = null;
						strikeout = false;
						break;
					case TableMode.sales:
						Sale sale = sales[i];
						fields = sales[i].fields;
						groupId = sales[i].groupId;
						groupName = sale.groupName;
						strikeout = sale.voided;
						break;
					case TableMode.tenders:
						Tender tender = tenders[i];
						fields = tender.fields;
						groupId = tender.groupId;
						groupName = tender.groupName;
						strikeout = tender.voided;
						break;
					case TableMode.items:
						Item item = items[i];
						fields = items[i].fields;
						groupId = items[i].groupId;
						groupName = (a.byCategory ? phrase(58, "Category") + ": " : phrase(59, "Location") + ": ") + item.groupName;
						strikeout = false;
						break;
					default: throw new ApplicationException();
				}

				if (groupId != currentGroupId && groupCount != 0)
				{

					graphics.DrawLine(pen, 40, currentTop, rightX, currentTop);
					printGroup(groupName, rowHeight);
					groupsRemaining--;

					if (multiplePage && (currentTop + rowHeight) > (e.PageBounds.Height - 40))
					{
						startAtRow = i;
						break;
					}

					currentGroupId = groupId;

				}

				graphics.DrawLine(pen, 40, currentTop, rightX, currentTop);
				printItemsRow(headings1, fields, widths, rowHeight, strikeout, mode, false);

				if (multiplePage && (currentTop + rowHeight) > (e.PageBounds.Height - 40))
				{
					graphics.DrawLine(pen, 40, currentTop, rightX, currentTop);
					startAtRow = i + 1;
					break;
				}

				if (i == rowCount - 1)
				{
					graphics.DrawLine(pen, 40, currentTop, rightX, currentTop);
					if (mode != TableMode.flightsSimple)
					{
						advanceSection(ref startAtRow, ref currentGroupId, ref groupsRemaining);
					}
				}

			}

			if (documentType == DocumentType.Sif && mode == TableMode.flightsSimple)
			{
				//gap not required because the top of the Sif table has raised columns like this "xxxxxxxxXXXxXXxxxxxx"
			}
			else
			{
				currentTop += font10.Height;
			}

		}

		bool noSeat
		{
			get
			{
				return airline == 24;
			}
		}

		int airline
		{
			get
			{
				if (bondroom == null) return -1;
				return bondroom.airlineCode;
			}
		}

		string[] sifTenderFields
		{
			get
			{

				string[] sa = new string[] { sifCreditCardTransactions.ToString(), sifCashTransactions.ToString(), sifTransactions.ToString(), ((float)sifCreditCardsBase).ToString() };

				string[] sa2 = new string[sa.Length + a.sifCurrencyTotalRows.Count];

				for (int i = 0; i < sa.Length; i++)
				{
					sa2[i] = sa[i];
				}

				for (int i = 0; i < a.sifCurrencyTotalRows.Count; i++)
				{
					sa2[i + sa.Length] = ((float)a.sifCurrencyTotalRows[i].value).ToString();
				}

				return sa2;

			}
		}

		string[] sifTenderHeadings
		{
			get
			{

				string[] sa = new string[] { phrase(60, "Credit card transactions"), phrase(61, "Cash transactions"), phrase(62, "Total transactions"), phrase(63, "Credit cards") };

				string[] sa2 = new string[sa.Length + a.sifCurrencyTotalRows.Count];

				for (int i = 0; i < sa.Length; i++)
				{
					sa2[i] = sa[i];
				}

				for (int i = 0; i < a.sifCurrencyTotalRows.Count; i++)
				{
					sa2[i + sa.Length] = a.sifCurrencyTotalRows[i].currency;
				}

				return sa2;

			}
		}

		string[] flightsTenderHeadings
		{
			get
			{

				string[] sa = new string[] { "#", phrase(28, "Flight no."), phrase(29, "From/to"), phrase(30, "Departure"), phrase(63, "Credit cards") };

				string[] sa2 = new string[sa.Length + a.sifCurrencyTotalRows.Count];

				for (int i = 0; i < sa.Length; i++)
				{
					sa2[i] = sa[i];
				}

				for (int i = 0; i < a.sifCurrencyTotalRows.Count; i++)
				{
					sa2[i + sa.Length] = a.sifCurrencyTotalRows[i].currency;
				}

				return sa2;

			}
		}

		void expandWidths(bool headings, string[] sa, ref int[] widths, TableMode mode)
		{

			SizeF s;

			for (int i1 = 0; i1 < sa.Length; i1++)
			{
				if (sa[i1] != null)
				{
					s = graphics.MeasureString(sa[i1] + " ", headings && a.spanish && mode == TableMode.items ? font7 : font10);
					if (s.Width > widths[i1])
					{
						widths[i1] = (int)s.Width;
					}
				}
			}
		}

		void printGroup(string groupName, float rowHeight)
		{
			currentLeft = 40;
			graphics.DrawString(groupName, font10bold, brush, currentLeft, currentTop);
			currentTop += rowHeight;
		}

		void printItemsRow(string[] headings, string[] sa, int[] widths, float rowHeight, bool strikeout, TableMode mode, bool heading)
		{

			SizeF s;

			currentLeft = 40;

			for (int i = 0; i < sa.Length; i++)
			{

				bool rightJust = false;

				switch (mode)
				{
					case TableMode.items: rightJust = i > 1; break;
					case TableMode.sales: rightJust = i > 7; break;
					case TableMode.tenders: rightJust = i > 3; break;
					case TableMode.flightsTender: rightJust = i > 3; break;
					case TableMode.sifTender: rightJust = true; break;
				}

				Font font = strikeout ? font10strikeout : heading && a.spanish && mode == TableMode.items ? font7 : font10;

				if (rightJust)
				{
					s = graphics.MeasureString(sa[i], font);
					graphics.DrawString(sa[i], font, strikeout ? brushSilver : brush, currentLeft + widths[i] - s.Width, currentTop);
				}
				else
				{
					graphics.DrawString(sa[i], font, strikeout ? brushSilver : brush, currentLeft, currentTop);
				}
				currentLeft += widths[i];

			}

			currentTop += rowHeight;

		}

		void printSeals()
		{

			currentLeft = 40;

			if (documentType == DocumentType.PI)
			{
				printField("Kit", kitDesc);
			}

			SizeF s;

			string text1 = documentType == DocumentType.PI ? phrase(64, "Closing seals") + " #(s): " : phrase(65, "Seals") + " #(s): ";

			s = graphics.MeasureString(text1, font10);

			graphics.DrawString(text1, font10, brush, currentLeft, currentTop);

			currentLeft += (int)s.Width;

			foreach (string sealNo in seals)
			{

				s = graphics.MeasureString(sealNo + " ", font10);

                if (currentLeft + (int)s.Width > graphics.VisibleClipBounds.Width)
                {
                    currentLeft = 40;
                    currentTop += font10.Height;
                }
				graphics.DrawString(sealNo, font10, brush, currentLeft, currentTop);

				currentLeft += (int)s.Width;

			}

			currentLeft += 10;
			currentTop += font10.Height;

		}

		void printField(string text1, string text2)
		{

			SizeF s;

			if (!text1.EndsWith("#")) text1 += ":";

			text1 += " ";

			s = graphics.MeasureString(text1, font10);

			graphics.DrawString(text1, font10, brush, currentLeft, currentTop);

			currentLeft += (int)s.Width;

			s = graphics.MeasureString(text2, font10);

			graphics.DrawString(text2, font10, brush, currentLeft, currentTop);

			currentLeft += (int)s.Width + 10;

		}

		void printField(string text1)
		{

			SizeF s;

			text1 += " ";

			s = graphics.MeasureString(text1, font10);

			graphics.DrawString(text1, font10, brush, currentLeft, currentTop);

		}

		DateTime readDate()
		{
			byte[] ba = brResponse.ReadBytes(6);
			return new DateTime(ba[0] + 2000, ba[1], ba[2], ba[3], ba[4], ba[5]);
		}

		string secondsSuffix
		{
			get
			{
				if (seconds < 60) return "";
				float minutes = seconds / 60;
				return " (polling every " + minutes + " minute" + s(minutes) + ")";
			}
		}

		string s(float number)
		{
			return number == 1 ? "" : "s";
		}

		string readProcessServerMessage()
		{
			return brResponse.ReadString();
		}

		void serverStatus(string text)
		{
			this.Invoke(serverStatus_handler, text);
		}

		static public string GetVersion()
		{
			Version ver = Assembly.GetExecutingAssembly().GetName().Version;
			return ver.Major + "." + ver.Minor + "." + ver.Build;
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (ignoreDropdownEvent) return;

			bondroom = null;
			seconds = 0;
			labelStatusServer.Text = "Initialising";

			if (dropdownBondroom.SelectedIndex == -1)//ok
			{
				return;
			}

			try
			{
				ignoreDropdownEvent = true;
				a.spanish = ((Bondroom)dropdownBondroom.SelectedItem).airlineCode == 24 || ((Bondroom)dropdownBondroom.SelectedItem).airlineCode == 31;
				dropdownLang.SelectedIndex = a.spanish ? 1 : 0;
			}
			finally
			{
				ignoreDropdownEvent = false;
			}

			DialogResult result1 = MessageBox.Show(dropdownBondroom.SelectedItem.ToString() + " - is that correct?", Text, MessageBoxButtons.YesNo);//ok
			switch (result1)
			{
				case DialogResult.No:
					bondroom = null;
					DeleteRegistry();
					dropdownBondroom.SelectedIndex = -1;//ok
					dropdownBondroom.Enabled = true;//ok
					dropdownLang.Enabled = true;
					break;
				case DialogResult.Yes:
					MessageBox.Show("In the future you must press Ctrl+C to change the settings.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					bondroom = (Bondroom)dropdownBondroom.SelectedItem;//ok
					SaveRegistry(bondroom);
					dropdownBondroom.Enabled = false;//ok
					dropdownLang.Enabled = false;
					break;
			}

			seconds = 0;
			labelStatusServer.Text = "Initialising";

		}

		int seconds
		{
			get
			{
				lock (a.lockObject)
				{ return _seconds; }
			}
			set
			{
				lock (a.lockObject)
				{
					_seconds = value;
				}
			}
		}

		private void Form1_Activated(object sender, EventArgs e)
		{
			seconds = 1;
		}

		private void Form1_KeyPress(object sender, KeyPressEventArgs e)
		{

		}

		private void Form1_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.C)
			{
				dropdownBondroom.Enabled = true;//ok
				dropdownLang.Enabled = true;
				e.SuppressKeyPress = true;
			}

		}

		/// <summary>
		/// Saves a value in the registry
		/// </summary>
		public void SaveRegistry(Bondroom bondroom)
		{
			RegistryKey rk = Registry.CurrentUser;
			rk = rk.OpenSubKey("Software", true);
			rk = rk.CreateSubKey("DFASS");
			rk = rk.CreateSubKey("MDCPrintAgent");
			rk.SetValue("newAirlineCode", bondroom.airlineCode);
			if (bondroom.sifPrefix == null)
			{
				rk.DeleteValue("sifPrefix", false);
			}
			else
			{
				rk.SetValue("sifPrefix", bondroom.sifPrefix);
			}
		}

		/// <summary>
		/// delete a value in the registry
		/// </summary>
		public void DeleteRegistry()
		{

			RegistryKey rk = Registry.CurrentUser;
			rk = rk.OpenSubKey("Software", true);
			rk = rk.CreateSubKey("DFASS");
			rk = rk.CreateSubKey("MDCPrintAgent");
			rk.DeleteValue("newAirlineCode", false);
			rk.DeleteValue("sifPrefix", false);
		}

		/// <summary>
		/// Reads a value from the registry
		/// </summary>
		/// <returns>A Bondroom object</returns>
		public static Bondroom ReadRegistry()
		{

			try
			{

				RegistryKey rk = Registry.CurrentUser;
				rk = rk.OpenSubKey("Software", true);
				rk = rk.CreateSubKey("DFASS");
				rk = rk.CreateSubKey("MDCPrintAgent");

				object ac = rk.GetValue("newAirlineCode");

				if (ac == null) return null;

				int airlineCode = (int)ac;
				if (airlineCode == 902) airlineCode = 903;//todotime, remap May 2012

				object sp = rk.GetValue("sifPrefix");

				string sifPrefix = sp == null ? null : (string)sp;

				Bondroom b = new Bondroom(airlineCode, sifPrefix);
				return b;

			}
			catch
			{
				return null;
			}

		}

		private void dropdownLang_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (ignoreDropdownEvent) return;

			if (dropdownBondroom.SelectedIndex == -1)//ok
			{
				return;
			}

			a.spanish = dropdownLang.SelectedIndex == 1;

			seconds = 0;
			labelStatusServer.Text = "Initialising";

			MessageBox.Show("In the future you must press Ctrl+C to change the settings.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			SaveRegistry(bondroom);
			dropdownBondroom.Enabled = false;
			dropdownLang.Enabled = false;

			seconds = 0;
			labelStatusServer.Text = "Initialising";

		}

	}

	public class Bondroom
	{

		public string sifPrefix;
		public int airlineCode;

		public Airline airline
		{
			get
			{
				return (Airline)airlineCode;
			}
			set
			{
				airlineCode = (int)value;
			}
		}

		public Bondroom(int airlineCode, string sifPrefix)
		{
			this.airlineCode = airlineCode;
			this.sifPrefix = sifPrefix;
		}

		public override string ToString()
		{

			switch (airlineCode)
			{

				case 1014:
					if (sifPrefix == "DPS") return "1014 & 1015 " + sifPrefix;//see "1014 & 1015 DPS share the same printer"
					return airlineCode + " " + sifPrefix;

				case 1015:
					if (sifPrefix == "DPS") return "1014 & 1015 " + sifPrefix;//see "1014 & 1015 DPS share the same printer"
					return airlineCode + " " + sifPrefix;
					
				case 2903:
				case 2908:
					return "2903 & 2908 " + sifPrefix;

				case 1008:
				case 1009:
					return "1008 & 1009";

				case 35:
				case 36:
				case 38:
				case 39:
				case 1000:
				case 1006:
					return "35, 36, 38, 39, 1000 & 1006";

				case 48:
				case 1004:
					return "48 & 1004";
                case 1034:
                case 1035:
                    return "1034 & 1035 " + sifPrefix;

			}

			return airlineCode + " " + sifPrefix;

		}

	}

	struct Item : IComparable
	{
        public bool displayTransferCount;

		public string sku;

        //AirCanada AX
        public int airline;
        public string skuAX;

		public string _buttonText;
		public float retailPrice;
		public string location, category;
		public bool inMoreDrawers;
		public int packerOut,crewBeginRouteAdjust, crewSold, crewReturn, returnTotal, returnDamaged, replenish;
		public int categoryId;

		public override string ToString()
		{
			string secondLevel = a.bondroom.airline == Airline.pluna ? buttonText : sku;
			if (a.byCategory)
			{
				return categoryId.ToString("000") + secondLevel;
			}
			if (a.groupCountItems > 0)
			{

				if (a.specialLocationSorting && location != null && location.Length == 2)
				{

					char[] ca = location.ToCharArray();

					if (char.IsDigit(ca[0]) && char.IsLetter(ca[1]))
					{
						location = ca[1].ToString() + ca[0].ToString();
					}

				}

				return location + secondLevel;
			}
			return sku;
		}

		public string groupName
		{
			get
			{
				return a.byCategory ? category : location;
			}
		}

		public string groupId
		{
			get
			{
				return a.byCategory ? categoryId.ToString() : location;
			}
		}

		public string buttonText
		{
			get
			{
				return inMoreDrawers && a.groupCountItems != 0 && a.byLocation ? _buttonText + "*" : _buttonText;
			}
			set
			{
				_buttonText = value;
			}
		}

		int virtualReturn
		{
			get
			{
                if (packerOut + crewBeginRouteAdjust - crewSold > 0) return packerOut + crewBeginRouteAdjust - crewSold;
				return 0;
			}
		}

		public string[] fields
		{
			get
			{

				switch (a.documentType)
				{

					case DocumentType.PI:
                        string board = packerOut.ToString();
                        if (displayTransferCount && crewBeginRouteAdjust != 0)
                        {
                            if (crewBeginRouteAdjust > 0)
                                board += "+" + crewBeginRouteAdjust;
                            else
                                board += crewBeginRouteAdjust.ToString();
                        }
                        //AirCanada AX
                        if (airline != Form1.AX)
                        {
                            if (a.piCartInboundFigures)
                            {
                                return new string[] { sku, buttonText, retailPrice.ToString(), board, crewSold.ToString(), virtualReturn.ToString(), returnTotal.ToString(), format(returnDamaged), format(replenish) };
                            }
                            else if (a.piSoldFigures)
                            {
                                return new string[] { sku, buttonText, retailPrice.ToString(), board, crewSold.ToString(), crewReturn.ToString() };
                            }
                            else
                            {
                                return new string[] { sku, buttonText, retailPrice.ToString(), board };
                            }
                        }
                        else
                        {
                            if (a.piCartInboundFigures)
                            {
                                return new string[] { sku, skuAX, buttonText, retailPrice.ToString(), board, crewSold.ToString(), virtualReturn.ToString(), returnTotal.ToString(), format(returnDamaged), format(replenish) };
                            }
                            else if (a.piSoldFigures)
                            {
                                return new string[] { sku, skuAX, buttonText, retailPrice.ToString(), board, crewSold.ToString(), crewReturn.ToString() };
                            }
                            else
                            {
                                return new string[] { sku, skuAX, buttonText, retailPrice.ToString(), board };
                            }
                        }

					case DocumentType.Sif:
                        //AirCanada AX
                        if (airline != Form1.AX)
						    return new string[] { sku, buttonText, retailPrice.ToString(), packerOut.ToString() };
                        else
                            return new string[] { sku,skuAX, buttonText, retailPrice.ToString(), packerOut.ToString() };

					default: throw new ApplicationException();

				}
			}
		}

		static string format(int count)
		{
			if (count == 0) return null;
			return count.ToString();
		}

		public int CompareTo(object obj)
		{
			return this.ToString().CompareTo(obj.ToString());
		}

	}

}
