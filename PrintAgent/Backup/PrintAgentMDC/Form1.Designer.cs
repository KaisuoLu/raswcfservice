﻿namespace PrintAgentMDC
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.dropdownBondroom = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.labelStatusServer = new System.Windows.Forms.Label();
			this.labelSoftwareVersion = new System.Windows.Forms.Label();
			this.labelBig = new System.Windows.Forms.Label();
			this.dropdownLang = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(21, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(74, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Bond room";
			// 
			// dropdownBondroom
			// 
			this.dropdownBondroom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dropdownBondroom.FormattingEnabled = true;
			this.dropdownBondroom.Location = new System.Drawing.Point(163, 20);
			this.dropdownBondroom.Name = "dropdownBondroom";
			this.dropdownBondroom.Size = new System.Drawing.Size(208, 24);
			this.dropdownBondroom.TabIndex = 1;
			this.dropdownBondroom.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(21, 104);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(116, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Server connection";
			// 
			// labelStatusServer
			// 
			this.labelStatusServer.AutoSize = true;
			this.labelStatusServer.Location = new System.Drawing.Point(160, 104);
			this.labelStatusServer.Name = "labelStatusServer";
			this.labelStatusServer.Size = new System.Drawing.Size(66, 16);
			this.labelStatusServer.TabIndex = 3;
			this.labelStatusServer.Text = "Initialising";
			// 
			// labelSoftwareVersion
			// 
			this.labelSoftwareVersion.AutoSize = true;
			this.labelSoftwareVersion.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelSoftwareVersion.Location = new System.Drawing.Point(21, 146);
			this.labelSoftwareVersion.Name = "labelSoftwareVersion";
			this.labelSoftwareVersion.Size = new System.Drawing.Size(125, 16);
			this.labelSoftwareVersion.TabIndex = 9;
			this.labelSoftwareVersion.Text = "Software version x.x";
			// 
			// labelBig
			// 
			this.labelBig.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
									| System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.labelBig.ForeColor = System.Drawing.Color.DarkRed;
			this.labelBig.Location = new System.Drawing.Point(24, 189);
			this.labelBig.Name = "labelBig";
			this.labelBig.Size = new System.Drawing.Size(587, 185);
			this.labelBig.TabIndex = 11;
			this.labelBig.Text = "labelBig";
			// 
			// dropdownLang
			// 
			this.dropdownLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dropdownLang.FormattingEnabled = true;
			this.dropdownLang.Items.AddRange(new object[] {
            "English",
            "Español"});
			this.dropdownLang.Location = new System.Drawing.Point(163, 62);
			this.dropdownLang.Name = "dropdownLang";
			this.dropdownLang.Size = new System.Drawing.Size(121, 24);
			this.dropdownLang.TabIndex = 13;
			this.dropdownLang.SelectedIndexChanged += new System.EventHandler(this.dropdownLang_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(21, 62);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(59, 16);
			this.label3.TabIndex = 14;
			this.label3.Text = "Output in";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(623, 383);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.dropdownLang);
			this.Controls.Add(this.labelBig);
			this.Controls.Add(this.labelSoftwareVersion);
			this.Controls.Add(this.labelStatusServer);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.dropdownBondroom);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Print Agent RAS";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Activated += new System.EventHandler(this.Form1_Activated);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox dropdownBondroom;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label labelStatusServer;
		private System.Windows.Forms.Label labelSoftwareVersion;
		private System.Windows.Forms.Label labelBig;
		private System.Windows.Forms.ComboBox dropdownLang;
		private System.Windows.Forms.Label label3;
	}
}

