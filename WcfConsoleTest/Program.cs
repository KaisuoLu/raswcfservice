﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ConsoleApplication2.ServiceReference1;
namespace ConsoleApplication2
{
    class Program
    {
        public static string DBO_KEY = "initium";
        public static String DecryptNumber(string e_cipher)
        {
            if (e_cipher == null)
                return null;
            StringBuilder sb = new StringBuilder(e_cipher.Length);
            for (int i = 0; i < e_cipher.Length; i++)
            {
                int intXOrValue1 = (int)e_cipher[i];
                int intXOrValue2 = (int)DBO_KEY[(i + 1) % DBO_KEY.Length];
                char c = (char)(intXOrValue1 ^ intXOrValue2);
                sb.Append(c);
            }

            return sb.ToString();

        }


        public static String EncryptNumber(string e_cipher)
        {

            if (e_cipher == null)
                return null;
            StringBuilder sb = new StringBuilder(e_cipher.Length);
            for (int i = 0; i < e_cipher.Length; i++)
            {
                int intXOrValue1 = (int)e_cipher[i];
                int intXOrValue2 = (int)DBO_KEY[(i + 1) % DBO_KEY.Length];
                char c = (char)(intXOrValue1 ^ intXOrValue2);
                sb.Append(c);
            }
            return sb.ToString();

        }   
        static void Main(string[] args)
        {
            ISalesService service=new SalesServiceClient();            
            GetPreordersRequest request=new GetPreordersRequest();
            request.AirlineCode = 1026;
            request.FlightNo = 107;
            request.From = "HKG";
            request.To = "HAK";
            request.DepartureDate = new DateTime(2019, 1, 2);
            GetPreordersResult result = service.GetPreorders(request);

            //string cd = "5105105105105100"; //4544150000550150 4111111111111111
            //string ecd = EncryptNumber(cd);
            //byte[] toBytes = Encoding.ASCII.GetBytes(ecd);
            //string dcd = DecryptNumber(ecd);


            string fl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_FlightRecorded><Tab_FlightRecorded xmlns=\"Tab_FlightRecorded\"><FR_AirlineCode>1049</FR_AirlineCode><FR_SifNo>MIS 100022</FR_SifNo><FR_FlightCode>MI</FR_FlightCode><FR_FlightNo>800</FR_FlightNo><FR_From>SIN</FR_From><FR_To>PEK</FR_To><FR_DepartureDate>2017-08-25T00:00:00</FR_DepartureDate><FR_Sector>0</FR_Sector><FR_PaxCount>0</FR_PaxCount><FR_KitCode>K1</FR_KitCode><FR_Region>EA</FR_Region><FR_CartNo>EC801S1</FR_CartNo><FR_DeviceId>12345</FR_DeviceId><FR_DeviceSyncKey>849279</FR_DeviceSyncKey><FR_Hide>0</FR_Hide></Tab_FlightRecorded><Tab_FlightRecorded xmlns=\"Tab_FlightRecorded\"><FR_AirlineCode>1049</FR_AirlineCode><FR_SifNo>MIS 100022</FR_SifNo><FR_FlightCode>MI</FR_FlightCode><FR_FlightNo>917</FR_FlightNo><FR_From>SIN</FR_From><FR_To>MNL</FR_To><FR_DepartureDate>2017-09-01T05:30:33</FR_DepartureDate><FR_Sector>0</FR_Sector><FR_PaxCount>0</FR_PaxCount><FR_KitCode>K1</FR_KitCode><FR_Region>EA</FR_Region><FR_CartNo>EC801S1</FR_CartNo><FR_DeviceId>12345</FR_DeviceId><FR_DeviceSyncKey>849279</FR_DeviceSyncKey><FR_Hide>0</FR_Hide></Tab_FlightRecorded><Tab_FlightRecorded xmlns=\"Tab_FlightRecorded\"><FR_AirlineCode>1049</FR_AirlineCode><FR_SifNo>MIS 100022</FR_SifNo><FR_FlightCode>MI</FR_FlightCode><FR_FlightNo>917</FR_FlightNo><FR_From>SIN</FR_From><FR_To>MNL</FR_To><FR_DepartureDate>2017-09-01T00:00:00</FR_DepartureDate><FR_Sector>1</FR_Sector><FR_PaxCount>0</FR_PaxCount><FR_KitCode>K1</FR_KitCode><FR_Region>EA</FR_Region><FR_CartNo>EC801S1</FR_CartNo><FR_DeviceId>12345</FR_DeviceId><FR_DeviceSyncKey>849279</FR_DeviceSyncKey><FR_Hide>0</FR_Hide></Tab_FlightRecorded></Tab_FlightRecorded>";
            string oh = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_OrderHeader><Tab_OrderHeader xmlns=\"Tab_OrderHeader\"><OH_SifNo>MIS 100022</OH_SifNo><OH_Sector>1</OH_Sector><OH_OrderNo>1</OH_OrderNo><OH_Total>1406.0</OH_Total><OH_Voided>0</OH_Voided><OH_OrderTime>2017-09-01T08:25:21</OH_OrderTime><OH_Credit>0.0</OH_Credit><OH_OriginalSector>1</OH_OriginalSector><OH_Seat>999B</OH_Seat><OH_Type>S</OH_Type><OH_DeviceId>12345</OH_DeviceId><OH_DeviceSyncKey>849279</OH_DeviceSyncKey><OH_Passport/><OH_Service/><OH_PaxName/><OH_PaxEmail/><OH_PaxClass>Economy Class</OH_PaxClass><OH_CrewSale_CrewId/><OH_PointCardNo/><OH_PointCardType/></Tab_OrderHeader></Tab_OrderHeader>";
            string ol = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MIS 100022</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>1</OL_LineNo><OL_Sku>153465</OL_Sku><OL_ItemName>Pierre Cardin Ladies Watch Pendant &amp; Earring Set</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>690.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K1</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>849279</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MIS 100022</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>2</OL_LineNo><OL_Sku>147039</OL_Sku><OL_ItemName>TIMBERLAND HENNIKER WATCH</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>716.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K1</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>849279</OL_DeviceSyncKey></Tab_OrderLine></Tab_OrderLine>";
            string pl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_PaymentLine><Tab_PaymentLine xmlns=\"Tab_PaymentLine\"><PA_SifNo>MIS 100022</PA_SifNo><PA_Sector>1</PA_Sector><PA_OrderNo>1</PA_OrderNo><PA_LineNo>1</PA_LineNo><PA_TenderType>CA</PA_TenderType><PA_Currency>SGD</PA_Currency><PA_Amount>1406.0</PA_Amount><PA_AmountBase>1406.0</PA_AmountBase><PA_ExchangeRate>1.0</PA_ExchangeRate><PA_CardHolderName/><PA_ProcessStatus/><PA_HandheldMergeRowId>3486971067560394784</PA_HandheldMergeRowId><PA_DeviceId>12345</PA_DeviceId><PA_DeviceSyncKey>849279</PA_DeviceSyncKey><PA_CardNoBX/><PA_ExpiryDateBX/></Tab_PaymentLine></Tab_PaymentLine>";


            string xmlFlightRecorded = "";
            string xmlOrderHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_OrderHeader><Tab_OrderHeader xmlns=\"Tab_OrderHeader\"><OH_SifNo>MFC 300048</OH_SifNo><OH_Sector>1</OH_Sector><OH_OrderNo>1</OH_OrderNo><OH_Total>940.0</OH_Total><OH_Voided>0</OH_Voided><OH_OrderTime>2017-09-13T14:14:03</OH_OrderTime><OH_Credit>0.0</OH_Credit><OH_OriginalSector>1</OH_OriginalSector><OH_Seat>22A</OH_Seat><OH_Type>S</OH_Type><OH_DeviceId>MA2150</OH_DeviceId><OH_DeviceSyncKey>300048</OH_DeviceSyncKey><OH_Passport/><OH_Service/><OH_PaxName/><OH_PaxEmail/><OH_PaxClass>Economy Class</OH_PaxClass><OH_CrewSale_CrewId/><OH_PointCardNo/><OH_PointCardType/><OH_PointCardPoints>0.0</OH_PointCardPoints></Tab_OrderHeader></Tab_OrderHeader>";
            string xmlOrderLine = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>1</OL_LineNo><OL_Sku>120318</OL_Sku><OL_ItemName>Armani Acqua</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>254.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>2</OL_LineNo><OL_Sku>110926</OL_Sku><OL_ItemName>Armani Coffret</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>249.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>3</OL_LineNo><OL_Sku>111233</OL_Sku><OL_ItemName>Carolina GoodGirl</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>284.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>4</OL_LineNo><OL_Sku>111220</OL_Sku><OL_ItemName>Carolina MiniSet</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>202.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>5</OL_LineNo><OL_Sku>110096</OL_Sku><OL_ItemName>Chloe EDP</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>409.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>6</OL_LineNo><OL_Sku>110942</OL_Sku><OL_ItemName>Chloe Mini</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>258.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>7</OL_LineNo><OL_Sku>110031</OL_Sku><OL_ItemName>EA GreenTea</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>121.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>8</OL_LineNo><OL_Sku>111033</OL_Sku><OL_ItemName>EL PurseSpray</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>232.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>9</OL_LineNo><OL_Sku>111288</OL_Sku><OL_ItemName>Ferrag Feminie</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>235.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>10</OL_LineNo><OL_Sku>120445</OL_Sku><OL_ItemName>Ferrag Masculine</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>235.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>11</OL_LineNo><OL_Sku>111246</OL_Sku><OL_ItemName>Ferrag Signorina</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>268.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>12</OL_LineNo><OL_Sku>110930</OL_Sku><OL_ItemName>Gucci Bamboo EDP</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>374.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>1</OL_OrderNo><OL_LineNo>13</OL_LineNo><OL_Sku>@Disc10%</OL_Sku><OL_ItemName>@10% Off on Selected Skus</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>-312.0</OL_PriceEach><OL_Type>D</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>1</OL_LineNo><OL_Sku>156011</OL_Sku><OL_ItemName>Infinity Aimee</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>340.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>2</OL_LineNo><OL_Sku>154465</OL_Sku><OL_ItemName>Infinity Diana</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>426.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>3</OL_LineNo><OL_Sku>155807</OL_Sku><OL_ItemName>LES Glam</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>230.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>4</OL_LineNo><OL_Sku>153465</OL_Sku><OL_ItemName>PC Ladies W</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>690.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>5</OL_LineNo><OL_Sku>153471</OL_Sku><OL_ItemName>Monvien Necklace</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>306.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>6</OL_LineNo><OL_Sku>155806</OL_Sku><OL_ItemName>LES Medley</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>152.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>7</OL_LineNo><OL_Sku>153472</OL_Sku><OL_ItemName>PC PendEaring Set</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>264.0</OL_PriceEach><OL_Type>P</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine><Tab_OrderLine xmlns=\"Tab_OrderLine\"><OL_SifNo>MFC 300045</OL_SifNo><OL_Sector>1</OL_Sector><OL_OrderNo>2</OL_OrderNo><OL_LineNo>8</OL_LineNo><OL_Sku>@Disc10%</OL_Sku><OL_ItemName>@10% Off on Selected Skus</OL_ItemName><OL_Quantity>1</OL_Quantity><OL_PriceEach>-241.0</OL_PriceEach><OL_Type>D</OL_Type><OL_KitCode>K3</OL_KitCode><OL_DeviceId>12345</OL_DeviceId><OL_DeviceSyncKey>300045</OL_DeviceSyncKey></Tab_OrderLine></Tab_OrderLine>";
            string xmlPaymentLine = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_PaymentLine><Tab_PaymentLine xmlns=\"Tab_PaymentLine\"><PA_SifNo>MFC 300045</PA_SifNo><PA_Sector>1</PA_Sector><PA_OrderNo>1</PA_OrderNo><PA_LineNo>1</PA_LineNo><PA_TenderType>CA</PA_TenderType><PA_Currency>MYR</PA_Currency><PA_Amount>1800.0</PA_Amount><PA_AmountBase>1800.0</PA_AmountBase><PA_ExchangeRate>1.0</PA_ExchangeRate><PA_CardHolderName/><PA_ProcessStatus/><PA_HandheldMergeRowId>420668571157299232</PA_HandheldMergeRowId><PA_DeviceId>12345</PA_DeviceId><PA_DeviceSyncKey>300045</PA_DeviceSyncKey><PA_CardNoBX/><PA_ExpiryDateBX/></Tab_PaymentLine><Tab_PaymentLine xmlns=\"Tab_PaymentLine\"><PA_SifNo>MFC 300045</PA_SifNo><PA_Sector>1</PA_Sector><PA_OrderNo>1</PA_OrderNo><PA_LineNo>2</PA_LineNo><PA_TenderType>CC</PA_TenderType><PA_Currency>MYR</PA_Currency><PA_Amount>1009.0</PA_Amount><PA_AmountBase>1009.0</PA_AmountBase><PA_ExchangeRate>1.0</PA_ExchangeRate><PA_CardHolderName>L</PA_CardHolderName><PA_ProcessStatus/><PA_HandheldMergeRowId>420668571157299233</PA_HandheldMergeRowId><PA_DeviceId>12345</PA_DeviceId><PA_DeviceSyncKey>300045</PA_DeviceSyncKey><PA_CardNoBX>ZXEXD\\X_XEXD\\X_X</PA_CardNoBX><PA_ExpiryDateBX>^XF\\</PA_ExpiryDateBX></Tab_PaymentLine><Tab_PaymentLine xmlns=\"Tab_PaymentLine\"><PA_SifNo>MFC 300045</PA_SifNo><PA_Sector>1</PA_Sector><PA_OrderNo>2</PA_OrderNo><PA_LineNo>1</PA_LineNo><PA_TenderType>CA</PA_TenderType><PA_Currency>MYR</PA_Currency><PA_Amount>2167.0</PA_Amount><PA_AmountBase>2167.0</PA_AmountBase><PA_ExchangeRate>1.0</PA_ExchangeRate><PA_CardHolderName/><PA_ProcessStatus/><PA_HandheldMergeRowId>420668571157299264</PA_HandheldMergeRowId><PA_DeviceId>12345</PA_DeviceId><PA_DeviceSyncKey>300045</PA_DeviceSyncKey><PA_CardNoBX/><PA_ExpiryDateBX/></Tab_PaymentLine></Tab_PaymentLine>";
            string xmlItemCount = null;
            string xmlCrewRecorded = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_CrewRecorded><Tab_CrewRecorded xmlns=\"Tab_CrewRecorded\"><CR_SifNo>MFC 300045</CR_SifNo><CR_Sector>1</CR_Sector><CR_CrewId>2100112</CR_CrewId><CR_Position>IFS</CR_Position><CR_RecordedDate>2017-09-13</CR_RecordedDate><CR_DeviceId>12345</CR_DeviceId><CR_DeviceSyncKey>300045</CR_DeviceSyncKey></Tab_CrewRecorded><Tab_CrewRecorded xmlns=\"Tab_CrewRecorded\"><CR_SifNo>MFC 300045</CR_SifNo><CR_Sector>1</CR_Sector><CR_CrewId>2100114</CR_CrewId><CR_Position>LS</CR_Position><CR_RecordedDate>2017-09-13</CR_RecordedDate><CR_DeviceId>12345</CR_DeviceId><CR_DeviceSyncKey>300045</CR_DeviceSyncKey></Tab_CrewRecorded></Tab_CrewRecorded>";
            string xmlSeals = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_Seals><Tab_Seals xmlns=\"Tab_Seals\"><SE_CartNo>EC801S3</SE_CartNo><SE_SifNo>MFC 300045</SE_SifNo><SE_Sector>0</SE_Sector><SE_DisplaySeq>1</SE_DisplaySeq><SE_SealNo>00000222</SE_SealNo><SE_Status>BO</SE_Status><SE_RecordedDate>2017-09-13</SE_RecordedDate><SE_DeviceId>12345</SE_DeviceId><SE_DeviceSyncKey>300045</SE_DeviceSyncKey></Tab_Seals><Tab_Seals xmlns=\"Tab_Seals\"><SE_CartNo>EC801S3</SE_CartNo><SE_SifNo>MFC 300045</SE_SifNo><SE_Sector>1</SE_Sector><SE_DisplaySeq>1</SE_DisplaySeq><SE_SealNo>00000023</SE_SealNo><SE_Status>CL</SE_Status><SE_RecordedDate>2017-09-13</SE_RecordedDate><SE_DeviceId>12345</SE_DeviceId><SE_DeviceSyncKey>300045</SE_DeviceSyncKey></Tab_Seals></Tab_Seals>";
            string xmlSifStatus = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Tab_SifStatus><Tab_SifStatus xmlns=\"Tab_SifStatus\"><SS_AirlineCode>1049</SS_AirlineCode><SS_SifNo>MFC 300045</SS_SifNo><SS_SifPackedPrinted>2017-09-13T12:49:06</SS_SifPackedPrinted><SS_DeviceIdHHC1>12345</SS_DeviceIdHHC1><SS_DownloadStationHHC1>MFC</SS_DownloadStationHHC1><SS_DownloadSuccessHHC1>2017-09-13T12:49:06</SS_DownloadSuccessHHC1><SS_FirstInboundPI>2017-09-13T12:49:06</SS_FirstInboundPI><SS_DeviceIdInboundPI>12345</SS_DeviceIdInboundPI><SS_LastUpdated>2017-09-13T12:49:06</SS_LastUpdated></Tab_SifStatus></Tab_SifStatus>";
            


            /*
            PICountRequest pcr=new PICountRequest();
            pcr.AirlineCode=1049;
            pcr.SifNo="ZZZ 100062";
            PICountResult pcre = service.PICount(pcr);
            var v1 = pcre;

            var ts= service.Test(new DownloadDatabaseRequest[] { });

            //bool re = service.UpdateDatabase(1049);
            bool bl = service.UploadSales(xmlFlightRecorded, xmlOrderHeader, xmlOrderLine, xmlPaymentLine, xmlItemCount, xmlCrewRecorded, xmlSeals, xmlSifStatus);
            */

            /*
            DownloadDatabaseRequest request = new DownloadDatabaseRequest();
            request.AirlineCode = 25;
            request.SifPrefix = "SIN";
            request.CartNo = "AB805S2";
            DownloadDatabaseResult rsp = service.DownloadDatabase(request);
            */

            
            //byte[] buffer = new byte[8 * 1024];
            //int len;

            using (var fileStream = File.Create("C:/Lucky/db111.sqlite"))
            {
                //sp.FileByteStream.CopyTo(fileStream);
                //while ((len = fs.Read(buffer, 0, buffer.Length)) > 0)
                //{
                //    fileStream.Write(buffer, 0, len);
                //}

            }

            
            
            
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlFlightRecorded);
            XmlNamespaceManager manager = new XmlNamespaceManager(xmldoc.NameTable);
            manager.AddNamespace("ns", "Tab_FlightRecorded");

            foreach (XmlNode node in xmldoc.SelectNodes("//ns:Tab_FlightRecorded", manager))
            {
                var v = node.SelectSingleNode("//ns:FR_From",manager);
                Console.WriteLine("{0}: {1}", node.Name, node.InnerText);
            }

        }
    }
}
