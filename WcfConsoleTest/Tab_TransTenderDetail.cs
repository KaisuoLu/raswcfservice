//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleApplication2
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tab_TransTenderDetail
    {
        public int TC_RecordNo { get; set; }
        public short TC_AirLineCode { get; set; }
        public string TC_CuCode { get; set; }
        public decimal TC_ExRate { get; set; }
        public decimal TC_Amount { get; set; }
    }
}
