﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.SharedDb.DataAccess;
namespace Dfass.SharedDb.Repository
{
    public interface ISetupPassportBlacklistRepository : IRepository<Tab_SetupPassportBlackList>
    {
    }
}