﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;

namespace Dfass.Malaysia.Business
{
    public interface ISifStatusBusiness : IBusiness<Tab_SifStatus>
    {
        //IList<Tab_SifStatus> ParseXmlDocument(string xmlDocument);

        bool Existed(string sifno);
        //bool Add(IList<Tab_SifStatus> listSifStatus);
        //bool PreAdd(DbContext context, IList<Tab_SifStatus> listSifStatus);
    }
}
