﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;
using Dfass.Malaysia.Repository;
using Dfass.Requests;
using Dfass.Results;

namespace Dfass.Malaysia.Business
{
    public class SalesBusiness:ISalesBusiness
    {
        IFlightRecordedBusiness businessFlightRecorded = null;
        IOrderHeaderBusiness businessOrderHeader = null;
        IOrderLineBusiness businessOrderLine = null;
        IPaymentLineBusiness businessPaymentLine = null;
        IItemCountBusiness businessItemCount = null;
        ICrewRecordedBusiness businessCrewRecorded = null;
        ISealsBusiness businessSeals = null;
        ISifStatusBusiness businessSifStatus = null;

        public SalesBusiness()
        {
            businessFlightRecorded = new FlightRecordedBusiness();
            businessOrderHeader = new OrderHeaderBusiness();
            businessOrderLine = new OrderLineBusiness();
            businessPaymentLine = new PaymentLineBusiness();
            businessItemCount = new ItemCountBusiness();
            businessCrewRecorded = new CrewRecordedBusiness();
            businessSeals = new SealsBusiness();
            businessSifStatus = new SifStatusBusiness();
        }

        public bool CreateDatabase(string wcfPath, int airlinecode)
        {
            bool result = true;

            //ISetupCCBlackListRepository repositorySetupCCBlackList = new SetupCCBlackListRepository();
            //IList<Tab_SetupCCBlackList> ccBlackList = repositorySetupCCBlackList.GetAll();

            ISetupCCTypeRepository repositorySetupCCType = new SetupCCTypeRepository();
            IList<Tab_SetupCCType> ccTypes = repositorySetupCCType.GetAll();

            ISetupCrewMasterRepository repositorySetupCrewMaster = new SetupCrewMasterRepository();
            IList<Tab_SetupCrewMaster> crews = repositorySetupCrewMaster.GetAll();

            //ISetupCrewPositionRepository repositorySetupCrewPosition = new SetupCrewPositionRepository();
            //IList<Tab_SetupCrewPosition> positions = repositorySetupCrewPosition.GetAll();

            ISetupCurrencyRepository repositorySetupCurrency = new SetupCurrencyRepository();
            IList<Tab_SetupCurrency> currencies = repositorySetupCurrency.GetAll();

            ISetupFlightMasterRepository repositorySetupFlightMaster = new SetupFlightMasterRepository();
            IList<Tab_SetupFlightMaster> flights = repositorySetupFlightMaster.GetAll();

            ISetupHHCMessageRepository repositorySetupHHCMessage = new SetupHHCMessageRepository();
            IList<Tab_SetupHHCMessage> messages = repositorySetupHHCMessage.GetAll();

            ISetupItemCategoryRepository repositorySetupItemCategory = new SetupItemCategoryRepository();
            IList<Tab_SetupItemCategory> categories = repositorySetupItemCategory.GetAll();

            ISetupKitMasterRepository repositorySetupKitMaster = new SetupKitMasterRepository();
            IList<Tab_SetupKitMaster> kits = repositorySetupKitMaster.GetAll();

            ISetupKitDetailRepository repositorySetupKitDetail = new SetupKitDetailRepository();
            IList<Tab_SetupKitDetail> kds = repositorySetupKitDetail.GetAll();

            //ISetupRegionRepository repositorySetupRegion = new SetupRegionRepository();
            //IList<Tab_SetupRegion> regions = repositorySetupRegion.GetAll();

            ISetupSectorRepository repositorySetupSector = new SetupSectorRepository();
            IList<Tab_SetupSector> sectors = repositorySetupSector.GetAll();

            ISetupStationRepository repositorySetupStation = new SetupStationRepository();
            IList<Tab_SetupStation> stations = repositorySetupStation.GetAll();

            string source=Path.Combine(wcfPath,"db.sqlite");

            SalesBusiness2 bs2 = new SalesBusiness2();
            bs2.OpenDatabase();

            using (var dbc = new System.Data.SQLite.SQLiteConnection("Data Source=C:\\Development\\RasWcfService\\Dfass.RasWcfService\\db.sqlite"))
            {
                int t = 1;
            }
            var cnn1 = new SQLiteConnection("Data Source="+source);


            string destination=System.IO.Path.Combine(wcfPath+"database" ,airlinecode+".sqlite");
            FileInfo fl = new FileInfo(destination);
            if (fl.Exists)
                fl.Delete();
            File.Copy(source, destination);


            string cmdInsert=null;
            using (var conn = new SQLiteConnection("Data Source=" + destination))
            {
                // Be sure you already created the Person Table!
                conn.Open();

                using (var cmd = new SQLiteCommand(conn))
                {
                    using (var transaction = conn.BeginTransaction())
                    {
                        //Insert SetupCCType into sys_cctype
                        cmdInsert = "INSERT INTO sys_cctype VALUES(?,?,?,?,?,?,?,?,?,?,?);";
                        cmd.CommandText = cmdInsert;
                        int i = 0;
                        foreach (var ct in ccTypes)
                        {
                            i++;
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", i));
                            cmd.Parameters.Add(new SQLiteParameter("CCCode", ct.CT_CCCode));
                            cmd.Parameters.Add(new SQLiteParameter("CCName", ct.CT_CCName));
                            cmd.Parameters.Add(new SQLiteParameter("MaxLimit", ct.CT_MaxLimit));
                            cmd.Parameters.Add(new SQLiteParameter("MinLimit", ct.CT_MinLimit));
                            cmd.Parameters.Add(new SQLiteParameter("Range", ct.CT_Range));
                            cmd.Parameters.Add(new SQLiteParameter("PaymentMode", ct.CT_PaymentMode));
                            cmd.Parameters.Add(new SQLiteParameter("MerchantAC", ct.CT_MerchantAC));
                            cmd.Parameters.Add(new SQLiteParameter("MerchantAddress", ct.CT_MerchantAddress));
                            cmd.Parameters.Add(new SQLiteParameter("MerchantPhone", ct.CT_MerchantPhone));
                            cmd.Parameters.Add(new SQLiteParameter("BaseCurrency", ct.CT_BaseCurrency));
                            cmd.ExecuteNonQuery();
                        }


                        //Insert Crew from CrewMaster



                        cmdInsert = "INSERT INTO sys_crew VALUES(?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;

                        foreach (var crew in crews)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("CrewID", crew.CM_CrewId));
                            cmd.Parameters.Add(new SQLiteParameter("Name", crew.CM_FirstName));
                            cmd.Parameters.Add(new SQLiteParameter("PositionCode", crew.CM_PositionCode));
                            cmd.Parameters.Add(new SQLiteParameter("Base", crew.CM_Base));
                            cmd.Parameters.Add(new SQLiteParameter("Surname", crew.CM_Surname));
                            cmd.ExecuteNonQuery();
                        }
                        //Insert default Training1
                        cmd.Parameters.Add(new SQLiteParameter("CrewID", "8888888"));
                        cmd.Parameters.Add(new SQLiteParameter("Name", "TRAINING1"));
                        cmd.Parameters.Add(new SQLiteParameter("PositionCode", "IFS"));
                        cmd.Parameters.Add(new SQLiteParameter("Base", "KUL"));
                        cmd.Parameters.Add(new SQLiteParameter("Surname", ""));
                        cmd.ExecuteNonQuery();

                        //Insert default Training1
                        cmd.Parameters.Add(new SQLiteParameter("CrewID", "9999999"));
                        cmd.Parameters.Add(new SQLiteParameter("Name", "TRAINING2"));
                        cmd.Parameters.Add(new SQLiteParameter("PositionCode", "IFS"));
                        cmd.Parameters.Add(new SQLiteParameter("Base", "KUL"));
                        cmd.Parameters.Add(new SQLiteParameter("Surname", ""));
                        cmd.ExecuteNonQuery();

                        //Insert SetupCurrency into sys_currency
                        cmdInsert = "INSERT INTO sys_currency VALUES(?,?,?,?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var currency in currencies)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("Currency", currency.CU_Code));
                            cmd.Parameters.Add(new SQLiteParameter("Rate", currency.CU_ExRate));
                            cmd.Parameters.Add(new SQLiteParameter("Name", currency.CU_CurrencyName));
                            cmd.Parameters.Add(new SQLiteParameter("Symbol", currency.CU_Symbol));
                            cmd.Parameters.Add(new SQLiteParameter("MaxLimit", currency.CU_MaxLimit));
                            cmd.Parameters.Add(new SQLiteParameter("MinLimit", currency.CU_MinLimit));
                            cmd.Parameters.Add(new SQLiteParameter("temp", DBNull.Value));
                            cmd.Parameters.Add(new SQLiteParameter("temp2", DBNull.Value));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert FlightMaster into sys_flight
                        cmdInsert = "INSERT INTO sys_flight VALUES(?,?,?,?,?,?,?);";//(FlightCode,FlightNo,FlightFrom,FlightTo,Status,Region)
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var f in flights)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", f.FM_RecordNo));
                            cmd.Parameters.Add(new SQLiteParameter("FlightCode", f.FM_FlightCode));
                            cmd.Parameters.Add(new SQLiteParameter("FlightNo", f.FM_FlightNo));
                            cmd.Parameters.Add(new SQLiteParameter("FlightFrom", f.FM_From));
                            cmd.Parameters.Add(new SQLiteParameter("FlightTo", f.FM_To));
                            cmd.Parameters.Add(new SQLiteParameter("Status", f.FM_Status));
                            cmd.Parameters.Add(new SQLiteParameter("Region", f.FM_Region));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupHHCMessage into 
                        cmdInsert = "INSERT INTO sys_msg VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var m in messages)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("MessageType", m.HM_MessageType));
                            cmd.Parameters.Add(new SQLiteParameter("Message", m.HM_MessageContent));
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", m.HM_RecordNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupItemCategory into sys_category
                        cmdInsert = "INSERT or IGNORE INTO sys_category VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var c in categories)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("CategoryID", c.CA_CategoryId));
                            cmd.Parameters.Add(new SQLiteParameter("Name", c.CA_ButtonText));
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", c.CA_RecordNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupKitMaster into sys_kit
                        cmdInsert = "INSERT INTO sys_kit VALUES(?,?,?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var k in kits)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", k.SK_RecordNo));
                            cmd.Parameters.Add(new SQLiteParameter("KitCode", k.SK_KitCode));
                            cmd.Parameters.Add(new SQLiteParameter("Description", k.SK_Description));
                            cmd.Parameters.Add(new SQLiteParameter("POSDescription", k.SK_POSDescription));
                            cmd.Parameters.Add(new SQLiteParameter("Status", k.SK_Status));
                            cmd.Parameters.Add(new SQLiteParameter("StartDate", k.SK_StartDate));
                            cmd.Parameters.Add(new SQLiteParameter("EndDate", k.SK_EndDate));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert SetupKitDetail into sys_kit_details
                        cmdInsert = "INSERT INTO sys_kit_details VALUES(?,?,?,?,?,?,?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var kd in kds)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", kd.KD_RecordNo));//
                            cmd.Parameters.Add(new SQLiteParameter("ItemCode", kd.KD_ItemCode));
                            cmd.Parameters.Add(new SQLiteParameter("ItemName", kd.KD_ButtonText));
                            cmd.Parameters.Add(new SQLiteParameter("CategoryID", kd.KD_CategoryId));
                            cmd.Parameters.Add(new SQLiteParameter("UploadQty", kd.KD_UploadQty));
                            cmd.Parameters.Add(new SQLiteParameter("ItemLocation", kd.KD_ItemLocation));
                            cmd.Parameters.Add(new SQLiteParameter("Barcode", kd.KD_BarCode));
                            cmd.Parameters.Add(new SQLiteParameter("RetailPrice", kd.KD_RetailPrice));
                            cmd.Parameters.Add(new SQLiteParameter("CatalogNo", kd.KD_CatalogNo));
                            cmd.ExecuteNonQuery();
                        }

                        //Insert into g_products
                        if (kits.Count > 0)
                        {
                            IEnumerable<Tab_SetupKitDetail> prs=from d in kds where d.KD_RecordNo==kits[0].SK_RecordNo select d;
                            cmdInsert = "INSERT INTO g_products VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                            cmd.Parameters.Clear();
                            cmd.CommandText = cmdInsert;

                            foreach (var pr in prs)
                            {
                                //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                                cmd.Parameters.Add(new SQLiteParameter("ProductCode", pr.KD_ItemCode));//
                                cmd.Parameters.Add(new SQLiteParameter("Name", pr.KD_ButtonText));
                                cmd.Parameters.Add(new SQLiteParameter("Price", pr.KD_RetailPrice));
                                cmd.Parameters.Add(new SQLiteParameter("CategoryID", pr.KD_CategoryId));
                                cmd.Parameters.Add(new SQLiteParameter("Qty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("Brand", ""));
                                cmd.Parameters.Add(new SQLiteParameter("Bin", pr.KD_ItemLocation));
                                cmd.Parameters.Add(new SQLiteParameter("CatalogNo", pr.KD_CatalogNo));
                                cmd.Parameters.Add(new SQLiteParameter("Description", pr.KD_ItemName));
                                cmd.Parameters.Add(new SQLiteParameter("DmgQty", 0));
                                cmd.Parameters.Add(new SQLiteParameter("PhyQty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("CartQty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("Barcode", pr.KD_BarCode));
                                cmd.Parameters.Add(new SQLiteParameter("CrewDiscount", 0));
                                cmd.Parameters.Add(new SQLiteParameter("OpenQty", pr.KD_UploadQty));
                                cmd.Parameters.Add(new SQLiteParameter("LAG", 0));
                                cmd.ExecuteNonQuery();
                            }
                        }

                        //Insert SetupRegion into sys_region

                        //Insert SetupSector into sys_sector
                        cmdInsert = "INSERT INTO sys_sector VALUES(?,?,?);";
                        cmd.Parameters.Clear();
                        cmd.CommandText = cmdInsert;
                        foreach (var se in sectors)
                        {
                            //cmd.CommandText = String.Format(cmdInsert, crew.CM_CrewId, crew.CM_FirstName,crew.CM_PositionCode,crew.CM_Base,crew.CM_Surname);
                            cmd.Parameters.Add(new SQLiteParameter("SectorCode", se.SS_SectorCode));
                            cmd.Parameters.Add(new SQLiteParameter("SectorName", se.SS_SectorName));
                            cmd.Parameters.Add(new SQLiteParameter("RecordNo", se.SS_RecordNo));
                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                }
            }

            
            return result;
        }

        public bool SaveSales(string xmlFlightRecorded, string xmlOrderHeader, string xmlOrderLine, string xmlPaymentLine, string xmlItemCount, string xmlCrewRecorded, string xmlSeals, string xmlSifStatus)
        {
            bool result=true;

            using (var context = new IRS_BO_MalaysiaEntities())
            {
                //
                IList<Tab_SifStatus> sss = businessSifStatus.ParseXmlDocument(xmlSifStatus);
                if (sss.Count == 0)
                    return false;
                else
                {
                    //If already uploaded, just return true.
                    if (businessSifStatus.Existed(sss[0].SS_SifNo))
                        return true;
                }
                //Save Flight Recorded
                IList<Tab_FlightRecorded> frs = businessFlightRecorded.ParseXmlDocument(xmlFlightRecorded);
                result = result && businessFlightRecorded.PreAdd(context,frs);

                //Save OrderHeader
                IList<Tab_OrderHeader> ohs = businessOrderHeader.ParseXmlDocument(xmlOrderHeader);
                result = result && businessOrderHeader.PreAdd(context,ohs);

                //Save OrderLine
                IList<Tab_OrderLine> ols = businessOrderLine.ParseXmlDocument(xmlOrderLine);
                result = result && businessOrderLine.PreAdd(context,ols);

                ////Save PaymentLine
                IList<Tab_PaymentLine> pls = businessPaymentLine.ParseXmlDocument(xmlPaymentLine);
                result = result && businessPaymentLine.PreAdd(context,pls);

                //Save ItemCount
                IList<Tab_ItemCount> ics = businessItemCount.ParseXmlDocument(xmlItemCount);
                result = result && businessItemCount.PreAdd(context, ics);

                //Save CrewRecorded
                IList<Tab_CrewRecorded> crs = businessCrewRecorded.ParseXmlDocument(xmlCrewRecorded);
                result = result && businessCrewRecorded.PreAdd(context, crs);

                //Save Seals
                IList<Tab_Seals> ses = businessSeals.ParseXmlDocument(xmlSeals);
                result = result && businessSeals.PreAdd(context, ses);

                //Save SifStatus
                
                result = result && businessSifStatus.PreAdd(context, sss);


                context.SaveChanges();
            }

            return result;
        }

        public PICountResult PICount(PICountRequest request)
        {
            PICountResult result = new PICountResult();
            ISetupKitDetailRepository repositorySetupKitDetail=new SetupKitDetailRepository();
            ISetupItemCategoryRepository repositorySetupItemCategory=new SetupItemCategoryRepository();

            Tab_FlightRecorded fr = businessFlightRecorded.GetSingle(f => f.FR_SifNo == request.SifNo && f.FR_Hide == false);
            if (fr == null)
            {
                result.ResultState = "ERROR";
                result.ErrorDiscription = string.Format("Sif of {0} doesn't exist.",request.SifNo);
                return result;
            }
            result.FlightNo = fr.FR_FlightNo;
            result.FlightFrom = fr.FR_From;
            result.FlightTo = fr.FR_To;
            result.FlightDate = fr.FR_DepartureDate;
            result.CartNo = fr.FR_CartNo;
            result.KitCode = fr.FR_KitCode;

            var itemcounts = from ic in businessItemCount.GetList(i => i.IC_SifNo == request.SifNo)
                    join skd in repositorySetupKitDetail.GetList(k => k.KD_RecordNo == 1)
                      on ic.IC_Sku equals skd.KD_ItemCode into temp1
                    from t in temp1
                    join sic in repositorySetupItemCategory.GetAll()
                      on t.KD_CategoryId equals sic.CA_CategoryId
                    select new Dfass.Models.ItemCount
                    {
                        SifNo=request.SifNo,
                        CartNo=result.CartNo,
                        CategoryId=t.KD_CategoryId,
                        CategoryName=sic.CA_CategoryName,
                        Sku = t.KD_ItemCode,
                        Name=t.KD_ButtonText,
                        PackerOut=ic.IC_PackerOut,
                        CrewBegin=ic.IC_CrewBegin,
                        CrewBeginRouteAdjust=ic.IC_CrewBeginRouteAdjust,
                        CrewSold=ic.IC_CrewSold,
                        CrewSoldRoute=ic.IC_CrewSoldRoute,
                        CrewReturnRouteAdjust=ic.IC_CrewReturnRouteAdjust,
                        CrewReturn=ic.IC_CrewReturn,
                        ReturnTotal=ic.IC_ReturnTotal,
                        ReturnDamaged=ic.IC_ReturnDamaged,
                        Replenish=ic.IC_Replenish,
                        PI_Mode=ic.IC_PI_Mode,
                        CrewSoldUnit1=ic.IC_CrewSoldUnit1,
                        CrewSoldUnit2=ic.IC_CrewSoldUnit2,
                        RecordedDate=ic.IC_RecordedDate,
                        Latest=ic.IC_Latest??0,
                        LatestUplift=ic.IC_LatestUplift??0
                    };
            result.ItemCounts = itemcounts.ToList<Dfass.Models.ItemCount>();
            //IList<Tab_ItemCount> ics = businessItemCount.GetList(i => i.IC_SifNo == request.SifNo);
            //result.ItemCounts = new List<Models.ItemCount>();
            //foreach (Tab_ItemCount ic in ics)
            //{
            //    Dfass.Models.ItemCount itemcount = Dfass.Malaysia.Translators.ItemCountTranslator.TranslateToModel(ic);
            //    result.ItemCounts.Add(itemcount);
            //}
            return result;
        }
    }
}
