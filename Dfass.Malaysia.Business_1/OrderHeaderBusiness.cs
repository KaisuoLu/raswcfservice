﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Malaysia.Repository;

namespace Dfass.Malaysia.Business
{
    public class OrderHeaderBusiness : Business<Tab_OrderHeader>, IOrderHeaderBusiness
    {
        IOrderHeaderRepository repositoryOrderHeader = null;

        public OrderHeaderBusiness()
        {
            repositoryOrderHeader = new OrderHeaderRepository();
        }
        //public static long? ParseNullableLong(this string value)
        //{
        //    long longValue;
        //    if (long.TryParse(value, out longValue))
        //        return longValue;
        //    return null;
        //}
        public override IList<Tab_OrderHeader> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_OrderHeader> ohs = new List<Tab_OrderHeader>();
            if (string.IsNullOrEmpty(xmlDocument))
                return ohs;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_OrderHeader");

            var nodes = document.SelectNodes("//ns:Tab_OrderHeader", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_OrderHeader oh = new Tab_OrderHeader();
                oh.OH_SifNo = n.SelectSingleNode("ns:OH_SifNo", manager).InnerText;
                oh.OH_Sector = byte.Parse(n.SelectSingleNode("ns:OH_Sector", manager).InnerText);
                oh.OH_OrderNo = short.Parse(n.SelectSingleNode("ns:OH_OrderNo", manager).InnerText);
                oh.OH_Total = decimal.Parse(n.SelectSingleNode("ns:OH_Total", manager).InnerText);
                oh.OH_Voided = n.SelectSingleNode("ns:OH_Voided", manager).InnerText == "1" ? true : false;
                oh.OH_OrderTime = DateTime.Parse(n.SelectSingleNode("ns:OH_OrderTime", manager).InnerText);
                oh.OH_Credit = n.SelectSingleNode("ns:OH_Credit", manager).InnerText == "1" ? true : false;
                oh.OH_OriginalSector = byte.Parse(n.SelectSingleNode("ns:OH_OriginalSector", manager).InnerText);
                oh.OH_Seat = n.SelectSingleNode("ns:OH_Seat", manager).InnerText;
                oh.OH_Type = n.SelectSingleNode("ns:OH_Type", manager).InnerText;
                oh.OH_DeviceId = short.Parse(n.SelectSingleNode("ns:OH_DeviceId", manager).InnerText);
                oh.OH_DeviceSyncKey = int.Parse(n.SelectSingleNode("ns:OH_DeviceSyncKey", manager).InnerText);
                oh.OH_Passport = n.SelectSingleNode("ns:OH_Passport", manager).InnerText;
                oh.OH_Service = "MA";
                oh.OH_PaxName = n.SelectSingleNode("ns:OH_PaxName", manager).InnerText;
                oh.OH_PaxClass = n.SelectSingleNode("ns:OH_PaxClass", manager).InnerText;
                oh.OH_CrewSale_CrewId = n.SelectSingleNode("ns:OH_CrewSale_CrewId", manager).InnerText;
                oh.OH_PointCardNo = n.SelectSingleNode("ns:OH_PointCardNo", manager).InnerText;
                oh.OH_PointCardType = n.SelectSingleNode("ns:OH_PointCardType", manager).InnerText;
                //string pts = n.SelectSingleNode("ns:OH_PointCardPoints", manager).InnerText;
                oh.OH_PointCardPoints = Utils.Util.ParseNullableLong(n.SelectSingleNode("ns:OH_PointCardPoints", manager).InnerText);
                ohs.Add(oh);
            }

            return ohs;
        }
        public override bool Add(IList<Tab_OrderHeader> listOrderHeader)
        {
            bool result = true;
            repositoryOrderHeader.Add(listOrderHeader.ToArray());
            return result;
        }

        public override bool PreAdd(DbContext context, IList<Tab_OrderHeader> listOrderHeader)
        {
            return repositoryOrderHeader.PreAdd(context, listOrderHeader.ToArray<Tab_OrderHeader>());
        }
    }
}
