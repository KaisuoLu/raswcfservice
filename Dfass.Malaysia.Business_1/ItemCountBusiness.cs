﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Malaysia.Repository;
using Dfass.Utils;

namespace Dfass.Malaysia.Business
{
    public class ItemCountBusiness : Business<Tab_ItemCount>, IItemCountBusiness
    {
        IItemCountRepository repositoryItemCount = null;

        public ItemCountBusiness()
        {
            repositoryItemCount = new ItemCountRepository();
            base.repository = repositoryItemCount;
        }

        public override IList<Tab_ItemCount> ParseXmlDocument(string xmlDocument)
        {
            IList<Tab_ItemCount> ohs = new List<Tab_ItemCount>();
            if (string.IsNullOrEmpty(xmlDocument))
                return ohs;

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlDocument);

            XmlNamespaceManager manager = new XmlNamespaceManager(document.NameTable);
            manager.AddNamespace("ns", "Tab_ItemCount");

            var nodes = document.SelectNodes("//ns:Tab_ItemCount", manager);

            foreach (XmlNode n in nodes)
            {
                Tab_ItemCount ic = new Tab_ItemCount();
                ic.IC_SifNo = n.SelectSingleNode("ns:IC_SifNo", manager).InnerText;
                ic.IC_CartNo = n.SelectSingleNode("ns:IC_CartNo", manager).InnerText;
                ic.IC_Sku = n.SelectSingleNode("ns:IC_Sku", manager).InnerText;
                ic.IC_PackerOut = byte.Parse(n.SelectSingleNode("ns:IC_PackerOut", manager).InnerText);
                ic.IC_CrewBegin = ic.IC_PackerOut;
                //ic.IC_CrewBegin = short.Parse(n.SelectSingleNode("ns:IC_CrewBegin", manager).InnerText);
                ic.IC_CrewBeginRouteAdjust = short.Parse(n.SelectSingleNode("ns:IC_CrewBeginRouteAdjust", manager).InnerText);
                ic.IC_CrewSold = short.Parse(n.SelectSingleNode("ns:IC_CrewSold", manager).InnerText);
                ic.IC_CrewReturn = short.Parse(n.SelectSingleNode("ns:IC_CrewReturn", manager).InnerText);
                ic.IC_ReturnTotal = short.Parse(n.SelectSingleNode("ns:IC_ReturnTotal", manager).InnerText);
                ic.IC_ReturnDamaged = short.Parse(n.SelectSingleNode("ns:IC_ReturnDamaged", manager).InnerText);
                ic.IC_Replenish = short.Parse(n.SelectSingleNode("ns:IC_Replenish", manager).InnerText);
                ic.IC_PI_Mode = n.SelectSingleNode("ns:IC_PI_Mode", manager).InnerText == "1";
                ic.IC_RecordedDate = DateTime.Parse(n.SelectSingleNode("ns:IC_RecordedDate", manager).InnerText);
                ic.IC_Latest = Util.ParseNullableShort(n.SelectSingleNode("ns:IC_Latest", manager).InnerText);
                //ic.IC_DrawerNo = n.SelectSingleNode("ns:IC_DrawerNo", manager).InnerText;
                ic.IC_ReturnTotal = short.Parse(n.SelectSingleNode("ns:IC_ReturnTotal", manager).InnerText);
                ic.IC_LatestUplift = ic.IC_Replenish;
                ohs.Add(ic);
            }

            return ohs;
        }
        public override bool Add(IList<Tab_ItemCount> listItemCount)
        {
            bool result = true;
            repositoryItemCount.Add(listItemCount.ToArray());
            return result;
        }

        public override bool PreAdd(DbContext context, IList<Tab_ItemCount> listItemCount)
        {
            return repositoryItemCount.PreAdd(context, listItemCount.ToArray<Tab_ItemCount>());
        }
    }
}
