﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Requests;
using Dfass.Results;

namespace Dfass.Malaysia.Business
{
    public interface ISalesBusiness
    {
        bool CreateDatabase(string wcfPath, int airlinecode);

        bool SaveSales(string xmlFlightRecorded, string xmlOrderHeader, string xmlOrderLine, string xmlPaymentLine, string xmlItemCount, string xmlCrewRecorded, string xmlSeals, string xmlSifStatus);

        PICountResult PICount(PICountRequest request);
    }
}
