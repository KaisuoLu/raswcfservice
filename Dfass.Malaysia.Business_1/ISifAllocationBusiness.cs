﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.DataAccess;

namespace Dfass.Malaysia.Business
{
    public interface ISifAllocationBusiness : IBusiness<Tab_SifAllocation>
    {
        int GetSifNo(string sifprefix);
    }
}
