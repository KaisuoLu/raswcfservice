﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Dfass.Malaysia.Repository;


namespace Dfass.Malaysia
{
    public class Business<T> : IBusiness<T> where T : class
    {
        protected Dfass.Malaysia.Repository.IRepository<T> repository = null;

        public Business()
        {
        }

        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            return repository.GetAll(navigationProperties);
        }

        public virtual IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            return repository.GetList(where, navigationProperties);
        }

        public virtual T GetSingle(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            return repository.GetSingle(where, navigationProperties);
        }

        public virtual void Add(params T[] items)
        {
            repository.Add(items);
        }

        public virtual void Update(params T[] items)
        {
            repository.Update(items);
        }

        public virtual void Delete(params T[] items)
        {
            repository.Delete(items);
        }
        public virtual IList<T> Delete(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            return repository.Delete(where, navigationProperties);
        }

        public virtual bool Add(IList<T> listCrewRecorded)
        {
            return false;
        }

        public virtual bool PreAdd(DbContext context, IList<T> listCrewRecorded)
        {
            return false;
        }

        public virtual IList<T> ParseXmlDocument(string xmlDocument)
        {
            return null;
        }

    }
}
