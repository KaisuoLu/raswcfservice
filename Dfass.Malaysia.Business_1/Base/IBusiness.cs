﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dfass.Malaysia
{
    public interface IBusiness<T> where T : class
    {
        IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        void Add(params T[] items);
        void Update(params T[] items);
        void Delete(params T[] items);
        IList<T> Delete(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);

        bool Add(IList<T> listCrewRecorded);
        bool PreAdd(DbContext context, IList<T> listCrewRecorded);
        IList<T> ParseXmlDocument(string xmlDocument);
    }
}
