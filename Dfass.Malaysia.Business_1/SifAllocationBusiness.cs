﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Malaysia.Repository;
using Dfass.Utils;

namespace Dfass.Malaysia.Business
{
    public class SifAllocationBusiness : Business<Tab_SifAllocation>, ISifAllocationBusiness
    {
        ISifAllocationRepository repositorySifAllocation = null;
        object lockObject = new object();

        public SifAllocationBusiness()
        {
            repositorySifAllocation = new SifAllocationRepository();
        }

        public int GetSifNo(string sifprefix)
        {
            int sifno = -1;
            lock (lockObject)
            {
                Tab_SifAllocation sa = repositorySifAllocation.GetSingle(a => a.SA_SifPrefix == sifprefix);
                if(sa!=null)
                    sa.SA_SifNo_int++;
                repositorySifAllocation.Update(sa);

                sifno = sa.SA_SifNo_int;
            }
            return sifno;
        }
    }
}
