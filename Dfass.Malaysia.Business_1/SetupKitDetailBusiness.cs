﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Dfass.Malaysia.DataAccess;
using Dfass.Malaysia.Repository;

namespace Dfass.Malaysia.Business
{
    public class SetupKitDetailBusiness : Business<Tab_SetupKitDetail>, ISetupKitDetailBusiness
    {
        ISetupKitDetailRepository repositorySetupKitDetail = null;

        public SetupKitDetailBusiness()
        {
            repositorySetupKitDetail = new SetupKitDetailRepository();
        }

    }
}
